package es.minetsii.Splashoon.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import es.minetsii.Splashoon.ArenaData;
import es.minetsii.Splashoon.Splashoon;

public class Leave implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("leave")){
			if(sender instanceof Player){
				leave((Player)sender);
			}
		}
		return true;
	}

	
	public static void leave(Player player){
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("Connect");
		out.writeUTF(ArenaData.bungeelobby);
		player.sendPluginMessage(Splashoon.instance, "BungeeCord", out.toByteArray());
	}
}
