package es.minetsii.Splashoon.Objects;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class Region {

	
	private Location min;
	private Location max;
	public  List<Block> sendBlocks;
	public  Set<Block> blueblocks;
	public  Set<Block> orangeblocks;
	
	public Region(Location min, Location max){
		if(min == null){
			if(this.min == null){
				this.max = max;
				return;
			}
			else min = this.min;
		}
		if(max == null){
			if(this.max == null){
				this.min = min;
				return;
			}
			else max = this.max;
		}
		double xmin;
		double xmax;
		double ymin;
		double ymax;
		double zmin;
		double zmax;
		xmin = Math.min(min.getBlockX(),max.getBlockX());
		ymin = Math.min(min.getBlockY(),max.getBlockY());
		zmin = Math.min(min.getBlockZ(),max.getBlockZ());
		xmax = Math.max(min.getBlockX(),max.getBlockX());
		ymax = Math.max(min.getBlockY(),max.getBlockY());
		zmax = Math.max(min.getBlockZ(),max.getBlockZ());
		this.min = new Location(min.getWorld(), xmin, ymin, zmin);
		this.max = new Location(min.getWorld(), xmax, ymax, zmax);
		loadSendBlocks();
		blueblocks = new HashSet<Block>();
		orangeblocks = new HashSet<Block>();
	}
	
	public List<Location> getLocations(){
		List<Location> l = new ArrayList<Location>();
		for(double x = min.getX(); x <= max.getX();x++){
			for(double y = min.getY(); y <= max.getY();y++){
				for(double z = min.getZ(); z <= max.getZ();z++){
					l.add(new Location(min.getWorld(), x, y, z));
				}
			}
		}
		return l;
	}
	
	private List<Block> loadSendBlocks(){
		sendBlocks = new ArrayList<Block>();
		for(double x = min.getX(); x <= max.getX();x++){
			for(double y = min.getY(); y <= max.getY();y++){
				for(double z = min.getZ(); z <= max.getZ();z++){
					Block b = new Location(min.getWorld(), x, y, z).getBlock();
					if(b.getType().equals(Material.BIRCH_FENCE_GATE) ||
							b.getType().equals(Material.FENCE_GATE) ||
							b.getType().equals(Material.CARPET)){
						sendBlocks.add(b);
					}
				}
			}
		}
		return sendBlocks;
		
	}
	
	public Region setLocs(Location min, Location max){
		if(min == null){
			if(this.min == null){
				this.max = max;
				return this;
			}
			else min = this.min;
		}
		if(max == null){
			if(this.max == null){
				this.min = min;
				return this;
			}
			else max = this.max;
		}
		double xmin;
		double xmax;
		double ymin;
		double ymax;
		double zmin;
		double zmax;
		xmin = Math.min(min.getBlockX(),max.getBlockX());
		ymin = Math.min(min.getBlockY(),max.getBlockY());
		zmin = Math.min(min.getBlockZ(),max.getBlockZ());
		xmax = Math.max(min.getBlockX(),max.getBlockX());
		ymax = Math.max(min.getBlockY(),max.getBlockY());
		zmax = Math.max(min.getBlockZ(),max.getBlockZ());
		this.min = new Location(min.getWorld(), xmin, ymin, zmin);
		this.max = new Location(min.getWorld(), xmax, ymax, zmax);
		loadSendBlocks();
		return this;
	}
	
	public int getBluePoints(){
		return blueblocks.size();
	}
	
	public int getOrangePoints(){
		return orangeblocks.size();
	}
	
	public boolean isSetup(){
		return max != null && min != null;
	}

	public Location getMin() {
		return min;
	}

	public Location getMax() {
		return max;
	}
	
	public boolean inArea(Location targetLocation, boolean checkY) {
		if (min.getWorld().getName() == max.getWorld().getName()) {
			if (targetLocation.getWorld().getName() == min.getWorld().getName()) {
				if ((targetLocation.getBlockX() >= min.getBlockX() && targetLocation
						.getBlockX() <= max.getBlockX())
						|| (targetLocation.getBlockX() <= min.getBlockX() && targetLocation
								.getBlockX() >= max.getBlockX())) {
					if ((targetLocation.getBlockZ() >= min.getBlockZ() && targetLocation
							.getBlockZ() <= max.getBlockZ())
							|| (targetLocation.getBlockZ() <= min.getBlockZ() && targetLocation
									.getBlockZ() >= max.getBlockZ())) {
						if (checkY) {
							if ((targetLocation.getBlockY() >= min.getBlockY() && targetLocation
									.getBlockY() <= max.getBlockY())
									|| (targetLocation.getBlockY() <= min
											.getBlockY() && targetLocation
											.getBlockY() >= max.getBlockY())) { return true; }
						} else {
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	
	@SuppressWarnings("deprecation")
	public void sendBlockChangeToCarpet(Player p){
		for(Block l : sendBlocks){
			if(l.getType().equals(Material.FENCE_GATE)){
				p.sendBlockChange(l.getLocation(), Material.CARPET, (byte)0);
			}
			if(l.getType().equals(Material.BIRCH_FENCE_GATE)){
				p.sendBlockChange(l.getLocation(), Material.BIRCH_FENCE_GATE, (byte)(l.getData()-4));
			}	
		}
	}
	
	@SuppressWarnings("deprecation")
	public void sendBlockChangeToFence(Player p){
		for(Block l : sendBlocks){
			if(l.getType().equals(Material.FENCE_GATE)){
				p.sendBlockChange(l.getLocation(), Material.FENCE_GATE,(byte)6);
			}
			if(l.getType().equals(Material.BIRCH_FENCE_GATE)){
				p.sendBlockChange(l.getLocation(), Material.BIRCH_FENCE_GATE, (byte)
						l.getData());
			}	
			
		}
	}
	
	public void addBlueBlock(Block b){
		orangeblocks.remove(b);
		blueblocks.add(b);
	}
	
	public void addOrangeBlock(Block b){
		blueblocks.remove(b);
		orangeblocks.add(b);
	}
	
}
