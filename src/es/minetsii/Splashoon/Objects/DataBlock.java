package es.minetsii.Splashoon.Objects;

import org.bukkit.Material;

public class DataBlock {

	
	private Material material;
	private byte bite;
	
	public DataBlock(Material material, int bite){
		this.material = material;
		this.bite = (byte)bite;
	}
	public DataBlock(Material material, byte bite){
		this.material = material;
		this.bite = bite;
	}
	public DataBlock(Material material){
		this.material = material;
		this.bite = 99;
	}
	public Material getMaterial() {
		return material;
	}
	public void setMaterial(Material material) {
		this.material = material;
	}
	public byte getBite() {
		return bite;
	}
	public void setBite(byte bite) {
		this.bite = bite;
	}
	public boolean hasByte(){
		return bite != 99;
	}
	public boolean containsMaterial(Material m, byte b){
		if(material == m){
			
			if(hasByte()){
				return bite == b;
			}
			else return true;
		}
		else return false;
	}
	
	
}
