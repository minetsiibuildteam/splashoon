package es.minetsii.Splashoon.Objects;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

import es.minetsii.APIs.LocationString;

@SuppressWarnings("deprecation")
public class SaveBlock {

	
	private Material material;
	private byte data;
	private Location location;
	private Block block;
	
	public SaveBlock(Block b){
		 this.material = b.getType();
		 this.data = b.getData();
		 this.location = b.getLocation().clone();
		 block = b;
		 if(material.equals(Material.FENCE) || material.equals(Material.BIRCH_FENCE)){
			 data = 4;
			 updateBlock();
		 }
	}
	
	public SaveBlock setLocation(Location location){
		this.location = location;
		return this;
	}
	
	public SaveBlock setMaterial(Material material){
		this.material = material;
		return this;
	}
	
	public SaveBlock setData(byte data){
		this.data = data;
		return this;
	}

	public Material getMaterial() {
		return material;
	}

	public byte getData() {
		return data;
	}

	public Location getLocation() {
		return location;
	}
	
	public void updateBlock(){
		Block b = block;
		b.setType(material);
		b.setData(data);
		
	}

	@Override
	public String toString() {
		return LocationString.toString(block.getLocation())+"+"
		+material.name()+"+"+data;
	}
	
	public SaveBlock(String s){
		String[] sl = s.split("+");
		if(sl.length != 3) throw new IllegalArgumentException("String lenght must be 3!");
		block = LocationString.fromString(sl[0]).getBlock();
		material = Material.getMaterial(sl[1]);
		data = new Byte(sl[2]);
	}
	
	
}
