package es.minetsii.Splashoon.Weapons.SpecialWeapons;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import es.minetsii.Splashoon.ArenaData;
import es.minetsii.Splashoon.CustomEvents.InklingDamageEvent;
import es.minetsii.Splashoon.CustomEvents.InklingLaunchGrenadeEvent;
import es.minetsii.Splashoon.Player.Inkling;
import es.minetsii.Splashoon.enums.ArenaStatus;

public class SpecialWeaponEvents implements Listener{

	
	@EventHandler
	public void ItemUse(PlayerInteractEvent e){
		if(e.getAction().equals(Action.RIGHT_CLICK_AIR) ||
				e.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
			if(e.getPlayer().getItemInHand() == null) return;
			if(!ArenaData.status.equals(ArenaStatus.playing)) return;
			Inkling ikl = ArenaData.getInkling(e.getPlayer());
			SpecialWeapon sw = ikl.getWeapon().getSpecialWeapon();
			if(sw instanceof SpecialWeaponItem){
				if(sw.isActive() &&  ((SpecialWeaponItem)sw)
						.getItem().isSimilar(ikl.getPlayer().getItemInHand())){
					((SpecialWeaponItem)sw).onUse();
				}
			}
			if(!sw.isActive() && ikl.getPlayer()
					.getItemInHand().getType().equals(Material.CLAY_BALL)){		
				ikl.getWeapon().clearSpecialCount();
				ikl.equip();
				ikl.equipHair();
				sw.execute();
			}
		}
	}
	
	
	//REPEATER
	@EventHandler
	public void bombuse(InklingLaunchGrenadeEvent e){
		Inkling ikl = e.getInkling();
		if(!ikl.getWeapon().getSpecialWeapon().isActive()) return;
		if(!(ikl.getWeapon().getSpecialWeapon() instanceof Repeater)) return;
		e.setInkUsed(0);
	}
	
	//SHIELD
	@EventHandler(priority = EventPriority.LOWEST)
	public void nodamage(InklingDamageEvent e){
		if(!e.getInkling().getWeapon().getSpecialWeapon().isActive() ||
				!(e.getInkling().getWeapon().getSpecialWeapon() instanceof Shield)) return;
		e.setDamage(0);
	}
	
}
