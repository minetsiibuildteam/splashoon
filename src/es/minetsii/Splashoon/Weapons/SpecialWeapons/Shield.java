package es.minetsii.Splashoon.Weapons.SpecialWeapons;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.Location;

import es.minetsii.APIs.Geometric;
import es.minetsii.ApETSII.Particles.ParticleEffect;
import es.minetsii.ApETSII.Titles.TitleAPI;
import es.minetsii.Splashoon.Weapons.Weapon;

public class Shield extends SpecialWeapon{

	public Shield( Weapon weapon) {
		super(100, weapon);
	}

	@Override
	public void onStart() {
		TitleAPI.sendTitle(super.getInkling().getPlayer(),
				3, 5, 3, ChatColor.GOLD+"�Escudo activado!", "");
	}

	@Override
	public void onFinish() {
		TitleAPI.sendTitle(super.getInkling().getPlayer(),
				3, 5, 3, "", ChatColor.RED+"�El escudo se ha agotado!");
	}

	public void showParticles(){
		if(!super.isActive()) return;
		Set<Location> list = new HashSet<Location>(Geometric.Helix(super.getLocation(), 1.5D, 1.7, 3));
		list.stream().forEach(l -> ParticleEffect.CRIT_MAGIC.display(0, 0, 0, 0, 1, l, 100));
	}
	
	
}
