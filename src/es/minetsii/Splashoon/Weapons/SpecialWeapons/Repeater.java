package es.minetsii.Splashoon.Weapons.SpecialWeapons;

import org.bukkit.ChatColor;
import org.bukkit.event.Listener;

import es.minetsii.ApETSII.Titles.TitleAPI;
import es.minetsii.Splashoon.Weapons.Weapon;

public class Repeater extends SpecialWeapon implements Listener{


	public Repeater(Weapon weapon) {
		super(80, weapon);
	}

	@Override
	public void onStart() {
		TitleAPI.sendTitle(super.getWeapon().getInkling().getPlayer(), 
				5, 10, 5, ChatColor.GOLD+"�BOMBAS INFINITAS!", "");
		super.getWeapon().getInkling().setInk(100);
	}

	@Override
	public void onFinish() {
		TitleAPI.sendActionBar(super.getWeapon().getInkling().getPlayer(), 
				ChatColor.RED+"�La super arma se ha gastado!");
	}

}
