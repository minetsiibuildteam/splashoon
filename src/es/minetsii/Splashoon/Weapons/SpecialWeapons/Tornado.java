package es.minetsii.Splashoon.Weapons.SpecialWeapons;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.scheduler.BukkitRunnable;

import es.minetsii.APIs.Geometric;
import es.minetsii.APIs.SoundBuilder;
import es.minetsii.ApETSII.Particles.ParticleEffect;
import es.minetsii.ApETSII.Titles.TitleAPI;
import es.minetsii.Splashoon.ArenaData;
import es.minetsii.Splashoon.Splashoon;
import es.minetsii.Splashoon.CustomEvents.InklingDamageEvent;
import es.minetsii.Splashoon.Weapons.Weapon;

public class Tornado extends SpecialWeapon{
	
	private Location location;
	
	public Tornado(Weapon weapon) {
		super(50, weapon);
	}

	@Override
	public void onStart() {
		TitleAPI.sendTitle(super.getWeapon().getInkling().getPlayer(),
				3, 5, 3, ChatColor.GOLD+"¡TORNADO!", "");
		location = super.getWeapon().getInkling().getLocation().clone();
		Firework fw = location.getWorld().spawn(location, Firework.class);
		FireworkMeta fwm = fw.getFireworkMeta();
		fwm.setPower(2);
		FireworkEffect effect = FireworkEffect.builder()
        		.flicker(true).withColor(Color.AQUA)
        		.withFade().with(Type.BALL).trail(true).build();
		fwm.addEffect(effect);
		fw.setFireworkMeta(fwm);
		super.getWeapon().getInkling().setInk(100);
	}

	@Override
	public void onFinish() {
		final boolean blue = super.getWeapon().getInkling().isBlue();
		final Weapon weapon = super.getWeapon();
		
		final Set<Location> part = 
				new HashSet<Location>(Geometric.Helix(location, 3, 1.4, 40));
		final Set<Location> damage =
				new HashSet<Location>(Geometric.spheres(location.clone().add(0,-1,0),
						3, 20, false, false, 0));
		new SoundBuilder("splashoon.tornadoon").playInLocation(location, 3);
		for(int i = 0; i < 5; i++){
			new BukkitRunnable() {
				@Override
				public void run() {
					part.stream().forEach(loc -> {
						sendParticles(loc, blue);
						ArenaData.paintBlock(weapon.getInkling(),
								loc.clone().add(0,-1,0).getBlock());
						});
					ArenaData.players.values().stream()
					.filter(ikl -> damage
							.contains(ikl.getLocation().getBlock().getLocation()) && ikl.isBlue() != blue)
							.forEach(
									ikl -> new InklingDamageEvent
									(ikl, 10, weapon.getInkling()).execute());
				}
			}.runTaskLater(Splashoon.instance, 10*i);
		}
	}	
	
	
	public void sendParticles(Location l, boolean blue){
		if(blue) ParticleEffect.HEART.
		display(0.3F, 0.3F, 0.3F, 0, 1, l, 1000);
		else ParticleEffect.VILLAGER_ANGRY.
		display(0.3F, 0.3F, 0.3F, 0, 1, l, 1000);
	}

}
