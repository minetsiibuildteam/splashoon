package es.minetsii.Splashoon.Weapons.SpecialWeapons;

import org.bukkit.inventory.ItemStack;

import es.minetsii.Splashoon.Weapons.Weapon;

public abstract class SpecialWeaponItem extends SpecialWeapon{

	private ItemStack item;
	
	public SpecialWeaponItem(long duration, Weapon weapon, ItemStack item) {
		super(duration, weapon);
		this.item = item;
	}

	public abstract void onUse();
	
	public ItemStack getItem(){
		return item;
	}
	
	public void setItem(ItemStack item){
		this.item = item;
	}
	
}
