package es.minetsii.Splashoon.Weapons.SpecialWeapons;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitRunnable;

import es.minetsii.Splashoon.ArenaData;
import es.minetsii.Splashoon.Splashoon;
import es.minetsii.Splashoon.Builder.WeaponMethods;
import es.minetsii.Splashoon.Player.Inkling;
import es.minetsii.Splashoon.Weapons.Weapon;

public abstract class SpecialWeapon {

	private long duration;
	private Weapon weapon;
	private boolean active;
	
	
	public SpecialWeapon(long duration, Weapon weapon){
		this.duration = duration;
		this.weapon = weapon;
		this.active = false;
	}
	
	public abstract void onStart();
	
	public abstract void onFinish();
	
	public void execute(){
		onStart();
		active = true;
		weapon.getInkling().setInk(100);
		for(Inkling ikl : (weapon.getInkling().isBlue() ? ArenaData.teamblue : ArenaData.teamorange)){
			ikl.sendMessage(ChatColor.GREEN+weapon.getInkling().getPlayer().getName()
					+ChatColor.AQUA+" ha usado: "+ChatColor.RED+WeaponMethods.getSPName(this));
		}
		new BukkitRunnable() {
			
			@Override
			public void run() {
				onFinish();
				active = false;
			}
		}.runTaskLater(Splashoon.instance, duration);
	}
	
	public Weapon getWeapon(){
		return weapon;
	}
	
	public void setWeapon(Weapon weapon){
		this.weapon = weapon;
	}
	
	public boolean isActive(){
		return active;
	}
	
	public Inkling getInkling(){
		return weapon.getInkling();
	}
	
	public Location getLocation(){
		return getInkling().getLocation();
	}
}
