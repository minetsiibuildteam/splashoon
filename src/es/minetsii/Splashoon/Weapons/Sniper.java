package es.minetsii.Splashoon.Weapons;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import es.minetsii.ApETSII.Particles.ParticleEffect;
import es.minetsii.Splashoon.Player.Inkling;
import es.minetsii.Splashoon.Weapons.SpecialWeapons.SpecialWeapon;

public class Sniper extends MachineGun{

	
	
	public Sniper(ItemStack itemblue, ItemStack itemorange, 
			Inkling inkling, double inkused, long delay,
			double distance, double range, String name, 
			double damage, int radius, SpecialWeapon specialWeapon,
			String shootsound, String paintsound, String onPaint, String onHit, boolean vip) {
		super(itemblue, itemorange, inkling, inkused, delay, distance,
				range, name, damage, radius, specialWeapon, shootsound,
				paintsound, onPaint, onHit, vip);
		super.getSpecialWeapon().setWeapon(this);
	}

	public Sniper(Sniper gun) {
		super(gun);
		super.getSpecialWeapon().setWeapon(this);
	}

	public void showParticles(){
		Inkling ikl = super.getInkling();
		if(ikl.isInSuperJump() || ikl.isSquid() || 
				!ikl.getPlayer().getGameMode().equals(GameMode.ADVENTURE) || 
				!ikl.getPlayer().getItemInHand().isSimilar(super.getItem())) return;
		Location l = super.getInkling().getLocation().clone().add(0,1,0);
		Vector v = super.getInkling().getLocation().getDirection();
		ParticleEffect.CRIT.display(0, 0, 0, 0, 1, l, 10000);
		for(int i = 0; i < 50; i++){
			l.add(v);
			ParticleEffect.CRIT.display(0, 0, 0, 0, 1, l, 10000);
			if(!l.getBlock().getType().equals(Material.AIR)) break;
		}
	}
	
	public Weapon clone(){
		return new Sniper(this);
	}
}
