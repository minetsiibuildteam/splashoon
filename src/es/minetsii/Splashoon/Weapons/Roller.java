package es.minetsii.Splashoon.Weapons;

import java.util.List;
import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Egg;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import es.minetsii.APIs.Geometric;
import es.minetsii.APIs.SoundBuilder;
import es.minetsii.ApETSII.Titles.TitleAPI;
import es.minetsii.Splashoon.Splashoon;
import es.minetsii.Splashoon.Builder.WeaponMethods;
import es.minetsii.Splashoon.Player.Inkling;
import es.minetsii.Splashoon.Weapons.SpecialWeapons.SpecialWeapon;

@SuppressWarnings("unused")
public class Roller extends Weapon {

	private int		size;
	private long	startdelay;
	private int		balls;
	private double	ballsrange;
	private double	distance;
	private boolean	wait;
	private String 	onPaint;
	private String 	onHit;
	private String 	onBallPaint;
	private String 	onBallHit;
	private String 	ballshootsound;
	private String 	ballpaintsound;

	public Roller(ItemStack itemblue, ItemStack itemorange, Inkling inkling,
			double inkused, String name, double damage,
			int rollersize, int balls, double ballsrange, long startdelay,
			double distance, SpecialWeapon specialWeapon, String onPaint, String onHit,
			String onBallPaint, String onBalllHit, String ballpaintsound, String ballshootsound,
			boolean vip) {
		super(itemblue, itemorange, inkling, inkused, name, damage, specialWeapon, vip);
		this.size = rollersize;
		this.balls = balls;
		this.ballsrange = ballsrange;
		this.startdelay = startdelay;
		this.distance = distance;
		this.wait = false;
		this.onPaint = onPaint;
		this.onHit = onHit;
		this.onBallPaint = onBallPaint;
		this.onBallHit = onBalllHit;
		this.ballpaintsound = ballpaintsound;
		this.ballshootsound = ballshootsound;
		super.getSpecialWeapon().setWeapon(this);
	}
	
	public Roller(Roller gun){
		super(gun.getItemBlue(), gun.getItemOrange(), gun.getInkling(),
				gun.getInkused(), gun.getName(), gun.getDamage(), gun.getSpecialWeapon(), gun.isVip());
		this.size = gun.getSize();
		this.balls = gun.getBalls();
		this.ballsrange = gun.getBallsRange();
		this.startdelay = gun.getStartdelay();
		this.distance = gun.getBallsDistance();
		this.wait = false;
		this.onPaint = gun.getOnPaint();
		this.onHit = gun.getOnHit();
		this.onBallPaint = gun.getOnBallPaint();
		this.onBallHit = gun.getOnBallHit();
		this.ballpaintsound = gun.getBallPaintSound();
		this.ballshootsound = gun.getBallShootSound();
		super.getSpecialWeapon().setWeapon(this);
	}

	@Override
	public void use() {
		if (wait == true || super.isUsing() == true)
			return;
		Inkling ikl = super.getInkling();
		if (ikl.getInk() <= 0) {
			TitleAPI.sendTitle(ikl.getPlayer(), 5, 10, 5, "", ChatColor.RED + "Sin tinta");
			return;
		}
		wait = true;
		Location l = ikl.getLocation();
		ikl.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 100000, 2));
		new BukkitRunnable() {

			@Override
			public void run() {
				for (int i = 0; i < balls; i++) {
					if (ikl.isBlue())
						ikl.getPlayer().launchProjectile(Snowball.class, ikl.getPlayer().getLocation().getDirection()
								.multiply(distance).add(new Vector(RangeVector(), 0, RangeVector())));

					else
						ikl.getPlayer().launchProjectile(Egg.class, ikl.getPlayer().getLocation().getDirection()
								.multiply(distance).add(new Vector(RangeVector(), 0, RangeVector())));
					ikl.getPlayer().removePotionEffect(PotionEffectType.SLOW);
					new SoundBuilder(ballshootsound).playInLocation(ikl.getLocation(), 0.3F);
					setInUse();
					removeInk();
				}
			}
		}.runTaskLater(Splashoon.instance, startdelay);
	}
	
	public Weapon clone(){
		return new Roller(this);
	}

	public double RangeVector() {
		return (new Random().nextDouble() / 10) * (ballsrange / 10)
				- (new Random().nextDouble() / 10) * (ballsrange / 10);
	}

	public List<Location> getPaintBlocks() {
		Inkling ikl = super.getInkling();
		Player p = ikl.getPlayer();
		Location center = p.getLocation().clone().add(0, -1, 0).add(
				p.getLocation().getDirection().normalize().setY(0));
		return Geometric.spheres(center, size, 0, false, true, 0);
	}

	public void setInUse() {
		wait = false;
		super.setUsing(true);
	}

	public void removeInk() {
		if (super.getInkling().getInk() - super.getInkused() <= 0) {
			super.setUsing(false);
			super.getInkling().setInk(0);
		} else
			super.getInkling().removeInk(super.getInkused());
	}

	public void onPaint(PlayerMoveEvent e){
		WeaponMethods.onPaintRoller(this, e);
	}

	public void onHit(Player p){
		WeaponMethods.onRollerHit(p, this);
	}

	public void onBallHit(EntityDamageByEntityEvent e){
		WeaponMethods.onHitRollerBall(e, this);
	}

	public void onBallPaint(ProjectileHitEvent e){
		WeaponMethods.onPaintRollerBall(this, e);
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public long getStartdelay() {
		return startdelay;
	}

	public void setStartdelay(long startdelay) {
		this.startdelay = startdelay;
	}

	public boolean isWait() {
		return wait;
	}
	
	public int getBalls(){
		return balls;
	}
	
	public double getBallsRange(){
		return ballsrange;
	}
	
	public double getBallsDistance(){
		return distance;
	}
	
	public String getOnPaint(){
		return onPaint;
	}

	public String getOnHit(){
		return onHit;
	}
	
	public String getOnBallPaint(){
		return onBallPaint;
	}
	
	public String getOnBallHit(){
		return onBallHit;
	}
	
	public String getBallPaintSound(){
		return ballpaintsound;
	}
	
	public String getBallShootSound(){
		return ballshootsound;
	}
}
