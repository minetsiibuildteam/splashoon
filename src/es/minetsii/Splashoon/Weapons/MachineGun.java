package es.minetsii.Splashoon.Weapons;

import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Egg;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import es.minetsii.APIs.SoundBuilder;
import es.minetsii.ApETSII.Titles.TitleAPI;
import es.minetsii.Splashoon.Splashoon;
import es.minetsii.Splashoon.Builder.WeaponMethods;
import es.minetsii.Splashoon.Player.Inkling;
import es.minetsii.Splashoon.Weapons.SpecialWeapons.SpecialWeapon;

public class MachineGun extends Weapon {

	private long		delay;
	private double		distance;
	private double		range;
	private BukkitTask	task;
	private String 		shootsound;
	private String 		paintsound;
	private String		 onPaint;
	private String 		onHit;
	private int 		radius;

	public MachineGun(ItemStack itemblue, ItemStack itemorange, Inkling inkling, double inkused, long delay,
			double distance, double range, String name, double damage, int radius,
			SpecialWeapon specialWeapon, String shootsound, String paintsound,
			String onPaint, String onHit, boolean vip) {
		super(itemblue, itemorange, inkling, inkused, name, damage, specialWeapon, vip);
		this.delay = delay;
		this.range = range;
		this.distance = distance;
		this.shootsound = shootsound;
		this.onPaint = onPaint;
		this.onHit = onHit;
		this.radius = radius;
		this.paintsound = paintsound;
		super.getSpecialWeapon().setWeapon(this);
	}
	
	public MachineGun(MachineGun gun){
		super(gun.getItemBlue(), gun.getItemOrange(), gun.getInkling(),
				gun.getInkused(), gun.getName(), gun.getDamage(), gun.getSpecialWeapon(), gun.isVip());
		this.delay = gun.getDelay();
		this.range = gun.getRange();
		this.distance = gun.getDistance();
		this.shootsound = gun.getShootSound();
		this.paintsound = gun.getPaintSound();
		this.onPaint = gun.getOnPaint();
		this.onHit = gun.getOnHit();
		this.radius = gun.getRadius();
		super.getSpecialWeapon().setWeapon(this);
	}
	
	public Weapon clone(){
		return new MachineGun(this);
	}

	@Override
	public void use() {
		if(!getInkling().getPlayer().getGameMode().equals(GameMode.ADVENTURE)) return;
		Inkling ikl = super.getInkling();
		if (super.isUsing())
			return;
		if (ikl.getInk() <= 0) {
			TitleAPI.sendTitle(super.getInkling().getPlayer(), 5, 10, 5, "", ChatColor.RED + "Sin tinta");
			return;
		}
		super.setUsing(true);
		if (task != null)
			task.cancel();
		this.task = new BukkitRunnable() {
			@Override
			public void run() {
				if (ikl.getWeapon().isUsing()) {
					if (ikl.isBlue())
						ikl.getPlayer().launchProjectile(Snowball.class, ikl.getPlayer().getLocation().getDirection()
								.multiply(distance).add(new Vector(RangeVector(), 0, RangeVector())));
					else
						ikl.getPlayer().launchProjectile(Egg.class, ikl.getPlayer().getLocation().getDirection()
								.multiply(distance).add(new Vector(RangeVector(), 0, RangeVector())));
					new SoundBuilder(shootsound).playInLocation(ikl.getLocation(), 1);
					removeInk();
				} else
					cancel();
			}
		}.runTaskTimer(Splashoon.instance, delay, delay);
	}

	public double RangeVector() {
		return (new Random().nextDouble() / 10) * (range / 10) - (new Random().nextDouble() / 10) * (range / 10);
	}

	public void onHit(Player damager, Player damaged, double damage){
		WeaponMethods.onHitMachine(damaged, damage, this);
	}
	

	public void onPaint(ProjectileHitEvent e){
		WeaponMethods.onPaintMachine(this, e);
	}
	

	public void removeInk() {
		if (super.getInkling().getInk() - super.getInkused() <= 0) {
			super.setUsing(false);
			super.getInkling().setInk(0);
		} else
			super.getInkling().removeInk(super.getInkused());
	}
	
	public BukkitTask getTask(){
		return task;
	}
	
	public void setTask(BukkitTask task){
		this.task = task;
	}

	public double getDistance(){
		return distance;
	}
	
	public double getRange(){
		return range;
	}
	
	public long getDelay(){
		return delay;
	}
	
	public String getShootSound(){
		return shootsound;
	}
	
	public void setShootSound(String shootsound){
		this.shootsound = shootsound;
	}
	
	public void setOnPaint(String onPaint){
		this.onPaint = onPaint;
	}
	
	public String getOnPaint(){
		return onPaint;
	}
	
	public void setOnHit(String onHit){
		this.onHit = onHit;
	}
	
	public String getOnHit(){
		return onHit;
	}
	
	public int getRadius(){
		return radius;
	}
	
	public void setRadius(int radius){
		this.radius = radius;
	}
	
	public String getPaintSound(){
		return paintsound;
	}
	
	public void setPaintSound(String paintsound){
		this.paintsound = paintsound;
	}
	
	
	
}
