package es.minetsii.Splashoon.Weapons;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.bukkit.ChatColor;

import es.minetsii.APIs.ListUtils;
import es.minetsii.Splashoon.Builder.WeaponBuilder;
import es.minetsii.Splashoon.Player.Inkling;

public final class WeaponUtils {

	private static Map<Integer, Weapon> weaponList;

	public static void generateWeaponList() {
		weaponList = WeaponBuilder.loadWeapons();
	}

	public static Weapon stringToWeapon(String s) {
		
		Optional<Weapon> opt = weaponList.values().stream()
				.filter(w -> w.getName().equalsIgnoreCase(s)).findFirst();
		if(opt.isPresent())
			return opt.get().clone();
		
		return weaponList.values().stream().findFirst().get().clone();
	}

	public static Map<Integer, Weapon> getWeaponList() {
		Map<Integer, Weapon> map = new HashMap<Integer, Weapon>();
		for(Integer i : weaponList.keySet()){
			map.put(i, weaponList.get(i).clone());
		}
		return weaponList;
	}

	public static Set<Weapon> getAllWeapons() {
		Set<Weapon> set = new HashSet<Weapon>();
		weaponList.values().stream().forEach(w -> set.add(w.clone()));
		return set;
	}

	public static Set<Weapon> converterWeaponString(List<String> list) {
		Set<Weapon> nList = new HashSet<Weapon>();
		list.stream().forEach(w -> nList.add(stringToWeapon(w)));
		return nList;
	}

	public static Set<Weapon> converterWeaponString(Set<String> list) {
		Set<Weapon> nList = new HashSet<Weapon>();
		list.stream().forEach(w -> nList.add(stringToWeapon(w)));
		return nList;
	}

	public static Set<Weapon> converterWeaponInt(List<Integer> list) {
		Set<Weapon> nList = new HashSet<Weapon>();
		list.stream().forEach(w -> nList.add(getWeaponList().get(w)));
		return nList;
	}

	public static Set<Weapon> converterWeaponInt(Set<Integer> list) {
		Set<Weapon> nList = new HashSet<Weapon>();
		list.stream().forEach(w -> nList.add(getWeaponList().get(w)));
		return nList;
	}

	public static Set<String> converterString(List<Weapon> list) {
		Set<String> nList = new HashSet<>();
		list.stream().forEach(w -> nList.add(w.getName()));
		return nList;
	}

	public static Set<String> converterString(Set<Weapon> list) {
		Set<String> nList = new HashSet<>();
		list.stream().forEach(w -> nList.add(w.getName()));
		return nList;
	}

	public static Set<Integer> converterInt(List<Weapon> list) {
		Set<Integer> nList = new HashSet<>();
		list.stream().forEach(w -> nList.add(getIdByWeapon(w)));
		return nList;
	}

	public static Set<Integer> converterInt(Set<Weapon> list) {
		Set<Integer> nList = new HashSet<>();
		list.stream().forEach(w -> nList.add(getIdByWeapon(w)));
		return nList;
	}

	public static Weapon getByItemName(String s) {
		for (Weapon w : getAllWeapons()) {
			if (ChatColor.stripColor(w.getItemBlue().getItemMeta().getDisplayName())
					.equalsIgnoreCase(ChatColor.stripColor(s)))
				return w;
		}
		return weaponList.values().stream().findFirst().get().clone();
	}

	public static int getIdByWeapon(Weapon w) {
		Optional<Integer> res = ListUtils.getKeysByValue(weaponList, w).stream().findFirst();
		if (res.isPresent())
			return res.get();
		return 0;
	}

	public static Set<Weapon> getWeaponInv(Inkling ikl) {
		Set<Integer> set = new HashSet<Integer>();
		for (String s : ikl.getMPlayer().getWeaponInv().split(";")) {
			if (StringUtils.isNumeric(s)) {
				set.add(new Integer(s));
			}
		}
		return WeaponUtils.converterWeaponInt(set);
	}
}
