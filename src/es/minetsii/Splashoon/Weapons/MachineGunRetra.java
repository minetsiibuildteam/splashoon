package es.minetsii.Splashoon.Weapons;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Egg;
import org.bukkit.entity.Snowball;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import es.minetsii.APIs.SoundBuilder;
import es.minetsii.ApETSII.Titles.TitleAPI;
import es.minetsii.Splashoon.Splashoon;
import es.minetsii.Splashoon.Player.Inkling;
import es.minetsii.Splashoon.Weapons.SpecialWeapons.SpecialWeapon;

public class MachineGunRetra extends MachineGun{

	private long firstdelay;
	
	public MachineGunRetra(ItemStack itemblue, ItemStack itemorange,
			Inkling inkling, double inkused, long delay,
			double distance, double range, String name, double damage,
			int radius, SpecialWeapon specialWeapon,
			String shootsound, String paintsound, String onPaint, String onHit, 
			long firstdelay, boolean vip) {
		
		super(itemblue, itemorange, inkling, inkused, delay, distance, range,
				name, damage, radius, specialWeapon,
				shootsound, paintsound, onPaint, onHit, vip);
		this.firstdelay = firstdelay;
		super.getSpecialWeapon().setWeapon(this);
	}
	
	public MachineGunRetra(MachineGunRetra gun) {
		super(gun.getItemBlue(), gun.getItemOrange(), gun.getInkling(),
				gun.getInkused(), gun.getDelay(), gun.getDistance(), gun.getRange(),
				gun.getName(), gun.getDamage(), gun.getRadius(), gun.getSpecialWeapon(),
				gun.getShootSound(), gun.getPaintSound(), gun.getOnPaint(), gun.getOnHit(),
				gun.isVip());
		this.firstdelay = gun.getFirstDelay();
		super.getSpecialWeapon().setWeapon(this);
	}
	
	public Weapon clone(MachineGunRetra gun) {
		return new MachineGunRetra(gun);
	}



	@Override
	public void use() {
		if(!getInkling().getPlayer().getGameMode().equals(GameMode.ADVENTURE)) return;
		Inkling ikl = super.getInkling();
		if (super.isUsing())
			return;
		if (ikl.getInk() <= 0) {
			TitleAPI.sendTitle(super.getInkling().getPlayer(), 5, 10, 5, "", ChatColor.RED + "Sin tinta");
			return;
		}
		super.setUsing(true);
		if (super.getTask() != null)
			super.getTask().cancel();
		final double distance = super.getDistance();
		final String shootsound = super.getShootSound();
		super.setTask(new BukkitRunnable() {
			@Override
			public void run() {
				if (ikl.getWeapon().isUsing()) {
					if (ikl.isBlue())
						ikl.getPlayer().launchProjectile(Snowball.class, ikl.getPlayer().getLocation().getDirection()
								.multiply(distance).add(new Vector(RangeVector(), 0, RangeVector())));
					else
						ikl.getPlayer().launchProjectile(Egg.class, ikl.getPlayer().getLocation().getDirection()
								.multiply(distance).add(new Vector(RangeVector(), 0, RangeVector())));
					new SoundBuilder(shootsound).playInLocation(ikl.getLocation(), 1);
					removeInk();
				} else
					cancel();
			}
		}.runTaskTimer(Splashoon.instance, firstdelay, super.getDelay()));
	}
	
	public long getFirstDelay(){
		return firstdelay;
	}
}
