package es.minetsii.Splashoon.Weapons;

import org.bukkit.ChatColor;
import org.bukkit.inventory.ItemStack;

import es.minetsii.APIs.SoundBuilder;
import es.minetsii.ApETSII.Titles.TitleAPI;
import es.minetsii.Splashoon.Player.Inkling;
import es.minetsii.Splashoon.Weapons.SpecialWeapons.SpecialWeapon;

public abstract class Weapon {

	
	private ItemStack itemblue;
	private ItemStack itemorange;
	private boolean using;
	private Inkling inkling;
	private double inkused;
	private double damage;
	private String name;
	private SpecialWeapon specialWeapon;
	private boolean vip;
	//En %
	private double specialcount;
	
	
	public Weapon(ItemStack itemblue,
			ItemStack itemorange, Inkling inkling, double inkused, String name, double damage,
			SpecialWeapon specialWeapon, boolean vip) {
		this.itemblue = itemblue;
		this.itemorange = itemorange;
		this.using = false;
		this.inkling = inkling;
		this.inkused = inkused;
		this.name = name;
		this.damage = damage;
		this.specialWeapon = specialWeapon;
		this.specialcount = 0;
		this.vip = vip;
	}
	public ItemStack getItem() {
		return inkling.isBlue() ? itemblue : itemorange;
	}
	public ItemStack getItemBlue() {
		return itemblue;
	}
	public void setItemBlue(ItemStack item) {
		this.itemblue = item;
	}
	public ItemStack getItemOrange() {
		return itemorange;
	}
	public void setItemOrange(ItemStack item) {
		this.itemorange = item;
	}
	public boolean isUsing() {
		return using;
	}
	public void setUsing(boolean using) {
		this.using = using;
	}
	
	public Inkling getInkling() {
		return inkling;
	}
	public void setInkling(Inkling inkling) {
		this.inkling = inkling;
	}
	
	public double getInkused() {
		return inkused;
	}
	
	public void setInkused(double inkused) {
		this.inkused = inkused;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getDamage() {
		return damage;
	}
	public void setDamage(double damage) {
		this.damage = damage;
	}
	
	public boolean isOnhand(){
		return inkling.getPlayer().getItemInHand().isSimilar(itemblue) ||
				inkling.getPlayer().getItemInHand().isSimilar(itemorange);
	}
	
	public SpecialWeapon getSpecialWeapon(){
		return specialWeapon;
	}
	
	
	public abstract void use();
	
	public abstract Weapon clone();
	
	public void addSpecialCount(double add){
		if(specialcount == 100 && add > 0) return;
		if(specialcount + add >= 100){
			specialcount = 100;
			inkling.equip();
			inkling.equipHair();
			TitleAPI.sendTitle(getInkling().getPlayer(),
					3, 5, 3, "", ChatColor.GOLD+"�Arma especial cargada!");
			new SoundBuilder("splashoon.specialweaponready").playForPlayer(getInkling().getPlayer());
			return;
		}
		this.specialcount += add;
	}
	
	public void clearSpecialCount(){
		this.specialcount = 0;
	}
	
	public void setSpecialCount(double set){
		if(set >= 100){
			set = 100;
			inkling.equip();
			inkling.equipHair();
			TitleAPI.sendTitle(getInkling().getPlayer(),
					3, 5, 3, "", ChatColor.GOLD+"�Arma especial cargada!");
			new SoundBuilder("splashoon.specialweaponready").playForPlayer(getInkling().getPlayer());
			return;
		}
		this.specialcount = set;
	}
	
	public boolean isSpecialReady(){
		return specialcount >= 100;
	}
	
	public double getSpecialCount(){
		return specialcount;
	}
	
	public boolean isVip(){
		return vip;
	}
	
	public void setVip(boolean vip){
		this.vip = vip;
	}
	
}
