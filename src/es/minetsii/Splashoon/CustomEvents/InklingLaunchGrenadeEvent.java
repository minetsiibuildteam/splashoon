package es.minetsii.Splashoon.CustomEvents;

import org.bukkit.entity.Entity;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import es.minetsii.Splashoon.Player.Inkling;

public class InklingLaunchGrenadeEvent extends Event implements Cancellable{

	private boolean cancelled;
	private static final HandlerList handlers = new HandlerList();
	private Inkling inkling;
	private Entity grenade;
	private double inkUsed;
	
	public InklingLaunchGrenadeEvent(Inkling inkling, Entity grenade, double inkUsed) {
		this.inkling = inkling;
		this.cancelled = false;
		this.grenade = grenade;
		this.inkUsed = inkUsed;
	}
	
	
	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
		
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}


	public Inkling getInkling() {
		return inkling;
	}


	public void setInkling(Inkling inkling) {
		this.inkling = inkling;
	}
	
	public Entity getGrenade(){
		return grenade;
	}
	
	public void setInkUsed(double inkUsed){
		this.inkUsed = inkUsed;
	}
	
	public double getInkUsed(){
		return inkUsed;
	}
}