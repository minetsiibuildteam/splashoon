package es.minetsii.Splashoon.CustomEvents;

import org.bukkit.Bukkit;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import es.minetsii.Splashoon.Player.Inkling;

public class InklingDamageEvent extends Event implements Cancellable{

	private boolean cancelled;
	private static final HandlerList handlers = new HandlerList();
	private Inkling inkling;
	private Inkling damager;
	private double damage;
	
	public InklingDamageEvent(Inkling inkling, double damage, Inkling damager) {
		this.inkling = inkling;
		this.damager = damager;
		this.cancelled = false;
		this.damage = damage;
		
	}
	
	
	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
		
	}

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}


	public Inkling getInkling() {
		return inkling;
	}


	public void setInkling(Inkling inkling) {
		this.inkling = inkling;
	}


	public Inkling getDamager() {
		return damager;
	}


	public void setDamager(Inkling damager) {
		this.damager = damager;
	}


	public double getDamage() {
		return damage;
	}


	public void setDamage(double damage) {
		this.damage = damage;
	}
	
	public void execute(){
		
		Bukkit.getServer().getPluginManager().callEvent(this);
		if(!cancelled){
			
			inkling.getPlayer().damage(damage);
			
		}	
	}
}