package es.minetsii.Splashoon.Setup;

import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import es.minetsii.Splashoon.ArenaData;
import es.minetsii.Splashoon.Objects.Region;

public class SetupEvent implements Listener{

	
	@EventHandler(priority = EventPriority.LOWEST)
	public void selection(PlayerInteractEvent e){
		Player p = e.getPlayer();
		if(ArenaData.configreg.contains(p) && 
				p.getItemInHand().getType().equals(Material.STICK)){
			if(e.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
				Region sel = ArenaData.region;
				sel.setLocs(null, p.getTargetBlock((Set<Material>)null, 6).getLocation());
				p.sendMessage(ChatColor.LIGHT_PURPLE+"Posici�n 1 colocada.");
				e.setCancelled(true);
			}
			else if(e.getAction().equals(Action.LEFT_CLICK_BLOCK)){
				Region sel = ArenaData.region;
				sel.setLocs(p.getTargetBlock((Set<Material>)null, 6).getLocation(), null);
				p.sendMessage(ChatColor.LIGHT_PURPLE+"Posici�n 2 colocada.");
				e.setCancelled(true);
			}
			ArenaData.saveConfig();
		}
	}
}
