package es.minetsii.Splashoon.Setup;

import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.TextComponent;

import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import es.minetsii.APIs.LocationString;
import es.minetsii.APIs.WorldUtils;
import es.minetsii.Splashoon.ArenaData;
import es.minetsii.Splashoon.Splashoon;

public class SetupCommand implements CommandExecutor{

	public boolean onCommand(CommandSender sender, Command command,
			String label, String[] args) {
		if(command.getName().equalsIgnoreCase("sssetup")){
			if(!(sender instanceof Player)){
				sender.sendMessage(ChatColor.RED+"Este comando es solo para jugadores!");
				return true;
			}
			if(!sender.hasPermission("Splashoon.admin")) return true;
			Player p = (Player) sender;
			if(args.length == 0){
				sendInfo(p);
				return true;
			}
			if(args.length == 1){
				switch(args[0].toLowerCase()){
				case "setlobby":
					Splashoon.config.set("Arena.lobby", LocationString.toString(p.getLocation()));
					ArenaData.lobby = p.getLocation();
					p.sendMessage(ChatColor.GREEN+"Lobby colocado!");
					ArenaData.saveConfig();
					break;
					
				case "setspawnorange":
				case "setorange":
					Splashoon.config.set("Arena.spawn.orange",
							LocationString.toString(p.getLocation()));
					ArenaData.spawnOrange = p.getLocation();
					p.sendMessage(ChatColor.GREEN+"Spawn del equipo naranja colocado!");
					ArenaData.saveConfig();
					break;
					
				case "setspawnblue":
				case "setblue":
					Splashoon.config.set("Arena.spawn.blue",
							LocationString.toString(p.getLocation()));
					ArenaData.spawnBlue = p.getLocation();
					p.sendMessage(ChatColor.BLUE+"Spawn del equipo azul colocado!");
					ArenaData.saveConfig();
					break;
					
				case "toggleregion":
				case "togglereg":
				case "editmode":
					toggleReg(p);
					break;
					
				case "save":
				case "saveworld":
					saveWorld(p);
					break;
					
				case "seeregion":
					seeRegion(p);
					break;
					
				case "tpworld":
				case "gotoworld":
					p.teleport(new Location(WorldUtils.getSplashoonWorld(), 0,
							WorldUtils.getSplashoonWorld().getHighestBlockYAt(
									new Location(WorldUtils.getSplashoonWorld(), 0, 0, 0)), 0));
					break;
					
				default:
					sendInfo(p);
					break;
				}
				
			}
			if(args.length == 2){
				switch(args[0].toLowerCase()){
				
				case "setminplayers":
					if(!StringUtils.isNumeric(args[1])){
						p.sendMessage(ChatColor.RED+"�N�mero inv�lido!");
					}
					ArenaData.minPlayers = new Integer(args[1]);
					p.sendMessage(ChatColor.AQUA+"N�mero m�nimo de jugadores colocado: "+args[1]);
					ArenaData.saveConfig();
					break;
					
				case "setmaxplayers":
					if(!StringUtils.isNumeric(args[1])){
						p.sendMessage(ChatColor.RED+"�N�mero inv�lido!");
					}
					ArenaData.maxPlayers = new Integer(args[1]);
					p.sendMessage(ChatColor.AQUA+"N�mero m�ximo de jugadores colocado: "+args[1]);
					ArenaData.saveConfig();
					break;
					
				case "setbungeelobby":
				case "setBL":
					ArenaData.bungeelobby = args[1];
					p.sendMessage(ChatColor.AQUA+"Bungeelobby colocado: "+args[1]);
					ArenaData.saveConfig();
					break;

				case "setmaxlevel":
					if(!StringUtils.isNumeric(args[1])){
						p.sendMessage(ChatColor.RED+"�N�mero inv�lido!");
					}
					Splashoon.maxLevel = new Integer(args[1]);
					p.sendMessage(ChatColor.AQUA+"Nivel m�xico colocado: "+args[1]);
					ArenaData.saveConfig();
					Splashoon.levelCurve.clear();
					Splashoon.loadLevels();
					
					
				default:
					sendInfo(p);
					break;
				}
			}
		}
		return true;
	}
	
	
	public void sendInfo(Player p){
		ChatColor g = ChatColor.GREEN;
		ChatColor r = ChatColor.RED;
		p.sendMessage(ChatColor.AQUA+"############"+ChatColor.GOLD+" SplaShoon "
		+ChatColor.AQUA+"############");
		p.sendMessage(ChatColor.GOLD+"Estado de la arena:");
		p.sendMessage(ChatColor.BLUE+" Es jugable: "+(!ArenaData.isSetup() ? r+"NO" : g+"SI"));
		p.sendMessage("");
		p.sendMessage(g+"Nivel m�ximo: "+Splashoon.maxLevel+"."
				+ " (/ssetup setmaxlevel)");
		
		p.sendMessage(g+"N�mero m�nimo de jugadores: "+ArenaData.minPlayers+"."
				+ " (/ssetup setMinPlayers)");
		p.sendMessage(g+"N�mero m�ximo de jugadores: "+ArenaData.maxPlayers+"."
				+ " (/ssetup setMaxPlayers)");
		p.sendMessage(g+"Bungeelobby: "+ArenaData.bungeelobby+"."
				+ " (/ssetup setbungeelobby)");
		if(ArenaData.isSetup()) return;
		p.sendMessage("");
		p.sendMessage("--> Tareas:");
		p.sendMessage(
				(ArenaData.spawnBlue != null ? g : r)+"Colocar el spawn del equipo azul. "
						+ "(/sssetup setblue)");
		p.sendMessage(
				(ArenaData.spawnOrange != null ? g : r)+"Colocar el spawn del equipo naranja. "
						+ "(/sssetup setorange)");
		p.sendMessage(
				(ArenaData.region != null ? g : r)+"Colocar la regi�n. (/sssetup togglereg)");
		p.sendMessage(
				(ArenaData.lobby != null ? g : r)+"Colocar el lobby. (/sssetup setlobby)");
	}
	
	public void toggleReg(Player p){
		if(ArenaData.configreg.contains(p))
			ArenaData.configreg.remove(p);
		else ArenaData.configreg.add(p);
		p.sendMessage(ChatColor.GREEN+"Selecci�n de regi�n activada/desactivada. "
				+ "Usa el palo!");
		ArenaData.saveConfig();
		if(ArenaData.region != null){
			ArenaData.world = ArenaData.region.getMin().getWorld();
			WorldUtils.saveArena();
		}
	}
	
	public void saveWorld(Player p){
		if(ArenaData.world == null){
			p.sendMessage(ChatColor.RED+"�Antes tienes que seleccionar una regi�n!");
			return;
		}
		WorldUtils.saveArena();
		p.sendMessage(ChatColor.GREEN+"�guardado!");
	}
	
	public void seeRegion(Player p){
		TextComponent tc = new TextComponent(ChatColor.AQUA+"Max: "+
				ArenaData.region.getMax().getX()+" "+ ArenaData.region.getMax().getY()+" "+
				ArenaData.region.getMax().getZ());
		tc.setClickEvent
		(new ClickEvent(Action.RUN_COMMAND, "/tp "+p.getName()+" "+
		ArenaData.region.getMax().getX()+" "+ ArenaData.region.getMax().getY()+" "+
				ArenaData.region.getMax().getZ()));
		p.spigot().sendMessage(tc);
		p.sendMessage("");
		TextComponent tc2 = new TextComponent(ChatColor.AQUA+"Min: "+
				ArenaData.region.getMin().getX()+" "+ ArenaData.region.getMin().getY()+" "+
				ArenaData.region.getMin().getZ());
		tc2.setClickEvent
		(new ClickEvent(Action.RUN_COMMAND, "/tp "+p.getName()+" "+
		ArenaData.region.getMin().getX()+" "+ ArenaData.region.getMin().getY()+" "+
				ArenaData.region.getMin().getZ()));
		p.spigot().sendMessage(tc2);
	}
}
