package es.minetsii.Splashoon;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.persistence.PersistenceException;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import com.avaje.ebean.EbeanServer;

import es.minetsii.APIs.Disguise;
import es.minetsii.APIs.LocationString;
import es.minetsii.APIs.WorldUtils;
import es.minetsii.APIs.CNB.FileDecoder;
import es.minetsii.APIs.CNB.Song;
import es.minetsii.APIs.CNB.SongPlaying;
import es.minetsii.APIs.DataBase.MPlayer;
import es.minetsii.APIs.language.LanguageUtils;
import es.minetsii.Splashoon.Commands.Leave;
import es.minetsii.Splashoon.GamePlay.ChooseEquip;
import es.minetsii.Splashoon.GamePlay.Events.ChatEvent;
import es.minetsii.Splashoon.GamePlay.Events.DEggEvents;
import es.minetsii.Splashoon.GamePlay.Events.EntitySpawn;
import es.minetsii.Splashoon.GamePlay.Events.FireEvent;
import es.minetsii.Splashoon.GamePlay.Events.OnInteract;
import es.minetsii.Splashoon.GamePlay.Events.OnPlayerDeath;
import es.minetsii.Splashoon.GamePlay.Events.OnPlayerInventory;
import es.minetsii.Splashoon.GamePlay.Events.OnPlayerMenu;
import es.minetsii.Splashoon.GamePlay.Events.OnPlayerMove;
import es.minetsii.Splashoon.GamePlay.Events.OnPlayerQuit;
import es.minetsii.Splashoon.GamePlay.Events.OnServerPing;
import es.minetsii.Splashoon.GamePlay.Events.WeatherEvent;
import es.minetsii.Splashoon.Grenades.FastGrenade;
import es.minetsii.Splashoon.Movement.InklingMov;
import es.minetsii.Splashoon.Movement.Shoot;
import es.minetsii.Splashoon.Objects.Region;
import es.minetsii.Splashoon.Player.Inkling;
import es.minetsii.Splashoon.Player.Join;
import es.minetsii.Splashoon.Setup.SetupCommand;
import es.minetsii.Splashoon.Setup.SetupEvent;
import es.minetsii.Splashoon.Weapons.Sniper;
import es.minetsii.Splashoon.Weapons.WeaponUtils;
import es.minetsii.Splashoon.Weapons.SpecialWeapons.Repeater;
import es.minetsii.Splashoon.Weapons.SpecialWeapons.Shield;
import es.minetsii.Splashoon.Weapons.SpecialWeapons.SpecialWeaponEvents;
import es.minetsii.Splashoon.enums.ArenaStatus;

public class Splashoon extends JavaPlugin {

	public static Splashoon			instance;
	public static FileConfiguration	config;

	// CNB
	public static Map<String, ArrayList<SongPlaying>>	playerslist		= new HashMap<String, ArrayList<SongPlaying>>();
	public Map<String, Byte>							playerVolume	= new HashMap<String, Byte>();
	public static List<String>							songList		= new ArrayList<>();
	public static File									songsFolder;
	public static boolean								songActive;
	public static EbeanServer							dataBase;

	public static int							maxLevel	= 20;
	public static Map<Integer, Integer>	levelCurve	= new HashMap<Integer, Integer>();

	@Override
	public void onEnable() {

		for (World w : Bukkit.getWorlds()) {
			for (Entity en : w.getEntities()) {
				if (!(en instanceof Player)) {
					en.remove();
				}
			}
		}

		dataBase = this.getDatabase();

		Bukkit.getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");

		instance = this;
		config = getConfig();

		LanguageUtils.loadLanguages();
		
		loadFolders();
		loadLists();
		// Comprobar si esta configurada
		loadConfig();
		if (ArenaData.isSetup())
			WorldUtils.regenArena();
		// Cargar datos
		loadConfig();
		WeaponUtils.generateWeaponList();
		loadLevels();

		WorldUtils.getSplashoonWorld();

		eventRegister();
		playerRegister();
		commandregister();
		loadSongs();
		clock();
		setupDatabase();
	}

	public static void loadLevels() {
		int res = 0;
		for (int i = 1; i <= maxLevel; i++) {
			res += 900 * (i - 1) + 750 * i;
			levelCurve.put(i, res);
		}
		levelCurve.put(0, 0);
	}

	@Override
	public void onDisable() {
		for(Entity en : WorldUtils.getSplashoonWorld().getEntities())
			en.remove();
		for (ArrayList<SongPlaying> ar : playerslist.values()) {
			for (SongPlaying sp : ar) {
				sp.remove();
			}
		}
	}

	public void loadFolders() {
		songsFolder = new File(getDataFolder(), "songs");
		if (!songsFolder.exists())
			songsFolder.mkdirs();
	}

	private void eventRegister() {
		PluginManager pm = getServer().getPluginManager();
		pm.registerEvents(new InklingMov(), this);
		pm.registerEvents(new Join(), this);
		pm.registerEvents(new Disguise(), this);
		pm.registerEvents(new Shoot(), this);
		pm.registerEvents(new SetupEvent(), this);
		pm.registerEvents(new OnPlayerDeath(), this);
		pm.registerEvents(new EntitySpawn(), this);
		pm.registerEvents(new OnPlayerQuit(), this);
		pm.registerEvents(new OnInteract(), this);
		pm.registerEvents(new OnPlayerMove(), this);
		pm.registerEvents(new OnPlayerInventory(), this);
		pm.registerEvents(new OnPlayerMenu(), this);
		pm.registerEvents(new FastGrenade(), this);
		pm.registerEvents(new OnServerPing(), this);
		pm.registerEvents(new ChatEvent(), this);
		pm.registerEvents(new DEggEvents(), this);
		pm.registerEvents(new WeatherEvent(), this);
		pm.registerEvents(new FireEvent(), this);
		pm.registerEvents(new ChooseEquip(), this);
		
		//SPECIAL WEAPONS
		pm.registerEvents(new SpecialWeaponEvents(), this);
		pm.registerEvents(new Repeater(null), this);
	}

	private void commandregister() {
		getCommand("sssetup").setExecutor(new SetupCommand());
		getCommand("leave").setExecutor(new Leave());
	}

	private void loadLists() {
		ArenaData.players = new HashMap<Player, Inkling>();
		ArenaData.configreg = new ArrayList<Player>();
		Disguise.disguises = new HashMap<Player, LivingEntity>();
		ArenaData.teamblue = new HashSet<Inkling>();
		ArenaData.teamorange = new HashSet<Inkling>();
		ChooseEquip.load();
	}

	public void playerRegister() {
		for (Player p : getServer().getOnlinePlayers())
			Join.register(p);
	}

	public void loadConfig() {
		ArenaData.setStatus(ArenaStatus.lobby);
		ArenaData.world = WorldUtils.getSplashoonWorld();
		if (config.contains("Arena.lobby"))
			ArenaData.lobby = LocationString.fromString(config.getString("Arena.lobby"));
		else
			ArenaData.lobby = null;
		if (config.contains("Arena.spawn.orange"))
			ArenaData.spawnOrange = LocationString.fromString(config.getString("Arena.spawn.orange"));
		else
			ArenaData.spawnOrange = null;
		if (config.contains("Arena.spawn.blue"))
			ArenaData.spawnBlue = LocationString.fromString(config.getString("Arena.spawn.blue"));
		else
			ArenaData.spawnBlue = null;
		if (config.contains("Arena.region")) {
			ArenaData.region = new Region(LocationString.fromString(config.getString("Arena.region.min")),
					LocationString.fromString(config.getString("Arena.region.max")));
		} else
			ArenaData.region = new Region(null, null);

		if (config.contains("Arena.minplayers")) {
			ArenaData.minPlayers = config.getInt("Arena.minplayers");
		} else {
			ArenaData.minPlayers = 6;
			config.set("Arena.minplayers", 6);
		}
		if (config.contains("Arena.maxplayers")) {
			ArenaData.maxPlayers = config.getInt("Arena.maxplayers");
		} else {
			ArenaData.maxPlayers = 8;
			config.set("Arena.maxplayers", 8);
		}
		if (config.contains("Arena.bungeelobby")) {
			ArenaData.bungeelobby = config.getString("Arena.bungeelobby");
		} else {
			ArenaData.bungeelobby = "lobby";
		}
		if (config.contains("Arena.maxlevel")) {
			maxLevel = config.getInt("Arena.maxlevel");
		} else {
			config.set("Arena.maxlevel", 20);
		}
	}

	private void loadSongs() {
		File[] listOfFiles = songsFolder.listFiles();
		for (int i = 0; i < listOfFiles.length; i++) {
			if (listOfFiles[i].isFile()) {
				if (listOfFiles[i].toString().endsWith(".nbs")) {
					songList.add(listOfFiles[i].toString());
				}
			}
		}
		if (songList.isEmpty())
			songActive = false;
		else
			songActive = true;
	}

	public static Song getRandomSong() {
		Collections.shuffle(Splashoon.songList);
		String songName = Splashoon.songList.get(0);
		File songFile = new File(songName);
		Song s = FileDecoder.parse(songFile);
		return s;
	}

	public void clock() {
		new BukkitRunnable() {

			@Override
			public void run() {
				WorldUtils.getSplashoonWorld().setTime(100);
				if ((ArenaData.status != ArenaStatus.lobby && ArenaData.status != ArenaStatus.waiting)
						&& Bukkit.getOnlinePlayers().size() <= 0){
					ArenaData.setStatus(ArenaStatus.lobby);
					Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "restart");
				}
				if(ArenaData.status == ArenaStatus.playing){
					Shoot.spawnBalls();
					for(Inkling ikl : ArenaData.players.values()){
						ikl.showExp();
						if(ikl.getWeapon() instanceof Sniper && ikl.getWeapon().isOnhand()){
							((Sniper)ikl.getWeapon()).showParticles();
						}
						if(ikl.getWeapon().getSpecialWeapon() instanceof Shield &&
								ikl.getWeapon().getSpecialWeapon().isActive()){
							((Shield)ikl.getWeapon().getSpecialWeapon()).showParticles();
						}
					}
				}

			}
		}.runTaskTimer(this, 0, 1);
	}

	private void setupDatabase() {
		try {
			getDatabase().find(MPlayer.class).findRowCount();
		} catch (PersistenceException ex) {
			System.out.println("Installing database for " + getDescription().getName() + " due to first time usage");
			installDDL();
		}
	}

	@Override
	public List<Class<?>> getDatabaseClasses() {
		List<Class<?>> list = new ArrayList<Class<?>>();
		list.add(MPlayer.class);
		return list;
	}
}
