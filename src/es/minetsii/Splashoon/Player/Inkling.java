package es.minetsii.Splashoon.Player;

import java.util.Comparator;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.entity.Squid;
import org.bukkit.inventory.ItemStack;

import es.minetsii.APIs.Disguise;
import es.minetsii.APIs.ItemBuilder;
import es.minetsii.APIs.SoundBuilder;
import es.minetsii.APIs.DataBase.MPlayer;
import es.minetsii.APIs.language.LanguageUtils;
import es.minetsii.Splashoon.ArenaData;
import es.minetsii.Splashoon.Splashoon;
import es.minetsii.Splashoon.Weapons.Weapon;
import es.minetsii.Splashoon.Weapons.WeaponUtils;
import es.minetsii.Splashoon.enums.ArenaStatus;

public class Inkling {

	private MPlayer		inkling;
	private boolean		blue;
	private boolean		squid;
	private boolean		inInk;
	private double		totalInk;
	private ItemStack	grenade;
	private Weapon		weapon;
	private int			points;
	private int			expToLevelUp;
	private boolean 	inSuperJump;
	private boolean 	dead;

	public Inkling(boolean squid, boolean blue, Player player) {
		this.squid = squid;
		this.blue = blue;
		this.points = 0;
		this.totalInk = 100;
		this.inInk = false;
		this.inSuperJump = false;
		this.dead = false;
		this.grenade = new ItemBuilder(Material.EXP_BOTTLE).name(ChatColor.RED + "Granada r�pida").item();
		inkling = Splashoon.dataBase.find(MPlayer.class)
				.where().ieq("player_uuid", player.getUniqueId().toString())
				.ieq("name", player.getName()).findUnique();

		if (inkling == null) {

			inkling = new MPlayer();
			inkling.setPlayer(player);
			inkling.setName(player.getName());
			inkling.setWeapon("Default");
			inkling.setGender(player.getUniqueId().hashCode() % 2 == 1 ? true : false);
			inkling.setPoints(0);
			inkling.setLevel(1);
			inkling.setExp(0);
			inkling.setWins(0);
			inkling.setKills(0);
			inkling.setHair("Default");
			inkling.setTShirt("Default");
			inkling.setBoots("Default");
			inkling.setWeaponInv("0");
			inkling.setTShirtInv("0");
			inkling.setBootsInv("0");
			inkling.setHairInv("0");
		}
		this.weapon = WeaponUtils.stringToWeapon(inkling.getWeapon());
		this.weapon.setInkling(this);
		this.expToLevelUp = Splashoon.levelCurve.get(inkling.getLevel())
				- Splashoon.levelCurve.get(inkling.getLevel() - 1);
		if (inkling.getLevel() > Splashoon.maxLevel) {
			setLevel(Splashoon.maxLevel);
			inkling.setExp(Splashoon.levelCurve.get(Splashoon.maxLevel));
		}
		if (inkling.getLevel() < 1) {
			setLevel(1);
		}
		if (inkling.getExp() < 0) {
			inkling.setExp(0);
		}
		Splashoon.dataBase.save(inkling);
	}

	public void addWins(long wins) {
		long ow = inkling.getWins();
		inkling.setWins(ow + wins);
	}

	public double getInk() {
		return totalInk;
	}

	public void setInk(double ink) {
		this.totalInk = ink;
	}

	public void removeInk(double ink) {
		this.totalInk -= ink;
	}

	public boolean isBlue() {
		return blue;
	}

	public void setBlue(boolean blue) {
		this.blue = blue;
	}

	public boolean isSquid() {
		return squid;
	}

	public void setSquid(boolean squid) {
		if (squid) {
			ArenaData.region.sendBlockChangeToFence(inkling.getPlayer());
			Disguise.disguisePlayer(inkling.getPlayer(), Squid.class, false);
		} else {
			ArenaData.region.sendBlockChangeToCarpet(inkling.getPlayer());
			Disguise.undisguisePlayer(inkling.getPlayer());
		}
		if(this.squid == false && squid == true){
			weapon.setUsing(false);
		}
		this.squid = squid;
	}

	public boolean isInInk() {
		return inInk;
	}

	public void setInInk(boolean inInk) {
		if (inInk != this.inInk && inInk == true) {
			new SoundBuilder("splashoon.inink").playInLocation(inkling.getPlayer().getLocation(), 0.3F);
		}
		this.inInk = inInk;
	}

	public short getShortColor() {
		return (blue) ? (short) 11 : (short) 1;
	}

	public byte getByteColor() {
		return (blue) ? (byte) 11 : (byte) 1;
	}

	public void addDBPoints(int points) {
		inkling.setPoints(inkling.getPoints() + points);
	}

	public void addPoints(int points) {
		this.points += points;
	}

	public Location getLocation() {
		return inkling.getPlayer().getLocation();
	}

	public void sendMessage(String message) {
		if(inkling.getPlayer().isOnline())
			inkling.getPlayer().sendMessage(message);
	}

	@SuppressWarnings("deprecation")
	public void equip(){
		if(!ArenaData.status.equals(ArenaStatus.playing)) return;
		Weapon w = WeaponUtils.stringToWeapon(inkling.getWeapon());
		w.setInkling(this);
		inkling.getPlayer().getInventory().setItem(0, w.getItem());
		equipGrenade();
		inkling.getPlayer().getInventory().setItem(8, 
				new ItemBuilder(Material.getMaterial(126))
				.name(ChatColor.GREEN+"Volver a la base")
				.data(blue ? 5 : 4).item());
		inkling.getPlayer().getInventory().setItem(7, 
				new ItemBuilder(Material.CHEST)
				.name(ChatColor.GREEN+"Super Saltos").item());
		if(weapon.isSpecialReady()){
			inkling.getPlayer().getInventory()
			.setItem(2, new ItemBuilder(Material.CLAY_BALL)
					.name(ChatColor.GOLD+"-> Arma especial <-").item());
		}
		else inkling.getPlayer().getInventory().setItem(2, new ItemStack(Material.AIR));
	}

	public void equipHair(){
		if (inkling.isGender())
			inkling.getPlayer().getInventory()
					.setHelmet(blue
							? new ItemBuilder(Material.CARPET).data(11).name(ChatColor.BLUE + "Pelo de inkling chico")
									.item()
							: new ItemBuilder(Material.CARPET).data(1).name(ChatColor.RED + "Pelo de inkling chico")
									.item());
		else
			inkling.getPlayer().getInventory()
					.setHelmet(blue
							? new ItemBuilder(Material.CARPET).data(9).name(ChatColor.BLUE + "Pelo de inkling chica")
									.item()
							: new ItemBuilder(Material.CARPET).data(14).name(ChatColor.RED + "Pelo de inkling chica")
									.item());
	}
	
	public void addInk(double ink) {
		if (this.totalInk + ink >= 100) {
			this.totalInk = 100;
		} else
			this.totalInk += ink;
	}

	public void addLevel(int level) {
		setLevel(inkling.getLevel() + level);
	}

	public void setLevel(int level) {
		inkling.setLevel(level);
	}

	public void addExp(long exp) {
		inkling.setExp(inkling.getExp() + exp);
	}

	public void addMoney(long money) {
		inkling.setMoney(inkling.getMoney() + money);
	}

	public void checkLevelUp() {
		int oldLevel = inkling.getLevel();
		int newLevel = levelToGet(oldLevel, totalExp());
		if (oldLevel >= newLevel)
			return;
		inkling.setExp(totalExp() - Splashoon.levelCurve.get(newLevel - 1));
		setLevel(newLevel);
		setExpToLevelUp(Splashoon.levelCurve.get(newLevel) - Splashoon.levelCurve.get(newLevel - 1));
		LanguageUtils.sendMessage("messages.gameplay.actions.levelUp", getPlayer(), 
				inkling.getLevel());
	}

	public int expForNextLevel() {
		return Splashoon.levelCurve.get(inkling.getLevel());
	}

	public long totalExp() {
		return Splashoon.levelCurve.get(inkling.getLevel() - 1) + inkling.getExp();
	}

	public int levelToGet(int oldLevel, long exp) {
		Optional<Integer> opt = Splashoon.levelCurve.entrySet().stream()
				.filter(entry -> entry.getValue() <= exp && entry.getKey() >= oldLevel).map(Map.Entry::getKey)
				.collect(Collectors.toSet()).stream().sorted(Comparator.reverseOrder()).findFirst();
		if (opt.isPresent())
			return opt.get() + 1;

		return oldLevel;
	}


	public void addKills(long kills) {
		inkling.setKills(inkling.getKills() + kills);
	}

	public ItemStack getGrenade() {
		return grenade;
	}

	public void setGrenade(ItemStack grenade) {
		this.grenade = grenade;

	}

	public void equipGrenade() {
		if(!ArenaData.status.equals(ArenaStatus.playing)) return;
		inkling.getPlayer().getInventory().setItem(1, grenade);
	}

	public MPlayer getMPlayer() {
		return inkling;
	}

	public Player getPlayer() {
		return inkling.getPlayer();
	}

	public Weapon getWeapon() {
		return weapon;
	}

	public int getPoints() {
		return points;
	}

	public int getExpToLevelUp() {
		return expToLevelUp;
	}

	public void setExpToLevelUp(int expToLevelUp) {
		this.expToLevelUp = expToLevelUp;
	}
	
	public boolean isMaxLevel(){
		return inkling.getLevel() == Splashoon.maxLevel;
	}
	
	public void showExp() {
		inkling.getPlayer().setLevel((int)totalInk);
		inkling.getPlayer().setExp((float)totalInk/100F);
	}
	
	public boolean isInSuperJump(){
		return inSuperJump;
	}
	
	public void setInSuperJump(boolean inSuperJump){
		this.inSuperJump = inSuperJump;
	}
	
	public boolean isDead(){
		return dead;
	}
	
	public void setDead(boolean dead){
		this.dead = dead;
	}
	
	public boolean equals(Object obj){
		if(!(obj instanceof Inkling)) return false;
		Inkling ikl = (Inkling)obj;
		return ikl.getPlayer().equals(this.getPlayer());
	}
}
