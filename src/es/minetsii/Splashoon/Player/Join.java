package es.minetsii.Splashoon.Player;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent.Result;
import org.bukkit.event.player.PlayerJoinEvent;

import es.minetsii.ApETSII.Items.ItemBuilder;
import es.minetsii.Splashoon.ArenaData;
import es.minetsii.Splashoon.GamePlay.OnCountDownStart;
import es.minetsii.Splashoon.enums.ArenaStatus;

public class Join implements Listener {

	@EventHandler
	public void allow(AsyncPlayerPreLoginEvent e){
		Player p = Bukkit.getPlayer(e.getName());
		if (!ArenaData.isSetup()) {
			if (p.hasPermission("splashoon.admin")) {
				return;
			} else {
				e.disallow(Result.KICK_OTHER ,"La arena no est� configurada.");
				return;
			}
		}
		if ((!ArenaData.status.equals(ArenaStatus.lobby) && !ArenaData.status.equals(ArenaStatus.waiting))) {
			if (!p.hasPermission("splashoon.admin"))
				e.disallow(Result.KICK_OTHER , "La arena ya est� en juego!");
			return;
		}
	}
	
	
	@EventHandler
	public void join(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		e.setJoinMessage(null);
		register(p);
		Bukkit.broadcastMessage(ChatColor.GREEN + p.getName() + " ha entrado!");
	}

	public static void register(Player p) {
		Inkling ink = new Inkling(false,true,p);
		ink.getWeapon().setInkling(ink);
		
		ink.equipHair();

		p.teleport(ArenaData.lobby);
		ArenaData.region.sendBlockChangeToCarpet(p);
		p.getInventory().clear();
		p.getInventory().setItem(0, ItemBuilder.name(Material.EMERALD, (short)0, 1,
				ChatColor.GREEN+"Selecci�n de equipos"));
		p.setGameMode(GameMode.ADVENTURE);
		ArenaData.players.put(p, ink);
		if (OnCountDownStart.detectMaxPlayers() && ArenaData.status.equals(ArenaStatus.lobby))
			OnCountDownStart.start();
	}
}
