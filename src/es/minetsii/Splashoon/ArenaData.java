package es.minetsii.Splashoon;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import es.minetsii.APIs.LocationString;
import es.minetsii.APIs.MaterialUtils;
import es.minetsii.Splashoon.Objects.DataBlock;
import es.minetsii.Splashoon.Objects.Region;
import es.minetsii.Splashoon.Player.Inkling;
import es.minetsii.Splashoon.enums.ArenaStatus;

public class ArenaData {

	//DATA :D
	public static Region region;
	public static Map<Player, Inkling> players;
	public static int minPlayers;
	public static int maxPlayers;
	public static double bluepoints;
	public static double orangepoints;
	public static List<Player> configreg;
	public static ArenaStatus status;
	public static Location spawnBlue;
	public static Location spawnOrange;
	public static Location lobby;
	public static int countdown;
	public static String bungeelobby;
	public static World world;
	public static Set<Inkling> teamblue;
	public static Set<Inkling> teamorange;
	
	public static boolean isSetup(){
		return spawnBlue != null && spawnOrange != null && region != null && lobby != null;
	}
	
	public static void saveConfig(){
		FileConfiguration c = Splashoon.config;
		if(region != null){
			if(region.getMin() != null)
			c.set("Arena.region.min", LocationString.toString(region.getMin()));
			if(region.getMax() != null)
			c.set("Arena.region.max", LocationString.toString(region.getMax()));
		}	
		if(lobby != null){
			c.set("Arena.lobby", LocationString.toString(lobby));
		}
		if(spawnOrange != null){
			c.set("Arena.spawn.orange", LocationString.toString(spawnOrange));
		}
		if(spawnBlue != null){
			c.set("Arena.spawn.blue", LocationString.toString(spawnBlue));
		}
		c.set("Arena.minplayers", minPlayers);
		c.set("Arena.maxplayers", maxPlayers);
		c.set("Arena.bungeelobby", bungeelobby);
		c.set("Arena.maxlevel", Splashoon.maxLevel);
		Splashoon.instance.saveConfig();
	}
	
	@SuppressWarnings("deprecation")
	public static List<DataBlock> protectedBlocks(){
		List<DataBlock> list = new ArrayList<DataBlock>();
		list.add(new DataBlock(Material.CARPET));
		list.add(new DataBlock(Material.GLASS));
		list.add(new DataBlock(Material.STAINED_GLASS_PANE));
		list.add(new DataBlock(Material.WATER));
		list.add(new DataBlock(Material.STATIONARY_WATER));
		list.add(new DataBlock(Material.LAVA));
		list.add(new DataBlock(Material.STATIONARY_LAVA));
		list.add(new DataBlock(Material.STAINED_GLASS));
		list.add(new DataBlock(Material.FENCE_GATE));
		list.add(new DataBlock(Material.HOPPER));
		list.add(new DataBlock(Material.AIR));
		list.add(new DataBlock(Material.THIN_GLASS));
		list.add(new DataBlock(Material.BIRCH_FENCE_GATE));
		list.add(new DataBlock(Material.getMaterial(101)));
		list.add(new DataBlock(Material.ANVIL));
		list.add(new DataBlock(Material.IRON_BLOCK));
		list.add(new DataBlock(Material.STONE_BUTTON));
		list.add(new DataBlock(Material.WOOD_BUTTON));
		list.add(new DataBlock(Material.GOLD_BLOCK));
		list.add(new DataBlock(Material.BANNER));
		list.add(new DataBlock(Material.WALL_BANNER));
		list.add(new DataBlock(Material.SNOW));
		list.add(new DataBlock(Material.YELLOW_FLOWER)); 
		list.add(new DataBlock(Material.RED_ROSE));
		list.add(new DataBlock(Material.RED_MUSHROOM));
		list.add(new DataBlock(Material.BROWN_MUSHROOM));
		list.add(new DataBlock(Material.BARRIER));
		list.add(new DataBlock(Material.WOOD_STAIRS));
		list.add(new DataBlock(Material.GOLD_PLATE));
		list.add(new DataBlock(Material.IRON_PLATE));
		list.add(new DataBlock(Material.WOOD_PLATE));
		list.add(new DataBlock(Material.GLOWSTONE));
		list.add(new DataBlock(Material.STONE_PLATE));
		list.add(new DataBlock(Material.getMaterial(139)));
		list.add(new DataBlock(Material.REDSTONE));
		list.add(new DataBlock(Material.DIODE_BLOCK_OFF));
		list.add(new DataBlock(Material.DIODE_BLOCK_ON));
		list.add(new DataBlock(Material.REDSTONE_COMPARATOR_ON));
		list.add(new DataBlock(Material.REDSTONE_COMPARATOR_OFF));
		list.add(new DataBlock(Material.SLIME_BLOCK));
		list.add(new DataBlock(Material.RAILS));
		list.add(new DataBlock(Material.ACTIVATOR_RAIL));
		list.add(new DataBlock(Material.DETECTOR_RAIL));
		list.add(new DataBlock(Material.POWERED_RAIL));
		list.add(new DataBlock(Material.DRAGON_EGG));
		list.add(new DataBlock(Material.getMaterial(168),(byte)2));
		list.add(new DataBlock(Material.getMaterial(5),(byte)0));
		list.add(new DataBlock(Material.getMaterial(126),(byte)4));
		list.add(new DataBlock(Material.getMaterial(126),(byte)5));
		list.add(new DataBlock(Material.getMaterial(126),(byte)0));
		list.add(new DataBlock(Material.getMaterial(126),(byte)8));
		list.add(new DataBlock(Material.getMaterial(125),(byte)0));
		return list;
	}
	
	@SuppressWarnings("deprecation")
	public static List<DataBlock> blueBlocks(){
		List<DataBlock> list = new ArrayList<DataBlock>();
		list.add(new DataBlock(Material.WOOL, (byte)11));
		list.add(new DataBlock(Material.SPRUCE_WOOD_STAIRS));
		list.add(new DataBlock(Material.getMaterial(126), (byte)1));
		list.add(new DataBlock(Material.DARK_OAK_STAIRS));
		list.add(new DataBlock(Material.getMaterial(126),(byte)5));
		list.add(new DataBlock(Material.SPRUCE_FENCE));
		return list;
	}
	
	@SuppressWarnings("deprecation")
	public static List<DataBlock> orangeBlocks(){
		List<DataBlock> list = new ArrayList<DataBlock>();
		list.add(new DataBlock(Material.WOOL, (byte)1));
		list.add(new DataBlock(Material.RED_SANDSTONE_STAIRS));
		list.add(new DataBlock(Material.getMaterial(182)));
		list.add(new DataBlock(Material.ACACIA_STAIRS));
		list.add(new DataBlock(Material.ACACIA_FENCE));
		list.add(new DataBlock(Material.getMaterial(126),(byte)4));
		return list;
	}
	
	@SuppressWarnings("deprecation")
	public static void paintBlock(Inkling ikl, Block b){
		Material m = b.getType();
		if(m.equals(Material.AIR)) return;
		byte d = b.getData();
		if(detectMat(m, b.getData(), protectedBlocks())) return;
		if(ikl.isBlue()){
			if(detectMat(m, b.getData(), blueBlocks())) return;
			if(MaterialUtils.isSlab(m)){
				b.setType(Material.getMaterial(126));
				b.setData((byte)1);
			}
			else if(m.equals(Material.COBBLESTONE_STAIRS) || m.equals(Material.SPRUCE_WOOD_STAIRS)
					|| m.equals(Material.RED_SANDSTONE_STAIRS)){
				b.setType(Material.SPRUCE_WOOD_STAIRS);
				b.setData(d);
			}
			else if(MaterialUtils.isStair(m)){
				b.setType(Material.DARK_OAK_STAIRS);
				b.setData(d);
			}
			else if(MaterialUtils.isNormalFence(m)){
				b.setType(Material.SPRUCE_FENCE);
				b.setData(d);
			}
			else {
				b.setType(Material.WOOL);
				b.setData((byte)11);
			}
			region.addBlueBlock(b);
		}else{
			if(detectMat(m, b.getData(), orangeBlocks())) return;
			if(MaterialUtils.isSlab(m)){
				b.setType(Material.getMaterial(182));
			}
			else if(m.equals(Material.COBBLESTONE_STAIRS) || m.equals(Material.SPRUCE_WOOD_STAIRS)
					|| m.equals(Material.RED_SANDSTONE_STAIRS)){
				b.setType(Material.RED_SANDSTONE_STAIRS);
				b.setData(d);
			}
			else if(MaterialUtils.isStair(m)){
				b.setType(Material.ACACIA_STAIRS);
				b.setData(d);
			}
			else if(MaterialUtils.isNormalFence(m)){
				b.setType(Material.ACACIA_FENCE);
				b.setData(d);
			}
			else {
				b.setType(Material.WOOL);
				b.setData((byte)1);
			}
			region.addOrangeBlock(b);
		}
		ikl.addPoints(1);
		ikl.getWeapon().addSpecialCount(0.2D);
		if(ikl.getPlayer().hasPermission("splashoon.vip") && new Random().nextBoolean())
			ikl.addPoints(1);
	}
	
	private static boolean detectMat(Material m, byte b, List<DataBlock> l){
		for(DataBlock db : l){
			if(db.containsMaterial(m, b)) return true;
		}
		return false;
	}
	
	public static Inkling getInkling(Player p){
		return players.get(p);
	}
		
	public static void setStatus(ArenaStatus status){
		ArenaData.status = status;
	}
	
}
