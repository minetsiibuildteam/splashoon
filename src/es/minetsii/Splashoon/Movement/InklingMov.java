package es.minetsii.Splashoon.Movement;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Squid;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import es.minetsii.APIs.Disguise;
import es.minetsii.Splashoon.ArenaData;
import es.minetsii.Splashoon.CustomEvents.InklingDamageEvent;
import es.minetsii.Splashoon.Player.Inkling;
import es.minetsii.Splashoon.Weapons.Roller;
import es.minetsii.Splashoon.Weapons.Sniper;
import es.minetsii.Splashoon.enums.ArenaStatus;

public class InklingMov implements Listener{

	
	@EventHandler
	public void shift(PlayerToggleSneakEvent e){
		if(e.getPlayer().isSneaking() || !ArenaData.status.equals(ArenaStatus.playing) ||
				e.getPlayer().getGameMode().equals(GameMode.SPECTATOR)) return;
		
		Inkling ikl = ArenaData.players.get(e.getPlayer());	
		
		if(ikl.isInSuperJump()) return;
		
		if(!ikl.isSquid()){	

			ikl.setSquid(true);	
			
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					while(ikl.isSquid() && !ikl.isInSuperJump()){
						
						if(Methods.detectFloorBlocks(e.getPlayer())){
						ikl.addInk(1.2);
						}
						else {
							ikl.addInk(0.07);
							Methods.sendParticles(ikl);
						}
						try {
							Thread.sleep(50);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			}).start();
		}
		else{
			ikl.setSquid(false);
		}
	}
	
	@EventHandler
	public void move(PlayerMoveEvent e){
		e.getPlayer().setSprinting(true);
		e.getPlayer().setFallDistance(0);
		e.getPlayer().setFoodLevel(20);
		Inkling ikl = ArenaData.players.get(e.getPlayer());	
		e.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 200000, 2));
		if(ikl == null || !ArenaData.status.equals(ArenaStatus.playing)) return;
		if(ikl.getWeapon() instanceof Roller && ((Roller)ikl.getWeapon()).isWait()){
			return;
		}
		Player p = e.getPlayer();
		
		if(!p.getGameMode().equals(GameMode.SPECTATOR) &&
				(p.getLocation().getBlock().getType().equals(Material.WATER) || 
				p.getLocation().getBlock().getType().equals(Material.STATIONARY_WATER)) ||
				p.getLocation().getBlock().getType().equals(Material.LAVA) || 
				p.getLocation().getBlock().getType().equals(Material.STATIONARY_LAVA)){
			new InklingDamageEvent(ikl, 20, null).execute();;
		}
		
		if(ikl.isInSuperJump()) return;
		
		if(Methods.detectEnemyBlocks(p)){
			p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW,2000,3));
			p.addPotionEffect(new PotionEffect(PotionEffectType.POISON,2000,2));
			p.removePotionEffect(PotionEffectType.SPEED);
			if(Disguise.isPlayerDisguise(p))
				Disguise.getDisguise(p).removePotionEffect(PotionEffectType.INVISIBILITY);
			return;
		}else p.removePotionEffect(PotionEffectType.POISON);
		
		if(!ikl.isSquid()) {
			p.removePotionEffect(PotionEffectType.SPEED);
			p.removePotionEffect(PotionEffectType.POISON);
			p.removePotionEffect(PotionEffectType.SLOW);
			p.removePotionEffect(PotionEffectType.REGENERATION);
			if(ikl.getWeapon() instanceof Sniper && ikl.getWeapon().isOnhand()){
				p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW,10000,2));
			}
			else p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW,10000,0));
			ikl.setInInk(false);
			return;
		}
		
		if(Methods.detectBlocks(p) && p.getLocation().getDirection().getY() > 0.2D){
			Disguise.undisguisePlayer(p);
			if(!p.getLocation().getBlock().getLocation().clone().add(0,1,0).getBlock()
					.getType().equals(Material.STONE_BUTTON) &&
					!p.getLocation().getBlock().getLocation().clone().getBlock()
					.getType().equals(Material.STONE_BUTTON)){
				p.addPotionEffect(new PotionEffect(
						PotionEffectType.REGENERATION, 200000, 3));
				p.removePotionEffect(PotionEffectType.POISON);
				p.setVelocity(p.getVelocity().setY(p.getLocation().getDirection().getY()/2));
				p.addPotionEffect(new PotionEffect(
					PotionEffectType.INVISIBILITY, 200000, 0));
			}else{
				p.removePotionEffect(PotionEffectType.INVISIBILITY);
				p.setVelocity(p.getVelocity().setY(-0.4));
			}
			return;
		}
		else Disguise.disguisePlayer(e.getPlayer(),Squid.class,false);
		if(!((Entity)p).isOnGround()) {
			Disguise.getDisguise(p).removePotionEffect(PotionEffectType.INVISIBILITY);
			p.removePotionEffect(PotionEffectType.REGENERATION);
			return;
		}
		if(Methods.detectFloorBlocks(p)){
			ikl.setInInk(true);
			p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED,200000,0));
			p.addPotionEffect(new PotionEffect(
					PotionEffectType.REGENERATION, 200000, 3));
			p.removePotionEffect(PotionEffectType.SLOW);
			Disguise.getDisguise(p).addPotionEffect(new PotionEffect(
					PotionEffectType.INVISIBILITY, 200000, 0));
			Methods.sendParticles(ikl);
			
		}
		else {
			
			ikl.setInInk(false);
			p.removePotionEffect(PotionEffectType.POISON);
			p.removePotionEffect(PotionEffectType.REGENERATION);
			p.removePotionEffect(PotionEffectType.SPEED);
			p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW,2000,2));
			Disguise.getDisguise(p).removePotionEffect(PotionEffectType.INVISIBILITY);
		}
			
		
	}
	
	@EventHandler
	public void jump(PlayerMoveEvent e){
		
		if(e.getFrom().getY() < e.getTo().getY()){
			if(Disguise.isPlayerDisguise(e.getPlayer())){
				
				Disguise.getDisguise(e.getPlayer()).setVelocity(new Vector(0,0.3D,0));
			}
				
			
		}
	}
}
