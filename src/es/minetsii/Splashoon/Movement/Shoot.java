package es.minetsii.Splashoon.Movement;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.entity.Egg;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

import es.minetsii.APIs.Disguise;
import es.minetsii.APIs.Geometric;
import es.minetsii.APIs.WorldUtils;
import es.minetsii.Splashoon.ArenaData;
import es.minetsii.Splashoon.Splashoon;
import es.minetsii.Splashoon.Player.Inkling;
import es.minetsii.Splashoon.Weapons.MachineGun;
import es.minetsii.Splashoon.Weapons.Roller;
import es.minetsii.Splashoon.Weapons.Sniper;
import es.minetsii.Splashoon.enums.ArenaStatus;

public class Shoot implements Listener{
	
	
	public static Map<String, BukkitTask> shootMap;
	public static Set<Projectile> sniperBalls;
	
	public Shoot(){
		shootMap = new HashMap<String, BukkitTask>();
		sniperBalls = new HashSet<Projectile>();
	}
	
	@EventHandler(priority = EventPriority.LOWEST)
	public void shootEvent(PlayerInteractEvent e){
		Player p = e.getPlayer();
		if(ArenaData.status != ArenaStatus.playing) return;
		if(!(e.getAction().equals(Action.RIGHT_CLICK_AIR) ||
				e.getAction().equals(Action.RIGHT_CLICK_BLOCK))) return;
		Inkling ikl = ArenaData.players.get(e.getPlayer());
		if(p.getItemInHand() == null) return;
		if(p.getItemInHand().isSimilar(ikl.getWeapon().getItem())){
			shoot(ikl);
			
			
			
		}		
	}
	
	
	public void shoot(Inkling ikl){
		if(!ikl.getWeapon().isUsing())
			ikl.getWeapon().use();
		
		Disguise.undisguisePlayer(ikl.getPlayer());
		ikl.setSquid(false);
		
		if(shootMap.containsKey(ikl.getPlayer().getName())){
			shootMap.get(ikl.getPlayer().getName()).cancel();
			
		}
		
		long i = 10;
		if(ikl.getWeapon() instanceof Roller) i = ((Roller)ikl.getWeapon()).getStartdelay()+3;
		shootMap.put(ikl.getPlayer().getName(), new BukkitRunnable() {
			
			@Override
			public void run() {
				ikl.getWeapon().setUsing(false);
			}
		}.runTaskLater(Splashoon.instance, i));
	}
	
	@EventHandler
	public void paintEvent(ProjectileHitEvent e){
		if(e.getEntity().getShooter() instanceof Player){
			Inkling ikl = ArenaData.players.get((Player)e.getEntity().getShooter());
			if(sniperBalls.contains(e.getEntity()) ||
					(e.getEntity().getCustomName() != null &&
					e.getEntity().getCustomName().equals("Second"))){
				for(Location l : Geometric.spheres(e.getEntity().getLocation(),
						((Sniper)ikl.getWeapon()).getRadius(), 0, false, true, 0)){
					ArenaData.paintBlock(ikl, l.getBlock());
				}
			}
			if(ikl.getWeapon() instanceof MachineGun)
				((MachineGun)ikl.getWeapon()).onPaint(e);
			else if(ikl.getWeapon() instanceof Roller)
				((Roller)ikl.getWeapon()).onBallPaint(e);
			}
		}
	
	@EventHandler
	public void hitEvent(EntityDamageByEntityEvent e){
		if(e.getDamager() instanceof Player){
			e.setCancelled(true);
			return;
		}
		if(e.getDamager() instanceof Egg || e.getDamager() instanceof Snowball){
			Projectile pj = (Projectile) e.getDamager();
			if(sniperBalls.contains(pj) ||
					(pj.getCustomName() != null && pj.getCustomName().equals("Second"))){
				e.setCancelled(true);
				return;
			}
			Player p;
			if(e.getEntity() instanceof Player)
				p = (Player) e.getEntity();
			else if (Disguise.getPlayer(e.getEntity()) != null) 
				p = Disguise.getPlayer(e.getEntity());
			else return;
			Inkling ikl = ArenaData.players.get((Player)pj.getShooter());
			if(ArenaData.players.get(p).isBlue() == ikl.isBlue()){
				e.setCancelled(true);
				return;
			}
			if(ikl.getWeapon() instanceof MachineGun)
				((MachineGun)ikl.getWeapon()).onHit(ikl.getPlayer(),p,0);
			else if(ikl.getWeapon() instanceof Roller)
				((Roller)ikl.getWeapon()).onBallHit(e);
			}
		}
	
	
	//SNIPER
	public static void spawnBalls(){
		for(Entity en : WorldUtils.getSplashoonWorld().getEntities()){
			if(en instanceof Snowball || en instanceof Egg){
				Projectile pr = (Projectile) en;
				if(sniperBalls.contains(pr) ||
						(en.getCustomName() != null && en.getCustomName().equals("Second"))) return;
				if(pr.getShooter() instanceof Player){
					Inkling ikl = ArenaData.getInkling((Player)pr.getShooter());
					if(!(ikl.getWeapon() instanceof Sniper)) return;
					boolean b = ikl.isBlue();
					Projectile p2 = b ? WorldUtils.getSplashoonWorld().spawn(pr.getLocation().clone()
							.add(0,-0.2,0),
							Snowball.class) : WorldUtils.getSplashoonWorld().spawn(pr.getLocation(),
									Egg.class);
					sniperBalls.add(pr);
					p2.setCustomName("Second");
					p2.setVelocity(new Vector(0,-4,0));
					p2.setShooter(ikl.getPlayer());
				}
			}
		}
	}
}


