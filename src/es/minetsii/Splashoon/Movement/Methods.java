package es.minetsii.Splashoon.Movement;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import es.minetsii.ApETSII.Particles.ColoredParticle;
import es.minetsii.Splashoon.ArenaData;
import es.minetsii.Splashoon.Objects.DataBlock;
import es.minetsii.Splashoon.Player.Inkling;

public class Methods {

	
	public static void sendParticles(Inkling ikl){
		Player p = ikl.getPlayer();
		if(ikl.isBlue()) ColoredParticle.MOB_SPELL.send(p.getLocation(), 
				new ArrayList<Player>(Bukkit.getOnlinePlayers()), 0, 0, 255);
		else ColoredParticle.MOB_SPELL.send(p.getLocation(), 
				new ArrayList<Player>(Bukkit.getOnlinePlayers()), 255, 149, 35);
	}
	
	public static void sendSuperJumpParticles(Inkling ikl, Location location){
		if(ikl.isBlue()) ColoredParticle.RED_DUST.send(location, 
				new ArrayList<Player>(Bukkit.getOnlinePlayers()), 0, 0, 255);
		else ColoredParticle.RED_DUST.send(location, 
				new ArrayList<Player>(Bukkit.getOnlinePlayers()), 255, 149, 35);
	}
	
	@SuppressWarnings("deprecation")
	public static boolean detectBlocks(Player p){	
		
		Location l = p.getLocation().clone();
		List<DataBlock> list = (ArenaData.players.get(p).isBlue()) ? ArenaData.blueBlocks() :
			ArenaData.orangeBlocks();	
		
		for(DataBlock db : list){
			Material m = db.getMaterial();
			byte data = db.getBite();
			
			if(db.hasByte()){
				if ((l.clone().add(0,-0,0)
						.getBlock().getType().equals(m) &&
						l.clone().add(0,-0,0)
						.getBlock().getData() == data) ||
						(l.clone().add(0.5,-0,0)
						.getBlock().getType().equals(m) &&
						l.clone().add(0.5,-0,0)
						.getBlock().getData() == data) ||
						(l.clone().add(-0.5,-0,0)
								.getBlock().getType().equals(m) &&
								l.clone().add(-0.5,-0,0)
								.getBlock().getData() == data) ||
								(l.clone().add(0,-0,0.5)
										.getBlock().getType().equals(m) &&
										l.clone().add(0,-0,0.5)
										.getBlock().getData() == data) ||
										(l.clone().add(0,-0,-0.5)
												.getBlock().getType().equals(m) &&
												l.clone().add(0,-0,-0.5)
												.getBlock().getData() == data)) return true;
			}
			else if ((l.clone().add(0,-0,0)
					.getBlock().getType().equals(m)) ||
					(l.clone().add(0.5,-0,0)
							.getBlock().getType().equals(m)) ||
							(l.clone().add(-0.5,-0,0)
									.getBlock().getType().equals(m)) ||
									(l.clone().add(0,-0,0.5)
											.getBlock().getType().equals(m)) ||
											(l.clone().add(0,-0,-0.5)
													.getBlock().getType().equals(m)))
				return true;
		}
		return false;
	}
	
	@SuppressWarnings("deprecation")
	public static boolean detectFloorBlocks(Player p){	
		
		Location l = p.getLocation().clone();
		List<DataBlock> list = (ArenaData.players.get(p).isBlue()) ? ArenaData.blueBlocks() :
			ArenaData.orangeBlocks();	
		
		for(DataBlock db : list){
			Material m = db.getMaterial();
			byte data = db.getBite();
			
			if(db.hasByte()){
				if ((l.clone().add(0,-0.5,0)
						.getBlock().getType().equals(m) &&
						l.clone().add(0,-0.5,0)
						.getBlock().getData() == data) ||
						(l.clone().add(0.5,-0.5,0)
						.getBlock().getType().equals(m) &&
						l.clone().add(0.5,-0.5,0)
						.getBlock().getData() == data) ||
						(l.clone().add(-0.5,-0.5,0)
								.getBlock().getType().equals(m) &&
								l.clone().add(-0.5,-0.5,0)
								.getBlock().getData() == data) ||
								(l.clone().add(0,-0.5,0.5)
										.getBlock().getType().equals(m) &&
										l.clone().add(0,-0.5,0.5)
										.getBlock().getData() == data) ||
										(l.clone().add(0,-0.5,-0.5)
												.getBlock().getType().equals(m) &&
												l.clone().add(0,-0.5,-0.5)
												.getBlock().getData() == data)) return true;
			}
			else if ((l.clone().add(0,-0.5,0)
					.getBlock().getType().equals(m)) ||
					(l.clone().add(0.5,-0.5,0)
							.getBlock().getType().equals(m)) ||
							(l.clone().add(-0.5,-0.5,0)
									.getBlock().getType().equals(m)) ||
									(l.clone().add(0,-0.5,0.5)
											.getBlock().getType().equals(m)) ||
											(l.clone().add(0,-0.5,-0.5)
													.getBlock().getType().equals(m)))
				return true;
		}
		return false;
	}
	
	@SuppressWarnings("deprecation")
	public static boolean detectEnemyBlocks(Player p){	
		
		Location l = p.getLocation().clone();
		List<DataBlock> list = (!ArenaData.players.get(p).isBlue()) ? ArenaData.blueBlocks() :
			ArenaData.orangeBlocks();	
		
		for(DataBlock db : list){
			Material m = db.getMaterial();
			byte data = db.getBite();
			
			if(db.hasByte()){
				if ((l.clone().add(0,-0.5,0)
						.getBlock().getType().equals(m) &&
						l.clone().add(0,-0.5,0)
						.getBlock().getData() == data)) return true;
			}
			else if ((l.clone().add(0,-0.5,0)
					.getBlock().getType().equals(m)))
				return true;
		}
		return false;
	}
}
