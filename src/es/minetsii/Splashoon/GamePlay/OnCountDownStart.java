package es.minetsii.Splashoon.GamePlay;

import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import es.minetsii.APIs.SoundBuilder;
import es.minetsii.APIs.language.LanguageUtils;
import es.minetsii.Splashoon.ArenaData;
import es.minetsii.Splashoon.Splashoon;
import es.minetsii.Splashoon.enums.ArenaStatus;

public class OnCountDownStart {

	public static void start() {
		ArenaData.setStatus(ArenaStatus.waiting);
		ArenaData.countdown = 30;
		new BukkitRunnable() {
			@Override
			public void run() {
				if (!detectMaxPlayers()) {
					ArenaData.status = ArenaStatus.lobby;
					cancel();
					return;
				}
				ArenaData.countdown--;
				Scoreboards.getWaiting();
				for (Player p : ArenaData.players.keySet()) {
					p.setLevel(ArenaData.countdown);
					p.setExp((float)ArenaData.countdown / 30F);
					if (ArenaData.countdown == 20 || ArenaData.countdown == 10
							|| (ArenaData.countdown <= 5 && ArenaData.countdown > 0)) {
						LanguageUtils.sendMessage(
								"messages.gameplay.onLobby.startCountDown", p, ArenaData.countdown);
						new SoundBuilder(Sound.CLICK).playForPlayer(p);
					}
				}
				if (ArenaData.countdown == 0) {
					OnCountDownGameStart.start();
					cancel();
					return;
				}
			}
		}.runTaskTimer(Splashoon.instance, 0, 20);
	}

	public static boolean detectMaxPlayers() {
		return ArenaData.minPlayers <= ArenaData.players.size();
	}
}
