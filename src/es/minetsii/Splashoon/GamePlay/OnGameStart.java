package es.minetsii.Splashoon.GamePlay;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import es.minetsii.APIs.CNB.FileDecoder;
import es.minetsii.APIs.CNB.SetSongPlaying;
import es.minetsii.APIs.CNB.SongPlaying;
import es.minetsii.APIs.language.LanguageUtils;
import es.minetsii.ApETSII.Titles.TitleAPI;
import es.minetsii.Splashoon.ArenaData;
import es.minetsii.Splashoon.Splashoon;
import es.minetsii.Splashoon.Player.Inkling;
import es.minetsii.Splashoon.enums.ArenaStatus;

public class OnGameStart {

	private static SongPlaying song;
	
	
	public static void start(){
		
		ArenaData.setStatus(ArenaStatus.playing);
		ArenaData.countdown = 180;
		for(Inkling p : ArenaData.players.values()){
			p.equip();
			p.equipHair();
		}
		if(Splashoon.songActive){
			song =  new SetSongPlaying(Splashoon.getRandomSong());
			for(Player p : ArenaData.players.keySet())
				song.addPlayer(p);
			song.setPlaying(true);
		}
		new BukkitRunnable() {
			
			@Override
			public void run() {
				ArenaData.countdown--;
				Scoreboards.getPlaying();
				if(ArenaData.countdown % 60 == 0 && ArenaData.countdown != 0){
					for(Player p : ArenaData.players.keySet())
						if(ArenaData.countdown/60 != 1){
							TitleAPI.sendTitle
							(p, 5, 20, 5, ArenaData.countdown/60+"", 
									LanguageUtils.getMessage("messages.gameplay.onGame.minuteLeft", p));
						}
						else TitleAPI.sendTitle
						(p, 5, 20, 5, ArenaData.countdown/60+"", 
								LanguageUtils.getMessage("messages.gameplay.onGame.minuteLeft", p));
					if(ArenaData.countdown/60 == 1){
						if(Splashoon.songActive){
							File f = new File(Splashoon.instance.getDataFolder(),"lastmin.nbs");
							if(f.exists()){
							song.remove();
							song = new SetSongPlaying(FileDecoder
									.parse(f));
							song.setCustomDelay(41);
							for(Player p : ArenaData.players.keySet())
								song.addPlayer(p);
							song.setPlaying(true);
							}
						}
					}
				}
				if(ArenaData.countdown <= 10 && ArenaData.countdown > 0){
					for(Player p : ArenaData.players.keySet())
						TitleAPI.sendTitle
						(p, 5, 20, 5, ChatColor.GREEN+""+ArenaData.countdown, null);
				}
				if(ArenaData.countdown == 0 || Bukkit.getOnlinePlayers().size() <= 0){
					OnGameFinish.finish();
					for(Inkling ikl : ArenaData.players.values()){
						ikl.setSquid(false);
						TitleAPI.sendTitle
						(ikl.getPlayer(), 5, 20, 5, ChatColor.GREEN+"�Se acab�!", null);
					}
					cancel();
				}
			}
		}.runTaskTimer(Splashoon.instance, 0, 20);
		
	}
}
