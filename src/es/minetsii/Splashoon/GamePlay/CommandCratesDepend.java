package es.minetsii.Splashoon.GamePlay;

import java.util.Random;

import org.bukkit.entity.Player;

import com.gmail.gaelitoelquesito.CommandCrates.CommandCrates;

import es.minetsii.APIs.language.LanguageUtils;
import es.minetsii.Splashoon.Splashoon;

public class CommandCratesDepend {

	
	public static void addKeys(Player p) {
		if (Splashoon.instance.getServer().getPluginManager().getPlugin("CommandCrates") == null) {
			return;
		}
		if(new Random().nextInt(100) < (p.hasPermission("splashoon.vip") ? 40 : 20)){
			CommandCrates.getApi().addKeys(p, 1);
			p.sendMessage(LanguageUtils.getMessage("messages.gameplay.onFinish.keyObtained"));
		}
	}
}
