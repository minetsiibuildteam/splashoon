package es.minetsii.Splashoon.GamePlay.Events;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

import es.minetsii.Splashoon.ArenaData;
import es.minetsii.Splashoon.GamePlay.Scoreboards;

public class OnServerPing implements Listener{

	
	@EventHandler
	public void ping(ServerListPingEvent e){
		String name = null;
		switch (ArenaData.status) {
		case building:
			name = "�9REINICIANDO";
			break;
		case finished:
			name = "�cFINALZANDO";
			break;
		case playing:
			name = ChatColor.DARK_PURPLE+ChatColor.stripColor(Scoreboards.getTime(ArenaData.countdown));
			break;
		case setting:
			name = "�4REPARANDO";
			break;
		case lobby:
		case waiting:
			name = "�6EMPEZANDO";
			break;
		case waitingspawn:
			name = "�dJUGANDO";
			break;
		}
		e.setMotd(name);
	}
}
