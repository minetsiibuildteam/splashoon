package es.minetsii.Splashoon.GamePlay.Events;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import es.minetsii.APIs.SoundBuilder;
import es.minetsii.APIs.language.LanguageUtils;
import es.minetsii.ApETSII.Particles.ParticleEffect;
import es.minetsii.ApETSII.Titles.TitleAPI;
import es.minetsii.Splashoon.ArenaData;
import es.minetsii.Splashoon.Splashoon;
import es.minetsii.Splashoon.CustomEvents.InklingDamageEvent;
import es.minetsii.Splashoon.Player.Inkling;
import es.minetsii.Splashoon.enums.ArenaStatus;

public class OnPlayerDeath implements Listener{

	@EventHandler
	public void death(InklingDamageEvent e){
		Inkling ikl = e.getInkling();
		Player p = ikl.getPlayer();
		if(!ArenaData.status.equals(ArenaStatus.playing)) return;
		if(p.getGameMode().equals(GameMode.SPECTATOR)) return;
		if(e.getDamager() != null && 
				!e.getDamager().getPlayer().getGameMode().equals(GameMode.ADVENTURE))
			return;
		if(p.getHealth() - e.getDamage() <= 0){
			ikl.setSquid(false);
			ParticleEffect.CLOUD.display(0.3F, 0.3F, 0.3F, 0F, 30, p.getLocation(), 
					new ArrayList<Player>(ArenaData.players.keySet()));
			ikl.setInSuperJump(false);
			e.setCancelled(true);
			p.setGameMode(GameMode.SPECTATOR);
			ikl.getWeapon().setUsing(false);
			LanguageUtils.sendMessage("messages.gameplay.actions.atDeath", p);
			ikl.setDead(true);
			if(e.getDamager() == null)
				Bukkit.getOnlinePlayers().stream().forEach(pl ->
				LanguageUtils.sendMessage("messages.gameplay.actions.playerDeath", pl,
						(ikl.isBlue() ? ChatColor.BLUE : ChatColor.RED)+
						ikl.getPlayer().getName()));
			else Bukkit.getOnlinePlayers().stream().forEach(pl ->
			LanguageUtils.sendMessage("messages.gameplay.actions.playerKill", pl,
					(ikl.isBlue() ? ChatColor.BLUE : ChatColor.RED)+
					ikl.getPlayer().getName(), 
					(e.getDamager().isBlue() ? ChatColor.BLUE : ChatColor.RED)+
					e.getDamager().getPlayer().getName()));
			ikl.getWeapon().setUsing(false);
			if(e.getDamager() != null){
				e.getDamager().addKills(1);
				TitleAPI.sendActionBar(e.getDamager().getPlayer(),
						LanguageUtils.getMessage
						("messages.gameplay.actions.onKill", p, 
								(ikl.isBlue() ? ChatColor.BLUE : ChatColor.RED)+
								ikl.getPlayer().getName()));
				new SoundBuilder("splashoon.death").playInLocation(p.getLocation(), 0.3F);
				new SoundBuilder("splashoon.happy").playInLocation(e.getDamager().getLocation(), 0.3F);
			}
			else new SoundBuilder("splashoon.waterdeath").playInLocation(p.getLocation(), 0.3F);
		
			ParticleEffect.BARRIER.display(0, 0, 0, 0, 1, p.getLocation().clone().add(0,1,0), 1000);
			
			new BukkitRunnable() {
				
				@Override
				public void run() {
					if(!ArenaData.status.equals(ArenaStatus.playing)) return;
					p.setGameMode(GameMode.ADVENTURE);
					p.setHealth(20);
					Location l = ArenaData.players.get(p).isBlue() ? ArenaData.spawnBlue :
						ArenaData.spawnOrange;
					p.teleport(l);
					p.addPotionEffect(
							new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE,
									100, 4, false, false));
					ikl.setInk(100);
					ikl.equip();
					ikl.equipHair();
					ikl.setDead(true);
				}
			}.runTaskLater(Splashoon.instance, 100);
		}
		else new SoundBuilder("splashoon.hit").playInLocation(ikl.getLocation(), 0.3F);
	}
	
	@EventHandler
	public void hit(EntityDamageEvent e){
		
		if(e.getEntity() instanceof Player && e.getCause().equals(DamageCause.WITHER)){
			e.setCancelled(true);
			new InklingDamageEvent(ArenaData.getInkling((Player)e.getEntity()), 1, null).execute();
		}
	}
}
