package es.minetsii.Splashoon.GamePlay.Events;

import org.bukkit.entity.Egg;
import org.bukkit.entity.Snowball;
import org.bukkit.entity.Squid;
import org.bukkit.entity.ThrownExpBottle;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.ItemSpawnEvent;

import es.minetsii.Splashoon.ArenaData;
import es.minetsii.Splashoon.enums.ArenaStatus;

public class EntitySpawn implements Listener{

	@EventHandler
	public void spawn(CreatureSpawnEvent e){
		if(!(e.getEntity() instanceof Egg) && !(e.getEntity() instanceof Snowball)
				&& !(e.getEntity() instanceof Squid)  && !(e.getEntity() instanceof ThrownExpBottle))
				e.setCancelled(true);
		
	}
	
	@EventHandler
	public void item(ItemSpawnEvent e){
		if(ArenaData.status == ArenaStatus.playing) e.setCancelled(true);
	}
}
