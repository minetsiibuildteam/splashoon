package es.minetsii.Splashoon.GamePlay.Events;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import es.minetsii.APIs.Geometric;
import es.minetsii.Splashoon.ArenaData;
import es.minetsii.Splashoon.Player.Inkling;

public class DEggEvents implements Listener {

	public static final int BLAST_RADIUS = 5;

	@EventHandler
	public void CannonInteract(PlayerInteractEvent event) {
		if(!event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) return;
		if (event.getClickedBlock().getType().equals(Material.DRAGON_EGG)) {
			event.setCancelled(true);
			event.getClickedBlock().setType(Material.AIR);
			Inkling ikl = ArenaData.players.get(event.getPlayer());
			Geometric.spheres
			(event.getClickedBlock().getLocation(), 5, 0, false, true, 0).stream()
					.forEach(b -> ArenaData.paintBlock(ikl, b.getBlock()));
		}
	}

	@EventHandler
	public void disableTeleport(BlockFromToEvent event) {
		if (event.getBlock().getType().equals(Material.DRAGON_EGG)){
			event.setCancelled(true);
			event.getBlock().setType(Material.AIR);
		}
	}
}
