package es.minetsii.Splashoon.GamePlay.Events;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.scheduler.BukkitRunnable;

import es.minetsii.APIs.SkullBuilder;
import es.minetsii.APIs.language.LanguageUtils;
import es.minetsii.Splashoon.ArenaData;
import es.minetsii.Splashoon.Splashoon;
import es.minetsii.Splashoon.GamePlay.SuperJump;
import es.minetsii.Splashoon.Player.Inkling;

public class OnInteract implements Listener{

	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.HIGH)
	public void interact(PlayerInteractEvent e){
		if(e.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
		Material m = e.getClickedBlock().getType();
		if(!ArenaData.configreg.contains(e.getPlayer()) && 
				(m.equals(Material.DIODE_BLOCK_OFF) || 
						m.equals(Material.DIODE_BLOCK_ON) || 
						m.equals(Material.REDSTONE_COMPARATOR_OFF) || 
						m.equals(Material.REDSTONE_COMPARATOR_ON) ||
						m.equals(Material.CARPET) ||
						m.equals(Material.BIRCH_FENCE_GATE) ||
						m.equals(Material.FENCE_GATE) ||
						m.equals(Material.SPRUCE_FENCE_GATE) ||
						m.equals(Material.DISPENSER) ||
						m.equals(Material.DROPPER) ||
						m.equals(Material.ANVIL))) e.setCancelled(true);
		
		new BukkitRunnable() {
			@Override
			public void run() {
				if(ArenaData.players.get(e.getPlayer()).isSquid()){
					ArenaData.region.sendBlockChangeToFence(e.getPlayer());
				}
				else ArenaData.region.sendBlockChangeToCarpet(e.getPlayer());
				
			}
		}.runTaskLater(Splashoon.instance, 1);
		}
		if(!e.getAction().equals(Action.RIGHT_CLICK_AIR) 
				&& ! e.getAction().equals(Action.RIGHT_CLICK_AIR)) return;
		ItemStack item = e.getPlayer().getItemInHand();
		
		if(item == null) return;
		if(item.getType().equals(Material.getMaterial(126)))
			SuperJump.executeSuperJump(ArenaData.players.get(e.getPlayer()), 
					ArenaData.players.get(e.getPlayer()).isBlue() ? ArenaData.spawnBlue :
						ArenaData.spawnOrange);
		if(item.getType().equals(Material.CHEST)){
			e.getPlayer().openInventory(getSuperJumpInv(ArenaData.getInkling(e.getPlayer())));
		}
	}
	
	@EventHandler
	public void interactentity(PlayerInteractAtEntityEvent e){
		if(!(e.getRightClicked() instanceof Player)) e.setCancelled(true);
	}
	
	public Inventory getSuperJumpInv(Inkling ikl){
		Inventory inv = Bukkit.createInventory(null, 54, 
				LanguageUtils.getMessage("messages.gameplay.hotbar.superJumps", ikl.getPlayer()));
		int i = 0;
		for(Inkling equip : ArenaData.players.values()){
			if(equip.isBlue() == ikl.isBlue()){
				if(equip.equals(ikl)) continue;
				inv.setItem(i, new SkullBuilder().setOwner(equip.getPlayer().getName()).getSkull());
				i++;
			}
		}
		return inv;
	}
	
	@EventHandler
	public void invclick(InventoryClickEvent e){
		if(e.getCurrentItem() == null) return;
		ItemStack item = e.getCurrentItem();
		if(e.getCurrentItem().getType().equals(Material.SKULL_ITEM)){
			e.setCancelled(true);
			Player target = Bukkit.getPlayer(((SkullMeta)item.getItemMeta()).getOwner());
			if(!target.isOnline()) {
				for(Inkling ikl : ArenaData.players.values()){
					ikl.equip();
					ikl.equipHair();
				}
				return;
			}
			if(!target.getGameMode().equals(GameMode.ADVENTURE)){
				LanguageUtils.sendMessage("messages.gameplay.actions.cannotJump", 
						(Player)e.getWhoClicked());
				return;
			}
			SuperJump.executeSuperJump(ArenaData.players.get((Player)e.getWhoClicked()),
					ArenaData.players.get(target));
		}	
	}
}
