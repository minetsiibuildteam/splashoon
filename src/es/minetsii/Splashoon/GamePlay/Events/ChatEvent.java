package es.minetsii.Splashoon.GamePlay.Events;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import es.minetsii.APIs.language.LanguageUtils;
import es.minetsii.Splashoon.ArenaData;
import es.minetsii.Splashoon.Player.Inkling;
import es.minetsii.Splashoon.enums.ArenaStatus;

public class ChatEvent implements Listener {

	@EventHandler
	public void chatFormat(AsyncPlayerChatEvent event) {
		Player p = event.getPlayer();
		Inkling ink = ArenaData.players.get(p);

		String blue = LanguageUtils.getMessage("messages.chat.blue", p);
		String orange = LanguageUtils.getMessage("messages.chat.orange", p);
		String team = LanguageUtils.getMessage("messages.chat.team", p);
		
		String allMessage = ChatColor.DARK_GRAY + "["
				+ (ink.isBlue() ? ChatColor.BLUE + blue : ChatColor.GOLD + orange) + ChatColor.DARK_GRAY + "] "
				+ p.getDisplayName() + ChatColor.GRAY + ": " + ChatColor.WHITE + event.getMessage();

		if (ArenaData.status.equals(ArenaStatus.finished))
			event.setFormat(allMessage);
		else if (ArenaData.status.equals(ArenaStatus.waiting) || ArenaData.status.equals(ArenaStatus.lobby))
			event.setFormat(p.getDisplayName() + ": " + event.getMessage());
		else if (!event.getMessage().startsWith("!")) {
			String teamMessage = ChatColor.DARK_GRAY + "["
					+ (ink.isBlue() ? ChatColor.BLUE + team : ChatColor.GOLD + team) + ChatColor.DARK_GRAY
					+ "] " + p.getDisplayName() + ChatColor.GRAY + ": "
					+ (ink.isBlue() ? ChatColor.BLUE : ChatColor.GOLD) + event.getMessage();
			Set<Player> playersOnTeam = ArenaData.players.entrySet().stream()
					.filter(ps -> ps.getValue().isBlue() == ink.isBlue()).map(Map.Entry::getKey)
					.collect(Collectors.toSet());
			playersOnTeam.stream().forEach(ps -> ps.sendMessage(teamMessage));
			event.setCancelled(true);
		} else
			allMessage = ChatColor.DARK_GRAY + "["
					+ (ink.isBlue() ? ChatColor.BLUE + blue :
						ChatColor.GOLD + orange) + ChatColor.DARK_GRAY + "] "
					+ p.getDisplayName() + ChatColor.GRAY + ": " 
						+ ChatColor.WHITE + event.getMessage().substring(1, event.getMessage().length());
			event.setFormat(allMessage);
	}
}
