package es.minetsii.Splashoon.GamePlay.Events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import es.minetsii.APIs.Disguise;
import es.minetsii.Splashoon.ArenaData;
import es.minetsii.Splashoon.Player.Inkling;

public class OnPlayerQuit implements Listener{

	
	@EventHandler
	public void quit(PlayerQuitEvent e){
		ArenaData.players.remove(e.getPlayer());
		Disguise.undisguisePlayer(e.getPlayer());
		e.setQuitMessage(null);
		for(Inkling ikl : ArenaData.players.values()){
			ikl.equip();
			ikl.equipHair();
		}
	}
}
