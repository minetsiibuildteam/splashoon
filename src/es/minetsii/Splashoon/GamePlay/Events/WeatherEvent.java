package es.minetsii.Splashoon.GamePlay.Events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.WeatherChangeEvent;

public class WeatherEvent implements Listener{

	
	@EventHandler(ignoreCancelled = true, priority = EventPriority.LOW)
	public void onWeatherChange(WeatherChangeEvent event) {
		if (event.toWeatherState()) event.setCancelled(true);
	}
}
