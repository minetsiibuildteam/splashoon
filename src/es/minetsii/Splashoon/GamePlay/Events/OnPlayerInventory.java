package es.minetsii.Splashoon.GamePlay.Events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

import es.minetsii.Splashoon.ArenaData;

public class OnPlayerInventory implements Listener{

	@EventHandler
	public void drop(PlayerDropItemEvent e){
		if(ArenaData.configreg.contains(e.getPlayer())) return;
		e.setCancelled(true);
	}
}
