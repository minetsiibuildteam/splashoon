package es.minetsii.Splashoon.GamePlay.Events;

import org.bukkit.entity.Entity;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import es.minetsii.Splashoon.ArenaData;
import es.minetsii.Splashoon.Splashoon;
import es.minetsii.Splashoon.Player.Inkling;
import es.minetsii.Splashoon.Weapons.Roller;
import es.minetsii.Splashoon.enums.ArenaStatus;

public class OnPlayerMove implements Listener{

	
	@EventHandler
	public void move(PlayerMoveEvent e){
		if(ArenaData.status != ArenaStatus.playing) return;
		if(!ArenaData.players.containsKey(e.getPlayer())){
			ByteArrayDataOutput out = ByteStreams
					.newDataOutput();
			out.writeUTF("Connect");
			out.writeUTF(ArenaData.bungeelobby);
			e.getPlayer().sendPluginMessage(
					Splashoon.instance,
					"BungeeCord", out.toByteArray());
			return;
		}
		Inkling ikl = ArenaData.players.get(e.getPlayer());
		ikl.showExp();
		if(ikl.isInSuperJump()) return;
		ikl.equipGrenade();
		if(ikl.getWeapon() instanceof Roller){
			((Roller)ikl.getWeapon()).onPaint(e);
		}
		if(e.getTo().getBlock().getLocation().equals(e.getFrom().getBlock().getLocation())) return;
		for(Entity en : e.getPlayer().getWorld().getEntities()){
			if(en instanceof  ExperienceOrb) en.remove();
		}
		if(!ArenaData.region.inArea(e.getTo().getBlock().getLocation(), true)){
			if(ArenaData.configreg.contains(e.getPlayer())) return;
			ikl.getPlayer().teleport((ikl.isBlue()) ? ArenaData.spawnBlue : ArenaData.spawnOrange);
		}
	}
}
