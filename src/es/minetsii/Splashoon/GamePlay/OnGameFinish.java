package es.minetsii.Splashoon.GamePlay;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import es.minetsii.APIs.SoundBuilder;
import es.minetsii.APIs.language.LanguageUtils;
import es.minetsii.ApETSII.Titles.TitleAPI;
import es.minetsii.Splashoon.ArenaData;
import es.minetsii.Splashoon.Splashoon;
import es.minetsii.Splashoon.Player.Inkling;
import es.minetsii.Splashoon.enums.ArenaStatus;

public class OnGameFinish {

	public static void finish() {
		ArenaData.setStatus(ArenaStatus.finished);
		for (Player p : ArenaData.players.keySet()) {
			p.getInventory().clear();
			p.removePotionEffect(PotionEffectType.SLOW);
			p.removePotionEffect(PotionEffectType.SPEED);
			p.removePotionEffect(PotionEffectType.POISON);
			p.removePotionEffect(PotionEffectType.JUMP);
			p.removePotionEffect(PotionEffectType.REGENERATION);
		}
		ArenaData.bluepoints = 0;
		ArenaData.orangepoints = 0;
		new BukkitRunnable() {

			@Override
			public void run() {
				for (Player p : ArenaData.players.keySet()) {
					p.setGameMode(GameMode.ADVENTURE);
					p.teleport(ArenaData.lobby);
					p.setHealth(20);
					p.getInventory().clear();
					Scoreboards.getFinish();
				}
			}
		}.runTaskLater(Splashoon.instance, 100);

		new BukkitRunnable() {

			@Override
			public void run() {
				getWinner();
			}
		}.runTaskLater(Splashoon.instance, 120);
	}

	public static void getWinner() {
		for (int i = 0; i <= 40; i++) {
			new BukkitRunnable() {

				@Override
				public void run() {
					ArenaData.bluepoints += 5;
					ArenaData.orangepoints += 5;
					for (Player p : ArenaData.players.keySet()) {
						p.playSound(p.getLocation(), Sound.NOTE_BASS, 100, 100);
					}
					Scoreboards.getFinish();
				}
			}.runTaskLater(Splashoon.instance, i + (i / 8));
		}
		new BukkitRunnable() {

			@Override
			public void run() {
				ArenaData.bluepoints = ArenaData.region.getBluePoints();
				ArenaData.orangepoints = ArenaData.region.getOrangePoints();
				Scoreboards.getFinish();
				end(ArenaData.bluepoints >= ArenaData.orangepoints);
			}
		}.runTaskLater(Splashoon.instance, 60);
	}

	public static void end(boolean winBlue) {
		for (Inkling ikl : ArenaData.players.values()) {

			if (ikl.isBlue() == winBlue) {
				TitleAPI.sendTitle(ikl.getPlayer(), 5, 20, 5, 
						LanguageUtils.getMessage("messages.gameplay.onFinish.win"), null);
				new SoundBuilder("splashoon.win").playForPlayer(ikl.getPlayer());
				ikl.addWins(1);
				ikl.addPoints(300);
				TitleAPI.sendActionBar(ikl.getPlayer(), ChatColor.GOLD+"+300!");
				CommandCratesDepend.addKeys(ikl.getPlayer());
			} else {
				TitleAPI.sendTitle(ikl.getPlayer(), 5, 20, 5, 
						LanguageUtils.getMessage("messages.gameplay.onFinish.lose"), null);
				new SoundBuilder("splashoon.lose").playForPlayer(ikl.getPlayer());
			}
		}

		new BukkitRunnable() {

			@Override
			public void run() {
				for (Inkling ikl : ArenaData.players.values()) {
					int total = ikl.getPoints() / 2;
					LanguageUtils.sendMessage("messages.gameplay.onFinish.summary", 
							ikl.getPlayer(), total, total);
					ikl.addDBPoints(ikl.getPoints());
					ikl.addExp(total);
					ikl.addMoney(total);
					ikl.checkLevelUp();
					Splashoon.dataBase.save(ikl.getMPlayer());
				}
			}
		}.runTaskLater(Splashoon.instance, 100);

		new BukkitRunnable() {

			@Override
			public void run() {
				for (Player p : Bukkit.getOnlinePlayers()) {
					ByteArrayDataOutput out = ByteStreams.newDataOutput();
					out.writeUTF("Connect");
					out.writeUTF(ArenaData.bungeelobby);
					p.sendPluginMessage(Splashoon.instance, "BungeeCord", out.toByteArray());
				}
			}
		}.runTaskLater(Splashoon.instance, 200);
		
		new BukkitRunnable() {

			@Override
			public void run() {
				
				Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "restart");
				
			}
		}.runTaskLater(Splashoon.instance, 220);
	}
}
