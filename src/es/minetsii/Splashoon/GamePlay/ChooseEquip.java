package es.minetsii.Splashoon.GamePlay;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import es.minetsii.APIs.ListUtils;
import es.minetsii.APIs.language.LanguageUtils;
import es.minetsii.ApETSII.Items.ItemBuilder;
import es.minetsii.Splashoon.ArenaData;
import es.minetsii.Splashoon.Player.Inkling;
import es.minetsii.Splashoon.enums.ArenaStatus;

public class ChooseEquip implements Listener{

	public static Map<Inkling, Boolean> equips;
	
	public static void load(){
		equips = new HashMap<Inkling, Boolean>();
	}
	
	@EventHandler
	public void openMenu(PlayerInteractEvent e){
		if(!ArenaData.status.equals(ArenaStatus.waiting) &&
				!ArenaData.status.equals(ArenaStatus.lobby)) return;
		
		Inkling ikl = ArenaData.getInkling(e.getPlayer());
		if(!e.getAction().equals(Action.RIGHT_CLICK_AIR)
				&& !e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) return;
		if(ikl.getPlayer().getItemInHand() == null) return;
		if(ikl.getPlayer().getItemInHand().getType().equals(Material.EMERALD)){
			Inventory inv = Bukkit.createInventory(null, 9, 
					LanguageUtils.getMessage("messages.gameplay.onLobby.teamMenu.menuItem"));
			String[] lorear = LanguageUtils.getMessage
					("messages.gameplay.onLobby.teamMenu.menuLore", ikl.getPlayer()).split("\\n");
			ItemStack blue = ItemBuilder.nameLore(Material.WOOL, (short)11, 1,
					ListUtils.toArrayList(lorear),
					LanguageUtils.getMessage("messages.gameplay.onLobby.teamMenu.teamBlue"));
			ItemStack orange = ItemBuilder.nameLore(Material.WOOL, (short)1, 1,
					ListUtils.toArrayList(lorear), 
					LanguageUtils.getMessage("messages.gameplay.onLobby.teamMenu.teamOrange"));
			inv.setItem(0, blue);
			inv.setItem(8, orange);
			ikl.getPlayer().openInventory(inv);
		}
	}
	
	@EventHandler
	public void clickMenu(InventoryClickEvent e){
		e.setCancelled(true);
		if(!e.getInventory().getTitle().equalsIgnoreCase(
				LanguageUtils.getMessage("messages.gameplay.onLobby.teamMenu.menuItem"))) return;
		Player p = (Player) e.getWhoClicked();
		Inkling ikl = ArenaData.getInkling(p);
		if(e.getCurrentItem().getDurability() == 11){
			if(!canJoin(true)){
				LanguageUtils.sendMessage("messages.gameplay.onLobby.teamMenu.teamFull", p);
				return;
			}
			if(!equips.containsKey(ikl) && !p.hasPermission("splashoon.vip")){
				if(ikl.getMPlayer().getMoney() < 500){
					LanguageUtils.sendMessage("messages.gameplay.onLobby.teamMenu.noMoney", p);
					return;
				}
				ikl.getMPlayer().setMoney(ikl.getMPlayer().getMoney()-500);
				LanguageUtils.sendMessage("messages.gameplay.onLobby.teamMenu.removeCoins", p);
			}
			equips.put(ikl, true);
			p.playSound(p.getLocation(), Sound.NOTE_PLING, 100, 1);
		}
		else if(e.getCurrentItem().getDurability() == 1){
			if(!canJoin(false)){
				LanguageUtils.sendMessage("messages.gameplay.onLobby.teamMenu.teamFull", p);
				return;
			}
			if(!equips.containsKey(ikl) && !p.hasPermission("splashoon.vip")){
				if(ikl.getMPlayer().getMoney() < 500){
					LanguageUtils.sendMessage("messages.gameplay.onLobby.teamMenu.noMoney", p);
					return;
				}
				ikl.getMPlayer().setMoney(ikl.getMPlayer().getMoney()-500);
				LanguageUtils.sendMessage("messages.gameplay.onLobby.teamMenu.removeCoins", p);
			}
			equips.put(ikl, false);
			p.playSound(p.getLocation(), Sound.NOTE_PLING, 100, 1);
		}
	}
	
	public boolean canJoin(boolean blue){
		int i = (int) Math.ceil(new Double(ArenaData.players.size())/2);
		int bt = ArenaData.teamblue.size();
		int ot = ArenaData.teamorange.size();
		if(blue){
			if(bt >= i) return false;	
		}
		else if(ot >= i) return false;
		return true;
	}
}
