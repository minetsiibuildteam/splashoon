package es.minetsii.Splashoon.GamePlay;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import es.minetsii.APIs.Disguise;
import es.minetsii.APIs.SoundBuilder;
import es.minetsii.ApETSII.Titles.TitleAPI;
import es.minetsii.Splashoon.ArenaData;
import es.minetsii.Splashoon.Splashoon;
import es.minetsii.Splashoon.Movement.Methods;
import es.minetsii.Splashoon.Player.Inkling;
import es.minetsii.Splashoon.enums.ArenaStatus;


public class SuperJump implements Listener{
	
	private static List<Location> calculatelocations(Location from, Location toraw){
		
		List<Location> list = new ArrayList<Location>();
		Location to = toraw.clone();
		int max = 200;
		int i = 0;
		double y = 3;
		Vector v = to.toVector().subtract(from.toVector()).normalize();
		Location last = from.clone();
		while((!last.getBlock().getLocation().equals(to.getBlock().getLocation())
				&& !last.getBlock().getLocation().equals(
						to.getBlock().getLocation().clone().add(0,1,0))
				&& !last.getBlock().getLocation().equals(
						to.getBlock().getLocation().clone().add(0,-1,0)))
				&& i <= max){
			if(y > 0){
				v = to.toVector().subtract(last.toVector()).normalize();
				last = last.add(v).add(v.setY(y));
				list.add(last.clone());
				y-=0.05;
			}
			else{
				v = to.toVector().subtract(last.toVector()).normalize();
				last = last.add(v.multiply(1.4));
				list.add(last.clone());
			}
			i++;
		}
		return list;
	}
	
	public static void jump(final Player p, final Location to){
		final List<Location> list = calculatelocations(p.getLocation(), to);
		new BukkitRunnable() {	
			@Override
			public void run() {
				try{
					if(!ArenaData.players.get(p).isInSuperJump() ||
							!ArenaData.status.equals(ArenaStatus.playing)){
						cancel();
						return;
					}
					Methods.sendSuperJumpParticles(ArenaData.players.get(p), to);
					p.teleport(list.get(0).clone().setDirection(
						to.toVector().subtract(p.getLocation().toVector()).normalize()));
					list.remove(0);
					if(list.isEmpty()){
						cancel();
						p.setGameMode(GameMode.ADVENTURE);
						p.teleport(p.getLocation().add(0,1,0));
						ArenaData.players.get(p).setInSuperJump(false);
						p.getInventory().setHeldItemSlot(0);
						ArenaData.players.get(p).equip();
						new SoundBuilder("splashoon.finishjump").playInLocation(p.getLocation(), 0.3F);
					}
				}catch(IndexOutOfBoundsException ex){
					cancel();
					p.setGameMode(GameMode.ADVENTURE);
					p.teleport(p.getLocation().add(0,1,0));
					ArenaData.players.get(p).setInSuperJump(false);
					p.getInventory().setHeldItemSlot(0);
					ArenaData.players.get(p).equip();
					new SoundBuilder("splashoon.finishjump").playInLocation(p.getLocation(), 0.3F);
				}
			}	
		}.runTaskTimer(Splashoon.instance, 0, 1);
	}
	
	
	public static void executeSuperJump(Inkling ikl, Location to){
		ikl.setInSuperJump(true);
		ikl.setInInk(false);
		ikl.getPlayer().getInventory().clear();
		ikl.setSquid(false);
		ikl.setSquid(true);
		ikl.getPlayer().removePotionEffect(PotionEffectType.SLOW);
		ikl.getPlayer().removePotionEffect(PotionEffectType.JUMP);
		ikl.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SLOW,5*20,100, false));
		ikl.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.JUMP,5*20,200, false));
		new BukkitRunnable() {
			
			@Override
			public void run() {
				if(ikl.isInSuperJump()){
					jump(ikl.getPlayer(), to);
					new SoundBuilder("splashoon.launch").playInLocation(ikl.getLocation(), 0.5F);
					Methods.sendSuperJumpParticles(ikl, ikl.getLocation());
					Disguise.getDisguise(ikl.getPlayer()).setVelocity(new Vector(0,5,0));
				}
			}
		}.runTaskLater(Splashoon.instance, 40);
		new BukkitRunnable() {
			
			@Override
			public void run() {
				if(ikl.isInSuperJump())
					new SoundBuilder("splashoon.superjump").playForPlayer(ikl.getPlayer());
			}
		}.runTaskLater(Splashoon.instance, 80);
	}
	
	public static void executeSuperJump(Inkling ikl, Inkling to){
		ikl.setInSuperJump(true);
		ikl.setInInk(false);
		ikl.getPlayer().getInventory().clear();
		ikl.setSquid(true);
		ikl.getPlayer().removePotionEffect(PotionEffectType.SLOW);
		ikl.getPlayer().removePotionEffect(PotionEffectType.JUMP);
		ikl.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SLOW,5*20,100, false));
		ikl.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.JUMP,5*20,200, false));
		TitleAPI.sendActionBar(to.getPlayer(), ChatColor.DARK_GRAY+"-> "+ikl.getPlayer().getName()+" <-");
		new BukkitRunnable() {
			
			@Override
			public void run() {
				if(ikl.isInSuperJump()){
					jump(ikl.getPlayer(), to.getLocation());
					new SoundBuilder("splashoon.launch").playInLocation(ikl.getLocation(), 0.5F);
					Methods.sendSuperJumpParticles(ikl, ikl.getLocation());
					Disguise.getDisguise(ikl.getPlayer()).setVelocity(new Vector(0,5,0));
				}
			}
		}.runTaskLater(Splashoon.instance, 40);
		new BukkitRunnable() {
			
			@Override
			public void run() {
				if(ikl.isInSuperJump())
					new SoundBuilder("splashoon.superjump").playForPlayer(ikl.getPlayer());
			}
		}.runTaskLater(Splashoon.instance, 80);
	}
}
