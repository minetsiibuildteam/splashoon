package es.minetsii.Splashoon.GamePlay;

import java.util.concurrent.TimeUnit;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

import es.minetsii.APIs.language.LanguageUtils;
import es.minetsii.Splashoon.ArenaData;

public class Scoreboards {

	public static void getWaiting() {
		for (Player p : ArenaData.players.keySet()) {

			ScoreboardManager manager = Bukkit.getScoreboardManager();
			Scoreboard board = manager.getNewScoreboard();

			Objective obj = board.registerNewObjective("vote", "dummy");

			obj.setDisplaySlot(DisplaySlot.SIDEBAR);

			obj.setDisplayName(LanguageUtils.getMessage("messages.gameplay.onLobby.scoreboard.name",
					p));
			
			setScoreboard(LanguageUtils.getMessage(
					"messages.gameplay.onLobby.scoreboard.data", p, 
					ArenaData.players.size()
							).split("\\n"), obj);

			p.setScoreboard(board);
		}

	}

	public static void getPlaying() {

		for (Player p : ArenaData.players.keySet()) {

			ScoreboardManager manager = Bukkit.getScoreboardManager();
			Scoreboard board = manager.getNewScoreboard();

			Objective obj = board.registerNewObjective("vote", "dummy");

			obj.setDisplaySlot(DisplaySlot.SIDEBAR);

			obj.setDisplayName(LanguageUtils.getMessage("messages.gameplay.onGame.scoreboard.name",
					p, getTime(ArenaData.countdown)));
			
			setScoreboard(LanguageUtils.getMessage(
					"messages.gameplay.onGame.scoreboard.data", p, 
					ArenaData.players.size(), ArenaData.players.get(p).getPoints() / 2,
					Math.floor(ArenaData.players.get(p).getInk()),
					Math.floor(ArenaData.players.get(p).getWeapon().getSpecialCount())
							).split("\\n"), obj);

			p.setScoreboard(board);
		}

	}

	public static void getFinish() {

		for (Player p : ArenaData.players.keySet()) {

			ScoreboardManager manager = Bukkit.getScoreboardManager();
			Scoreboard board = manager.getNewScoreboard();

			Objective obj = board.registerNewObjective("vote", "dummy");

			obj.setDisplaySlot(DisplaySlot.SIDEBAR);

			obj.setDisplayName(LanguageUtils.getMessage("messages.gameplay.onFinish.scoreboard.name",
					p, getTime(ArenaData.countdown)));
			
			setScoreboard(LanguageUtils.getMessage(
					"messages.gameplay.onFinish.scoreboard.data", p, 
					ArenaData.players.size(), ArenaData.players.get(p).getPoints() / 2,
					ArenaData.bluepoints,
					ArenaData.orangepoints
							).split("\\n"), obj);

			p.setScoreboard(board);
		}

	}

	public static String getTime(long time) {

		long minutes = TimeUnit.SECONDS.toMinutes(time) - (TimeUnit.SECONDS.toHours(time) * 60);
		long seconds = TimeUnit.SECONDS.toSeconds(time) - (TimeUnit.SECONDS.toMinutes(time) * 60);

		ChatColor c = ChatColor.GREEN;
		if (minutes < 2)
			c = ChatColor.GOLD;
		if (minutes < 1)
			c = ChatColor.DARK_RED;

		return c + "" + String.format("%02d", minutes) + ":" + String.format("%02d", seconds);

	}
	
	public static void setScoreboard(String[] st, Objective obj){
		int i = st.length;
		for(String s : st){
			obj.getScore(s).setScore(i);
			i--;
		}
	}
}
