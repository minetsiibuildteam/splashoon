package es.minetsii.Splashoon.GamePlay;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import es.minetsii.APIs.language.LanguageUtils;
import es.minetsii.ApETSII.Titles.TitleAPI;
import es.minetsii.Splashoon.ArenaData;
import es.minetsii.Splashoon.Splashoon;
import es.minetsii.Splashoon.Player.Inkling;
import es.minetsii.Splashoon.enums.ArenaStatus;

public class OnCountDownGameStart {

	public static void start() {

		ArenaData.setStatus(ArenaStatus.waitingspawn);
		createTeams();
		for (Player p : ArenaData.players.keySet()) {
			p.closeInventory();
			p.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
			p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 100000, 10));
			p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 100000, 100000));
			p.getInventory().clear();
			ArenaData.region.sendBlockChangeToCarpet(p);
		}
		new BukkitRunnable() {
			@Override
			public void run() {
				for (Player p : ArenaData.players.keySet()) {
					TitleAPI.sendTitle(p, 5, 5, 5, 
							LanguageUtils.getMessage("messages.gameplay.onStart.ready", p), "");
				}
			}
		}.runTaskLater(Splashoon.instance, 20);
		new BukkitRunnable() {

			@Override
			public void run() {
				for (Player p : ArenaData.players.keySet())
					TitleAPI.sendTitle(p, 5, 5, 5, 
							LanguageUtils.getMessage("messages.gameplay.onStart.set", p), "");

			}
		}.runTaskLater(Splashoon.instance, 40);
		new BukkitRunnable() {

			@Override
			public void run() {
				for (Player p : ArenaData.players.keySet()) {
					TitleAPI.sendTitle(p, 5, 5, 5, 
							LanguageUtils.getMessage("messages.gameplay.onStart.go", p), "");
					for (Inkling ikl : ArenaData.players.values()) {
						ikl.equipHair();
					}
					p.removePotionEffect(PotionEffectType.SLOW);
					p.removePotionEffect(PotionEffectType.JUMP);
				}
				OnGameStart.start();
			}
		}.runTaskLater(Splashoon.instance, 60);
	}

	private static void createTeams() {
		List<Inkling> inklings = new ArrayList<Inkling>(ArenaData.players.values());
		Collections.shuffle(inklings);
		for (Inkling ikl : inklings) {
			if(ChooseEquip.equips.containsKey(ikl)){
				ikl.setBlue(ChooseEquip.equips.get(ikl));
			}
			else {
				if(ArenaData.teamblue.size() <= ArenaData.teamorange.size()){
					ArenaData.teamblue.add(ikl);
					ikl.setBlue(true);
				}
				else{
					ArenaData.teamorange.add(ikl);
					ikl.setBlue(false);
				}
			}
			Location l = ikl.isBlue() ? ArenaData.spawnBlue.clone() : ArenaData.spawnOrange.clone();
			ikl.getPlayer().teleport(l.clone().add(new Random().nextInt(2) - 2, 0, new Random().nextInt(2) - 2));
		}
	}
}
