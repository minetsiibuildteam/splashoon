package es.minetsii.Splashoon.Grenades;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.ThrownExpBottle;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.scheduler.BukkitRunnable;

import es.minetsii.APIs.Geometric;
import es.minetsii.ApETSII.Titles.TitleAPI;
import es.minetsii.Splashoon.ArenaData;
import es.minetsii.Splashoon.Splashoon;
import es.minetsii.Splashoon.CustomEvents.InklingDamageEvent;
import es.minetsii.Splashoon.CustomEvents.InklingLaunchGrenadeEvent;
import es.minetsii.Splashoon.Player.Inkling;

public class FastGrenade implements Listener{

	@EventHandler
	public void launch(ProjectileLaunchEvent e){
		Projectile pr = e.getEntity();
		if(!(pr instanceof ThrownExpBottle)) return;
		if(!(pr.getShooter() instanceof Player)) return;
		Player p = (Player) pr.getShooter();
		Inkling ikl = ArenaData.players.get(p);
		if(p.getItemInHand() == null) return;
		
		
			ikl.setSquid(false);
			if(ikl.getInk() < 30){
				ikl.equipGrenade();
				e.setCancelled(true);
				TitleAPI.sendTitle(
						p, 5, 10, 5, "", ChatColor.RED+"Sin tinta");
				return;
			}
			InklingLaunchGrenadeEvent ev = new InklingLaunchGrenadeEvent(ikl, pr, 30);
			Bukkit.getPluginManager().callEvent(ev);
			if(!ev.isCancelled()){
				ikl.removeInk(ev.getInkUsed());
				pr.setVelocity(pr.getVelocity().multiply(0.6));
				new BukkitRunnable() {
					
					@Override
					public void run() {
						ikl.equipGrenade();
					}
				}.runTaskLater(Splashoon.instance, 10);
			}
			else pr.remove();
	}
	
	
	@EventHandler
	public void hit(ProjectileHitEvent e){
		if(e.getEntity() instanceof ThrownExpBottle && e.getEntity().getShooter() instanceof Player){
			Inkling ikl = ArenaData.getInkling((Player)e.getEntity().getShooter());
			ThrownExpBottle b = (ThrownExpBottle) e.getEntity();
			for(Location l :  Geometric.spheres(e.getEntity().getLocation(), 3, 0, false, true, 0)){
				ArenaData.paintBlock(ikl, l.getBlock());
				for(Inkling p : ArenaData.players.values()){
					if(p.isBlue() == ikl.isBlue()) continue;
					if(p.getLocation().clone().add(0,-1,0).getBlock().getLocation().equals(
							l.getBlock().getLocation())){
						new InklingDamageEvent(p, 20-(l.distance(b.getLocation())*2), ikl).execute();
					}
				}
			}
			b.remove();
			new BukkitRunnable() {
				
				@Override
				public void run() {
					for(Entity en : b.getWorld().getEntities()){
						if(en instanceof  ExperienceOrb) en.remove();
					}
					
				}
			}.runTaskLater(Splashoon.instance,1);
		}
	}
	
	@EventHandler
	public void exp(EntitySpawnEvent e){
		if(e.getEntity() instanceof ExperienceOrb){
			e.setCancelled(true);
		}
	}
	
}
