package es.minetsii.Splashoon.Sounds;

public enum CustomSound {

	ShootGun("splashoon.shootgun");
	
	
	private String name;
	
	private CustomSound(String name){
		this.name = name;
	}

	public String Name() {
		return name;
	}
	
	
}
