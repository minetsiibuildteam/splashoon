package es.minetsii.Splashoon.Builder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import es.minetsii.ApETSII.Files.ConfigAccessor;
import es.minetsii.Splashoon.Splashoon;
import es.minetsii.Splashoon.Weapons.Weapon;

public class WeaponBuilder {

	public static File folder;
	
	public static Map<Integer, Weapon> loadWeapons(){	
		
		Map<Integer, Weapon> map = new HashMap<Integer, Weapon>();
		genFiles();
		for(ConfigAccessor ca : getWeaponFiles()){
			WeaponPacket wp = YmlDecoder.ymlDecoder(ca);
			if(wp == null) continue;
			map.put(wp.getId(), wp.getWeapon());
		}
		
		return map;
	}
	
	
	public static void genFiles(){
		folder = new File(Splashoon.instance.getDataFolder(),"weapons");
		if(!folder.exists())
			folder.mkdirs();
		if(folder.listFiles().length == 0){
			Splashoon.instance.getLogger().info("Loading default weapon...");
			InputStream is = null;
			OutputStream os = null;
			try{
				is = Splashoon.instance.getResource("SplattershotJr.yml");
				os = new FileOutputStream(new File(folder,"SplattershotJr.yml"));
				int read = 0;
				byte[] bytes = new byte[1024];

				while ((read = is.read(bytes)) != -1) {
					os.write(bytes, 0, read);
				}
			} catch (IOException e) {
				Splashoon.instance.getLogger().warning("Error loading default weapon!");
				e.printStackTrace();
			} finally {
				if (is != null) {
					try {
						is.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if (os != null) {
					try {
						// outputStream.flush();
						os.close();
					} catch (IOException e) {
						e.printStackTrace();
					}

				}
			}
		}
	}
	
	public static Set<ConfigAccessor> getWeaponFiles(){
		Set<ConfigAccessor> list = new HashSet<ConfigAccessor>();
		for(File file : folder.listFiles()){
			if(file.getName().contains(".yml")){
				list.add(new ConfigAccessor(Splashoon.instance, file));
			}
		}
		return list;
	}
	
}
