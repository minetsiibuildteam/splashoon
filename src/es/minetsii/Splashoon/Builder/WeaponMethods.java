package es.minetsii.Splashoon.Builder;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;

import es.minetsii.APIs.Geometric;
import es.minetsii.APIs.ItemBuilder;
import es.minetsii.APIs.NumericUtils;
import es.minetsii.APIs.SoundBuilder;
import es.minetsii.ApETSII.Titles.TitleAPI;
import es.minetsii.Splashoon.ArenaData;
import es.minetsii.Splashoon.CustomEvents.InklingDamageEvent;
import es.minetsii.Splashoon.Player.Inkling;
import es.minetsii.Splashoon.Weapons.MachineGun;
import es.minetsii.Splashoon.Weapons.Roller;
import es.minetsii.Splashoon.Weapons.SpecialWeapons.Repeater;
import es.minetsii.Splashoon.Weapons.SpecialWeapons.Shield;
import es.minetsii.Splashoon.Weapons.SpecialWeapons.SpecialWeapon;
import es.minetsii.Splashoon.Weapons.SpecialWeapons.Tornado;

public class WeaponMethods {

	
	public static void onPaintMachine(MachineGun weapon, 
			ProjectileHitEvent e){
		switch(weapon.getOnPaint().toLowerCase()){
		case "explode":
			Set<Location> locs = new HashSet<Location>
			(Geometric.spheres(e.getEntity().getLocation(), 3, 0, false, true, 0));
			new SoundBuilder(weapon.getShootSound()).playInLocation(e.getEntity().getLocation(), 0.3F);

			for(Location l :  locs){
				ArenaData.paintBlock(weapon.getInkling(), l.getBlock());
				for(Inkling p : ArenaData.players.values()){
					if(p.isBlue() == weapon.getInkling().isBlue()) continue;
					if(p.getLocation().clone().add(0,-1,0).getBlock().getLocation().equals(
							l.getBlock().getLocation())){
						weapon.onHit(weapon.getInkling().getPlayer() ,p.getPlayer(), weapon.getDamage());
					}
				}
			}
			break;
		default:
		case "paint":
			new SoundBuilder(weapon.getShootSound()).playInLocation(e.getEntity().getLocation(), 0.3F);
			for(Location l : Geometric.spheres(e.getEntity().getLocation(), 
					new Integer(weapon.getRadius()), 0, false, true, 0))
				ArenaData.paintBlock(weapon.getInkling(), l.getBlock());
			break;
		}
	}
	
	public static void onHitMachine(Player damaged, double damage, 
			MachineGun weapon){
		String type = weapon.getOnHit();
		switch(type.toLowerCase()){
		case "explode":
			Set<Location> locs = new HashSet<Location>
			(Geometric.spheres(damaged.getLocation(), 3, 0, false, true, 0));
			new SoundBuilder(weapon.getPaintSound()).playInLocation(damaged.getLocation(), 0.3F);
			for(Location l :  locs){
				ArenaData.paintBlock(weapon.getInkling(), l.getBlock());
				for(Inkling p : ArenaData.players.values()){
					if(p.isBlue() == weapon.getInkling().isBlue()) continue;
					if(p.getLocation().clone().add(0,-1,0).getBlock().getLocation().equals(
							l.getBlock().getLocation())){
						new InklingDamageEvent(p, weapon.getDamage(), weapon.getInkling()).execute();
					}
				}
			}
			break;
		case "damage":
			new InklingDamageEvent(ArenaData.getInkling(damaged),
					weapon.getDamage(), weapon.getInkling()).execute();
			break;
		case "nothing":
			break;
		default:
		case "paint":
			new SoundBuilder(weapon.getPaintSound()).playInLocation(damaged.getLocation(), 0.3F);
			new InklingDamageEvent(ArenaData.getInkling(damaged)
					, weapon.getDamage(), weapon.getInkling()).execute();
		for(Location l : Geometric.spheres(damaged.getLocation(), 2, 0, false, true, 0)){
				ArenaData.paintBlock(weapon.getInkling(), l.getBlock());
			}
		break;
		}
	}
	
	public static void onPaintRoller(Roller weapon, PlayerMoveEvent e){
		switch(weapon.getOnPaint().toLowerCase()){
		case "damage":
			if(!weapon.isUsing()) return;
			Inkling ikl2 = weapon.getInkling();
			if(ikl2.getInk() <= 0){
				TitleAPI.sendTitle(
						weapon.getInkling().getPlayer(), 5, 10, 5, "", ChatColor.RED+"Sin tinta");
				return;
			}
			weapon.removeInk();
			for(Location l :  weapon.getPaintBlocks()){
				ArenaData.paintBlock(weapon.getInkling(), l.getBlock());
				for(Inkling p : ArenaData.players.values()){
					if(p.isBlue() == ikl2.isBlue()) continue;
					if(p.getLocation().clone().add(0,-1,0).getBlock().getLocation().equals(
							l.getBlock().getLocation())){
						weapon.onHit(p.getPlayer());
					}
				}
			}
			break;
		default:
		case "paint":
			if(!weapon.isUsing()) return;
			Inkling ikl = weapon.getInkling();
			if(ikl.getInk() <= 0){
				TitleAPI.sendTitle(
						weapon.getInkling().getPlayer(), 5, 10, 5, "", ChatColor.RED+"Sin tinta");
				return;
			}
			weapon.removeInk();
			for(Location l :  weapon.getPaintBlocks())
				ArenaData.paintBlock(weapon.getInkling(), l.getBlock());		
			break;
		}
	}
	public static void onPaintRollerBall(Roller weapon, 
			ProjectileHitEvent e){
		switch(weapon.getOnHit().toLowerCase()){
		case "explode":
			Set<Location> locs = new HashSet<Location>
			(Geometric.spheres(e.getEntity().getLocation(), 3, 0, false, true, 0));
			new SoundBuilder(weapon.getBallPaintSound()).playInLocation(e.getEntity().getLocation(), 0.3F);

			for(Location l :  locs){
				ArenaData.paintBlock(weapon.getInkling(), l.getBlock());
				for(Inkling p : ArenaData.players.values()){
					if(p.isBlue() == weapon.getInkling().isBlue()) continue;
					if(p.getLocation().clone().add(0,-1,0).getBlock().getLocation().equals(
							l.getBlock().getLocation())){
						new InklingDamageEvent(p, weapon.getDamage()/2, weapon.getInkling());
					}
				}
			}
			break;
		default:
		case "paint":
			new SoundBuilder(weapon.getBallPaintSound()).playInLocation(e.getEntity().getLocation(), 0.3F);
			for(Location l : Geometric.spheres(e.getEntity().getLocation(), 
					new Integer(weapon.getSize()), 0, false, true, 0))
				ArenaData.paintBlock(weapon.getInkling(), l.getBlock());
		}
	}
	
	public static void onHitRollerBall(EntityDamageByEntityEvent e,
			Roller weapon){
		String type = weapon.getOnBallHit();
		Inkling ikl = ArenaData.getInkling((Player)e.getEntity());
		switch(type.toLowerCase()){
		case "explode":
			Set<Location> locs = new HashSet<Location>
			(Geometric.spheres(e.getEntity().getLocation(), 3, 0, false, true, 0));
			new SoundBuilder(weapon.getBallPaintSound()).playInLocation(e.getEntity().getLocation(), 0.3F);
			for(Location l :  locs){
				ArenaData.paintBlock(weapon.getInkling(), l.getBlock());
				for(Inkling p : ArenaData.players.values()){
					if(p.isBlue() == weapon.getInkling().isBlue()) continue;
					if(p.getLocation().clone().add(0,-1,0).getBlock().getLocation().equals(
							l.getBlock().getLocation())){
						new InklingDamageEvent(p, weapon.getDamage()/2, weapon.getInkling()).execute();
					}
				}
			}
			break;
		case "superpaint":
			for(Location l : Geometric.spheres(ikl.getLocation(), 
					new Integer(weapon.getSize()), weapon.getSize(), false, true, 0))
				ArenaData.paintBlock(weapon.getInkling(), l.getBlock());
			new InklingDamageEvent(ikl, 
					weapon.getDamage(), weapon.getInkling()).execute();
			break;
		case "superdamage":
			new InklingDamageEvent(ikl, 
					weapon.getDamage(), weapon.getInkling()).execute();
			break;
		case "damage":
			new InklingDamageEvent(ArenaData.getInkling((Player)e.getEntity()),
					weapon.getDamage()/2, weapon.getInkling()).execute();
			break;
		case "nothing":
			break;
		default:
		case "paint":
			new SoundBuilder(weapon.getBallPaintSound()).playInLocation(e.getEntity().getLocation(), 0.3F);
			new InklingDamageEvent(ArenaData.getInkling((Player)e.getEntity())
					, weapon.getDamage()/2, weapon.getInkling()).execute();
			for(Location l : Geometric.spheres(e.getEntity().getLocation(), 2, 0, false, true, 0)){
				ArenaData.paintBlock(weapon.getInkling(), l.getBlock());
			}
			break;
		}
	}
	
	public static void onRollerHit(Player p, Roller weapon){
		switch(weapon.getOnHit().toLowerCase()){
		case "damage":
			new InklingDamageEvent(ArenaData.getInkling(p), 
					weapon.getDamage(), weapon.getInkling()).execute();
			break;
		default:
		case "paint":
			for(Location l : Geometric.spheres(p.getLocation(), 
					new Integer(weapon.getSize()), weapon.getSize(), false, true, 0))
				ArenaData.paintBlock(weapon.getInkling(), l.getBlock());
			new InklingDamageEvent(ArenaData.getInkling(p), 
					weapon.getDamage(), weapon.getInkling()).execute();
			break;
		}
	}
	
	
	
	public static SpecialWeapon getSpecialWeapon(String s){
		switch(s.toLowerCase()){
			case "shield":
				return new Shield(null);
			case "tornado":
				return new Tornado(null);
			default:
			case "repeater":
				return new Repeater(null);
		}
	}
	
	public static ItemStack getItemBlue(Map<String, String> map){
		String itemst = map.get("MaterialId").split(";")[0];
		String name = ChatColor.translateAlternateColorCodes('&',map.get("ItemName"));
		String[] itemsplit = itemst.split(":");
		if(itemsplit.length == 1){
			return new ItemBuilder(getMaterial(itemsplit[0])).name(name).item();
		}
		else{
			if(!NumericUtils.isInteger(itemsplit[1])) itemsplit[1] = "0";
			return new ItemBuilder(getMaterial(itemsplit[0]))
					.data(new Integer(itemsplit[1])).name(name).item();
		}

	}
	
	public static ItemStack getItemOrange(Map<String, String> map){
		String[] sarr = map.get("MaterialId").split(";");
		if(sarr.length <= 1){
			return getItemBlue(map);
		}
		String itemst = sarr[1];
		String name = ChatColor.translateAlternateColorCodes('&',map.get("ItemName"));
		String[] itemsplit = itemst.split(":");
		if(itemsplit.length == 1){
			return new ItemBuilder(getMaterial(itemsplit[0])).name(name).item();
		}
		else{
			if(!NumericUtils.isInteger(itemsplit[1])) itemsplit[1] = "0";
			return new ItemBuilder(getMaterial(itemsplit[0]))
					.data(new Integer(itemsplit[1])).name(name).item();
		}

	}
	
	@SuppressWarnings("deprecation")
	public static Material getMaterial(String s){
		Material m = (NumericUtils.isInteger(s)) ? 
				Material.getMaterial(new Integer(s)) : Material.getMaterial(s);
		if(m == null) m = Material.DIAMOND_BARDING;
		return m;
	}
	
	public static String getSPName(SpecialWeapon sp){
		if(sp instanceof Tornado){
			return "Tornado";
		}
		else if(sp instanceof Repeater){
			return "Repetidor";
		}
		else return "Escudo";
	}
}
