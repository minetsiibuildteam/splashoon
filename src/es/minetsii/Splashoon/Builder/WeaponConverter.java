package es.minetsii.Splashoon.Builder;

import java.util.Map;

import es.minetsii.Splashoon.Weapons.MachineGun;
import es.minetsii.Splashoon.Weapons.MachineGunRetra;
import es.minetsii.Splashoon.Weapons.Roller;
import es.minetsii.Splashoon.Weapons.Sniper;
import es.minetsii.Splashoon.Weapons.Weapon;

public class WeaponConverter {

	public static WeaponPacket WeaponSwitch(Map<String, String> map){
		
		String type = map.get("WeaponType");
		Weapon w = null;
		switch(type.toLowerCase()){
		case "machinegundelayed":
			w = getMachineGunDelayed(map);
			break;
		case "sniper":
			w = getSniper(map);
			break;
		case "roller":
			w = getRoller(map);
			break;
		default:
		case "machinegun":
			w = getMachineGun(map);
			break;
		}
		return new WeaponPacket(w, new Integer(map.get("WeaponId")));
	}
	
	public static MachineGun getMachineGun(Map<String, String> map){
		MachineGun gun = new MachineGun(WeaponMethods.getItemBlue(map),
				WeaponMethods.getItemOrange(map), null,
				new Double(map.get("InkUsed")), new Long(map.get("WeaponDelay")),
				new Double(map.get("WeaponDistance")), 
				new Double(map.get("WeaponRange")), map.get("WeaponName"),
				new Integer(map.get("WeaponDamage")), 
				new Integer(map.get("PaintRadius")), 
				WeaponMethods.getSpecialWeapon(map.get("SpecialWeapon")), 
				map.get("ShootSound"), map.get("PaintSound"), map.get("OnPaint"),
				map.get("OnHit"), new Boolean(map.get("Vip")));
		gun.getSpecialWeapon().setWeapon(gun);
		
		return gun;
	}
	
	public static Sniper getSniper(Map<String, String> map){
		Sniper gun = new Sniper(WeaponMethods.getItemBlue(map),
				WeaponMethods.getItemOrange(map), null,
				new Double(map.get("InkUsed")), new Long(map.get("WeaponDelay")),
				new Double(map.get("WeaponDistance")), 
				new Double(map.get("WeaponRange")), map.get("WeaponName"),
				new Integer(map.get("WeaponDamage")), 
				new Integer(map.get("PaintRadius")),
				WeaponMethods.getSpecialWeapon(map.get("SpecialWeapon")), map.get("ShootSound"),
				map.get("PaintSound"), map.get("OnPaint"), map.get("OnHit"), new Boolean(map.get("Vip")));
		gun.getSpecialWeapon().setWeapon(gun);
		
		return gun;
	}
	
	public static MachineGunRetra getMachineGunDelayed(Map<String, String> map){
		MachineGunRetra gun = new MachineGunRetra(WeaponMethods.getItemBlue(map),
				WeaponMethods.getItemOrange(map), null,
				new Double(map.get("InkUsed")), new Long(map.get("WeaponDelay")),
				new Double(map.get("WeaponDistance")), 
				new Double(map.get("WeaponRange")), map.get("WeaponName"),
				new Integer(map.get("WeaponDamage")), 
				new Integer(map.get("PaintRadius")), 
				WeaponMethods.getSpecialWeapon(map.get("SpecialWeapon")), 
				map.get("ShootSound"), map.get("PaintSound"), map.get("OnPaint"), map.get("OnHit"), 
				new Long(map.get("WeaponFirstDelay")), new Boolean(map.get("Vip")));
		gun.getSpecialWeapon().setWeapon(gun);
		
		return gun;
	}
	
	public static Roller getRoller(Map<String, String> map){
		Roller roller = new Roller(WeaponMethods.getItemBlue(map),
				WeaponMethods.getItemOrange(map), null,
				new Double(map.get("InkUsed")), map.get("WeaponName"), 	
				new Integer(map.get("WeaponDamage")), new Integer(map.get("PaintRadius")),
				new Integer(map.get("Balls")), new Double(map.get("BallsRange")),
				new Long(map.get("StartDelay")), new Double(map.get("BallsDistance")),
				WeaponMethods.getSpecialWeapon(map.get("SpecialWeapon")), 
				map.get("OnPaint"), map.get("OnHit"),
				map.get("OnBallPaint"), map.get("OnBallHit"), map.get("LaunchBallPaintSound"), 
				map.get("LaunchBallShootSound"), new Boolean(map.get("Vip")));
		roller.getSpecialWeapon().setWeapon(roller);
		return roller;
	}
	
}
