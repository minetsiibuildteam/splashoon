package es.minetsii.Splashoon.Builder;

import es.minetsii.Splashoon.Weapons.Weapon;

public class WeaponPacket {

	private Weapon weapon;
	private int id;
	
	public WeaponPacket(Weapon weapon, int id){
		this.weapon = weapon;
		this.id = id;
	}
	
	public int getId(){
		return id;
	}
	
	public Weapon getWeapon(){
		return weapon;
	}
}
