package es.minetsii.Splashoon.Builder;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.configuration.file.FileConfiguration;

import es.minetsii.APIs.NumericUtils;
import es.minetsii.ApETSII.Files.ConfigAccessor;

public class YmlDecoder {

	
	public static WeaponPacket ymlDecoder(ConfigAccessor ca){
		FileConfiguration c = ca.getConfig();
		Map<String, String> configmap = new HashMap<String, String>();
		
		for(String s : c.getKeys(false)){
			configmap.put(s, c.getString(s));
		}
		if(!configComplete(configmap)){
			System.out.println("Error loading a weapon!");
			if(configmap.containsKey("WeaponName"))
				System.out.println(configmap.containsKey("WeaponName"));
			return null;
		}
		return WeaponConverter.WeaponSwitch(configmap);
	}
	
	public static boolean configComplete(Map<String, String> configmap){
		if(!configmap.containsKey("WeaponName")) return false;
		if(configmap.get("WeaponName").equals("")) return false;
		if(!configmap.containsKey("WeaponType")) return false;
		if(!configmap.containsKey("WeaponId")) return false;
		if(!configmap.containsKey("MaterialId")) return false;
		if(!configmap.containsKey("InkUsed") ||
				!NumericUtils.isDouble(configmap.get("InkUsed")))
			configmap.put("InkUsed", "1");
		if(!configmap.containsKey("SpecialWeapon"))
			configmap.put("SpecialWeapon", "Repeater");
		if(!configmap.containsKey("Vip"))
			configmap.put("Vip", "false");
		if(!configmap.containsKey("ItemName"))
			configmap.put("ItemName", configmap.get("WeaponName"));
		if(!configmap.containsKey("WeaponDamage") ||
				!NumericUtils.isInteger(configmap.get("WeaponDamage")))
			configmap.put("WeaponDamage", "5");
		if(!configmap.containsKey("PaintRaduis") ||
				!NumericUtils.isInteger(configmap.get("PaintRadius")))
			configmap.put("PaintRaduis", "2");
		
		switch(configmap.get("WeaponType").toLowerCase()){
		case "roller":
			if(!configmap.containsKey("Balls") ||
					!NumericUtils.isInteger(configmap.get("Balls")))
				configmap.put("Balls", "3");
			if(!configmap.containsKey("BallsDistance") ||
					!NumericUtils.isDouble(configmap.get("BallsDistance")))
				configmap.put("BallsDistance", "1");
			if(!configmap.containsKey("BallsRange") ||
					!NumericUtils.isDouble(configmap.get("BallsRange")))
				configmap.put("BallsRange", "1");
			if(!configmap.containsKey("StartDelay") ||
					!NumericUtils.isLong(configmap.get("StartDelay")))
				configmap.put("StartDelay", "10");
			if(!configmap.containsKey("LaunchBallShootSound"))
				configmap.put("BallShootSound", "null");
			if(!configmap.containsKey("LaunchBallPaintSound"))
				configmap.put("BallHitSound", "null");
			if(!configmap.containsKey("OnPaint"))
				configmap.put("OnPaint", "null");
			if(!configmap.containsKey("OnHit"))
				configmap.put("OnHit", "null");
			if(!configmap.containsKey("OnBallPaint"))
				configmap.put("OnBallPaint", "null");
			if(!configmap.containsKey("OnBallHit"))
				configmap.put("OnBallHit", "null");
			break;
		case "machinegundelayed":
			if(!configmap.containsKey("WeaponDelay") ||
					!NumericUtils.isLong(configmap.get("WeaponDelay")))
				configmap.put("WeaponDelay", "2");
			if(!configmap.containsKey("WeaponFirstDelay") ||
					!NumericUtils.isLong(configmap.get("WeaponFirstDelay")))
				configmap.put("WeaponFirstDelay", configmap.get("WeaponDelay"));
		case "sniper":
		default:
		case "machinegun":
			if(!configmap.containsKey("WeaponDelay") ||
					!NumericUtils.isLong(configmap.get("WeaponDelay")))
				configmap.put("WeaponDelay", "2");
			if(!configmap.containsKey("WeaponRange") ||
					!NumericUtils.isDouble(configmap.get("WeaponRange"))) 
				configmap.put("WeaponRange", "0");
			if(!configmap.containsKey("WeaponDistance") ||
					!NumericUtils.isDouble(configmap.get("WeaponDistance")))
				configmap.put("WeaponDistance", "1");
			if(!configmap.containsKey("ShootSound"))
				configmap.put("ShootSound", "null");
			if(!configmap.containsKey("PaintSound"))
				configmap.put("PaintSound", "null");
			if(!configmap.containsKey("OnPaint"))
				configmap.put("OnPaint", "paint");
			if(!configmap.containsKey("OnHit"))
				configmap.put("OnHit", "paint");
			break;
		}
		return true;
	}
}
