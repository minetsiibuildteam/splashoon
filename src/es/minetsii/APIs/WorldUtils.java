package es.minetsii.APIs;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;

import es.minetsii.Splashoon.ArenaData;
import es.minetsii.Splashoon.Splashoon;

public class WorldUtils {

	public static void unloadWorld(String world) {
		World w = Bukkit.getWorld(world);
		if (w == null)
			return;
		Bukkit.getServer().unloadWorld(w, true);
	}

	public boolean deleteWorld(String world) {

		World delete = Bukkit.getWorld(world);
		File deleteFolder = delete.getWorldFolder();

		if (deleteFolder.exists()) {
			File files[] = deleteFolder.listFiles();
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					deleteWorld(files[i]);
				} else {
					files[i].delete();
				}
			}
		}
		return (deleteFolder.delete());
	}

	public static void copyWorld(String wS, String wT) {
		World source = Bukkit.getWorld(wS);
		File sourceFolder = source.getWorldFolder();

		World target = Bukkit.getWorld(wT);
		File targetFolder = target.getWorldFolder();

		try {
			ArrayList<String> ignore = new ArrayList<String>(Arrays.asList("uid.dat", "session.dat"));
			if (!ignore.contains(source.getName())) {
				if (sourceFolder.isDirectory()) {
					if (!targetFolder.exists())
						targetFolder.mkdirs();
					String files[] = sourceFolder.list();
					for (String file : files) {
						File srcFile = new File(sourceFolder, file);
						File destFile = new File(targetFolder, file);
						copyWorld(srcFile, destFile);
					}
				} else {
					InputStream in = new FileInputStream(sourceFolder);
					OutputStream out = new FileOutputStream(targetFolder);
					byte[] buffer = new byte[1024];
					int length;
					while ((length = in.read(buffer)) > 0)
						out.write(buffer, 0, length);
					in.close();
					out.close();
				}
				regenSession(wT);
			}
		} catch (IOException e) {

		}
	}

	public static void unloadWorld(World world) {
		if (world == null)
			return;
		Bukkit.getServer().unloadWorld(world, true);
	}

	public static boolean deleteWorld(File path) {
		if (path.exists()) {
			File files[] = path.listFiles();
			for (int i = 0; i < files.length; i++) {
				if (files[i].isDirectory()) {
					deleteWorld(files[i]);
				} else {
					files[i].delete();
				}
			}
		}
		return (path.delete());
	}

	public static void copyWorld(File source, File target) {
		try {
			ArrayList<String> ignore = new ArrayList<String>(Arrays.asList("uid.dat", "session.dat"));
			if (!ignore.contains(source.getName())) {
				if (source.isDirectory()) {
					if (!target.exists())
						target.mkdirs();
					String files[] = source.list();
					for (String file : files) {
						File srcFile = new File(source, file);
						File destFile = new File(target, file);
						copyWorld(srcFile, destFile);
					}
				} else {
					InputStream in = new FileInputStream(source);
					OutputStream out = new FileOutputStream(target);
					byte[] buffer = new byte[1024];
					int length;
					while ((length = in.read(buffer)) > 0)
						out.write(buffer, 0, length);
					in.close();
					out.close();
				}
				regenSession(target.getName());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void regenSession(String world) {
		File session = new File(Bukkit.getWorldContainer(), world + "/session.lock");
		session.delete();
		try {
			session.createNewFile();
			DataOutputStream dataoutputstream = new DataOutputStream(new FileOutputStream(session));

			try {
				dataoutputstream.writeLong(System.currentTimeMillis());
			} finally {
				dataoutputstream.close();
			}
		} catch (IOException ioexception) {

		}
	}
	
	public static void saveArena(){
		try{
		getSplashoonWorld().save();
		File f = getSplashoonWorld().getWorldFolder();
		File f2 = new File(Splashoon.instance.getDataFolder(),"world");
		if(f2.exists())
			f2.delete();
		WorldUtils.copyWorld(f, 
				f2);
		}catch(Exception ex){
			return;
		}
	}
	
	public static void regenArena(){
		try{
		File f = getSplashoonWorld().getWorldFolder();
		File f2 = new File(Splashoon.instance.getDataFolder(),"world");
		WorldUtils.unloadWorld(getSplashoonWorld());
		WorldUtils.deleteWorld(f);
		WorldUtils.copyWorld(f2, f);
		Bukkit.getWorld(ArenaData.world.getName());
		}catch(Exception ex){
			return;
		}
	}
	
	public static World getSplashoonWorld(){
		World w = Bukkit.getWorld("Splashoon");
		if(w == null)
			w = Bukkit.createWorld(new WorldCreator("Splashoon").type(WorldType.FLAT));
		return w;
	}
}
