package es.minetsii.APIs;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public class SkullBuilder {

	private ItemStack skull;
	
	public SkullBuilder(){
		this.skull = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
	}
	
	public SkullBuilder(ItemStack skull){
		this.skull = skull;
	}
	
	public ItemStack getSkull(){
		return skull;
	}
	
	public SkullBuilder setOwner(String owner){
		SkullMeta km = (SkullMeta)skull.getItemMeta();
		km.setOwner(owner);
		skull.setItemMeta(km);
		return this;
	}
}
