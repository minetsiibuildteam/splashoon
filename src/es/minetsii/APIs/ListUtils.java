package es.minetsii.APIs;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.base.Objects;

public class ListUtils {

	public static <T extends Object> Set<T> toSet(T[] array) {
		return new HashSet<T>(Arrays.asList(array));
	}

	public static <T extends Object> Set<T> toSet(List<T> array) {
		return new HashSet<T>(array);
	}

	public static <T extends Object> List<T> toArrayList(T[] array) {
		return Arrays.asList(array);
	}

	public static <T extends Object> String toString(List<T> list, char separator) {
		String sf = "";
		for (int i = 0; i < list.size(); i++) {

			sf += list.get(i).toString().trim();

			if (!(i == list.size() - 1)) {
				sf += separator;
			}
		}
		return sf;
	}
	
	public static <T, E> Set<T> getKeysByValue(Map<T, E> map, E value) {
	    return map.entrySet()
	              .stream()
	              .filter(entry -> Objects.equal(entry.getValue(), value))
	              .map(Map.Entry::getKey)
	              .collect(Collectors.toSet());
	}
}
