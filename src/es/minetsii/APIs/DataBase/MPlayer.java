package es.minetsii.APIs.DataBase;

import java.sql.Timestamp;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.avaje.ebean.validation.Length;
import com.avaje.ebean.validation.NotEmpty;
import com.avaje.ebean.validation.NotNull;

@Entity()
@Table(name="spl_players")
public class MPlayer {
    @Id
    private int id;

    @NotNull
    private String playerUUID;
    
    @NotNull
    private boolean gender;

    @Length(max=30)
    @NotEmpty
    private String name;
    
    @NotEmpty
    private String weapon;
    
    @NotNull
	private int points;
    
    @NotNull
	private int level;
    
    @NotNull
	private long exp;
    
    @NotNull
	private long wins;
    
    @NotNull
	private long money;
    
    @NotNull
	private long kills;

    @NotEmpty
    private String hair;
    
    @NotEmpty
    private String tShirt;
    
    @NotEmpty
    private String boots;
    
    @NotEmpty
    private String weaponInv;
    
    @NotEmpty
    private String tShirtInv;
    
    @NotEmpty
    private String hairInv;
    
    @NotEmpty
    private String bootsInv;
    
    @Version  
    Timestamp lastUpdate;
    
    
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getWeapon() {
        return weapon;
    }

    public void setWeapon(String weapon) {
        this.weapon = weapon;
    }
    
    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public String getPlayerUUID() {
        return playerUUID;
    }

    public void setPlayerUUID(String playerUUID) {
        this.playerUUID = playerUUID;
    }

    public Player getPlayer() {
        return Bukkit.getServer().getPlayer(UUID.fromString(playerUUID));
    }

    public void setPlayer(Player player) {
        this.playerUUID = player.getUniqueId().toString();
    }
    
    public int getPoints() {
    	return points;
    }
    
    public void setPoints(int points) {
    	this.points = points;
    }
    
    public int getLevel(){
    	return level;
    }
    
    public void setLevel(int level){
    	this.level = level;
    }
    
    public long getExp(){
    	return exp;
    }
    
    public void setExp(long exp){
    	this.exp = exp;
    }
    
    public long getWins(){
    	return wins;
    }
    
    public void setWins(long wins){
    	this.wins = wins;
    }
    
    public long getMoney(){
    	return money;
    }
    
    public void setMoney(long money){
    	this.money = money;
    }
    
    public long getKills(){
    	return kills;
    }
    
    public void setKills(long kills){
    	this.kills = kills;
    }
    
    public String getTShirt(){
    	return tShirt;
    }
    
    public String getBoots(){
    	return boots;
    }
    
    public String getHair(){
    	return hair;
    }
    
    public String getWeaponInv(){
    	return weaponInv;
    }
    
    public String getHairInv(){
    	return hairInv;
    }
    
    public String getTShirtInv(){
    	return tShirtInv;
    }
    
    public String getBootsInv(){
    	return bootsInv;
    }
    
    public void setWeaponInv(String inv){
    	weaponInv = inv;
    }
    
    public void setTShirt(String tShirt){
    	this.tShirt = tShirt;
    }
    
    public void setHair(String hair){
    	this.hair = hair;
    }
    
    public void setBoots(String boots){
    	this.boots = boots;
    }
    
    public void setHairInv(String inv){
    	hairInv = inv;
    }
    
    public void setTShirtInv(String inv){
    	tShirtInv = inv;
    }
    
    public void setBootsInv(String inv){
    	bootsInv = inv;
    }
    
    public Timestamp getLastUpdate(){
    	return lastUpdate;
    }
    
    public void setLastUpdate(Timestamp lastUpdate){
    	this.lastUpdate = lastUpdate;
    }
    
}