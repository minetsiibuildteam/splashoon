package es.minetsii.APIs;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import es.minetsii.Splashoon.Sounds.CustomSound;

public class SoundBuilder {

	
	private String sound;
	
	public SoundBuilder(Sound sound){
		this.sound = sound.name();
	}
	
	public SoundBuilder(CustomSound sound){
		this.sound = sound.name();
	}
	
	public SoundBuilder(String sound){
		this.sound = sound;
	}

	public SoundBuilder setSound(Sound sound) {
		this.sound = sound.name();
		return this;
	}
	
	public SoundBuilder setSound(CustomSound sound) {
		this.sound = sound.Name();
		return this;
	}
	
	public SoundBuilder setSound(String sound) {
		this.sound = sound;
		return this;
	}
	
	public SoundBuilder playInLocation(Location l, float range){
		
		for(Player p : l.getWorld().getPlayers())
			p.playSound(l, sound, range, 1);
		
		return this;
	}
	
	public SoundBuilder playForAllPlayers(){
		
		for(Player p : Bukkit.getOnlinePlayers())
			p.playSound(p.getLocation(), sound, 100, 1);
		
		return this;
	}
	
	public SoundBuilder playForPlayer(Player p){
		p.playSound(p.getLocation(), sound, 100, 1);
		return this;
	}
	
	public SoundBuilder playForPlayers(List<Player> list){
		for(Player p : list)
		p.playSound(p.getLocation(), sound, 100, 1);
		return this;
	}
	
}
