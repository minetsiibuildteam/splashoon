package es.minetsii.APIs;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemBuilder {

	private ItemStack	item;

	public ItemBuilder(Material material) {
		item = new ItemStack(material);
		ItemMeta m = item.getItemMeta();
		m.setLore(new ArrayList<String>());
		item.setItemMeta(m);
	}

	public ItemBuilder amount(int amount) {
		item.setAmount(amount);
		return this;
	}

	public ItemBuilder datashort(short durability) {
		item.setDurability(durability);
		return this;
	}
	public ItemBuilder data(int durability) {
		item.setDurability((short)durability);
		return this;
	}

	@Deprecated
	public ItemStack getItem() {
		return item();
	}

	public ItemStack item() {
		return item;
	}

	public ItemBuilder lore(List<String> lore) {
		ItemMeta m = item.getItemMeta();
		m.setLore(lore);
		item.setItemMeta(m);
		return this;
	}
	
	public ItemBuilder addLoreLine(String line) {
		ItemMeta m = item.getItemMeta();
		List<String> lore = m.getLore();
		if(lore == null) lore = new ArrayList<String>();
		lore.add(line);
		m.setLore(lore);
		item.setItemMeta(m);
		return this;
	}

	public ItemBuilder name(String name) {
		ItemMeta m = item.getItemMeta();
		m.setDisplayName(name);
		item.setItemMeta(m);
		return this;
	}

	@Deprecated
	public ItemBuilder setAmount(int amount) {
		return amount(amount);
	}

	@Deprecated
	public ItemBuilder setDurability(short durability) {
		return datashort(durability);
	}

	@Deprecated
	public ItemBuilder setLore(List<String> lore) {
		return lore(lore);
	}

	@Deprecated
	public ItemBuilder setName(String name) {
		return name(name);
	}

	@Deprecated
	public ItemBuilder setType(Material material) {
		return type(material);
	}

	public ItemBuilder type(Material material) {
		item.setType(material);
		return this;
	}

}
