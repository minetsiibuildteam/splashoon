package es.minetsii.APIs;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;

public class Geometric {

	public static List<Location> pyramid(int floors,Location center,boolean hollow){
		List<Location> locations = new ArrayList<Location>();
		floors--;
		int floor = 0;
		for(int i = floors; i >= 0; i--){
			Location l2 = center.clone().add(i,floor,i);
			Location l1 = center.clone().add(-i,floor,-i);
			double xm = l2.clone().getX();
			double x = l1.clone().getX();
			double zm = l2.clone().getZ();
			double z = l1.clone().getZ();
			double Y = l1.clone().getY();
			for(double X = x; X <= xm; X++){
				for(double Z = z; Z <= zm; Z++){
						locations.add(new Location(center.getWorld(),X,Y,Z));
						continue;
				}
			}
			floor++;
		}
		if(hollow){
			List<Location> remove = pyramid(floors-1,center,false);
			for(Location r : remove){
				locations.remove(r);
			}
		}
		return locations;
	}
	
	
	 public static List<Location> spheres (Location loc, Integer r, Integer h, Boolean hollow, Boolean sphere, int plus_y) {
	        List<Location> circleblocks = new ArrayList<Location>();
	        int cx = loc.getBlockX();
	        int cy = loc.getBlockY();
	        int cz = loc.getBlockZ();
	        for (double x = cx - r; x <= cx +r; x++)
	            for (double z = cz - r; z <= cz +r; z++)
	                for (double y = (sphere ? cy - r : cy); y < (sphere ? cy + r : cy + h); y++) {
	                    double dist = (cx - x) * (cx - x) + (cz - z) * (cz - z) + (sphere ? (cy - y) * (cy - y) : 0);
	                    if (dist < r*r && !(hollow && dist < (r-1)*(r-1))) {
	                        Location l = new Location(loc.getWorld(), x, y + plus_y, z);
	                        circleblocks.add(l);
	                        }
	                    }
	 
	        return circleblocks;
	    }
	 
	public static List<Location> Helix(Location loc,double radius,double ym,int ymax) {
		List<Location> locations = new ArrayList<Location>();
		for(double y = 0; y <= ymax; y+=0.05) {
			double x = radius * Math.cos(y);
			double z = radius * Math.sin(y);
			Location l = new Location(loc.getWorld(),
					(float) (loc.getX() + x), (float) (loc.getY()+(y-(y/ym))), (float) (loc.getZ() + z));
			locations.add(l);
		}
		return locations;
	}
	
	public static List<Location> Tornado(Location loc,double radius,double ym,int ymax) {
		List<Location> locations = new ArrayList<Location>();
		for(double y = 0; y <= ymax; y+=0.05) {
			double x = radius * Math.cos(y);
			double z = radius * Math.sin(y);
			Location l = new Location(loc.getWorld(),
					(float) (loc.getX() + x), (float) (loc.getY()+(y-(y/ym))), (float) (loc.getZ() + z));
			locations.add(l);
			radius+=0.01;
		}
		return locations;
	}
}
