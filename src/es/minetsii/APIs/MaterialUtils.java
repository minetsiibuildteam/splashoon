package es.minetsii.APIs;

import org.bukkit.Material;

public class MaterialUtils {

	
	@SuppressWarnings("deprecation")
	public static boolean isSlab(Material m){
		return m.equals(Material.getMaterial(44)) || m.equals(Material.getMaterial(126)) ||
				m.equals(Material.getMaterial(182));
	}
	
	@SuppressWarnings("deprecation")
	public static boolean isStair(Material m){
		return m.equals(Material.WOOD_STAIRS) || m.equals(Material.COBBLESTONE_STAIRS) ||
				m.equals(Material.BRICK_STAIRS) || m.equals(Material.getMaterial(109)) ||
				m.equals(Material.NETHER_BRICK_STAIRS) || m.equals(Material.SPRUCE_WOOD_STAIRS) ||
				m.equals(Material.BIRCH_WOOD_STAIRS) || m.equals(Material.JUNGLE_WOOD_STAIRS) ||
				m.equals(Material.ACACIA_STAIRS) || m.equals(Material.DARK_OAK_STAIRS) ||
				m.equals(Material.SANDSTONE_STAIRS) || m.equals(Material.RED_SANDSTONE_STAIRS);
	}
	
	public static boolean isNormalFence(Material m){
		return m.equals(Material.FENCE) || m.equals(Material.BIRCH_FENCE) ||
				m.equals(Material.SPRUCE_FENCE) || m.equals(Material.JUNGLE_FENCE) ||
				m.equals(Material.ACACIA_FENCE) || m.equals(Material.DARK_OAK_FENCE) ||
				m.equals(Material.NETHER_FENCE);
	}
}
