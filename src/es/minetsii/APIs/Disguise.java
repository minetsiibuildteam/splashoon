package es.minetsii.APIs;

import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo.EnumPlayerInfoAction;

public class Disguise implements Listener{

	
	public static Map<Player, LivingEntity> disguises;
	
	public static boolean isPlayerDisguise(Player p){
		return disguises.containsKey(p);
	}
	
	public static void undisguisePlayer(Player p){
		if(!isPlayerDisguise(p)) return;
		showPlayer(p);
		disguises.get(p).remove();
		disguises.remove(p);
		p.removePotionEffect(PotionEffectType.INVISIBILITY);
	}
	
	public static <T extends LivingEntity>
	void disguisePlayer(Player p, Class<T> cl, boolean undisguise){
		if(isPlayerDisguise(p)){
			if(undisguise)
				undisguisePlayer(p);
			else {
				return;
			}
		}
		hidePlayer(p);
		LivingEntity en = p.getLocation().getWorld().spawn(p.getLocation(), cl);
		disguises.put(p, en);
		p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 999999, 0));
	}
	
	public Player getPlayerByEntity(Entity en){
		for(Player p : disguises.keySet()){	
			if(disguises.get(p).equals(en)) return p;
		}
		return null;
	}
	
	public static LivingEntity getDisguise(Player p){
		return disguises.get(p);
	}
	
	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e){
		Player p = e.getPlayer();
		if(!isPlayerDisguise(p)) return;
		LivingEntity en = disguises.get(p);
		en.teleport(p);
		p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 999999, 0,true));
	}
	
	@EventHandler
	public void onPlayerDeath(PlayerDeathEvent e){
		Player p = e.getEntity();
		if(!isPlayerDisguise(p)) return;
		disguises.get(p).teleport(new Location(p.getWorld(), 0, 100000, 0));
	}
	
	@EventHandler
	public void onPlayerRespawn(PlayerRespawnEvent e){
		Player p = e.getPlayer();
		if(!isPlayerDisguise(p)) return;
		disguises.get(p).setFallDistance(0);
	}
	
	@EventHandler
	public void onEntityDamage(EntityDamageEvent e){
		if(!disguises.containsValue(e.getEntity())){
			if(e.getEntity() instanceof Player && isPlayerDisguise((Player)e.getEntity())){
				
				if(!e.getCause().equals(DamageCause.FALL) && 
						!e.getCause().equals(DamageCause.STARVATION) &&
						!e.getCause().equals(DamageCause.DROWNING) &&
						!e.getCause().equals(DamageCause.SUFFOCATION) &&
						!e.getCause().equals(DamageCause.WITHER) &&
						!e.getCause().equals(DamageCause.POISON) &&
						disguises.containsKey((Player)e.getEntity()))  {
					e.setCancelled(true);
					return;
				}
				disguises.get((Player)e.getEntity()).damage(1);
				disguises.get((Player)e.getEntity()).addPotionEffect
				(new PotionEffect(PotionEffectType.HEAL,100,100));
				return;
			}
			return;
		}
		if(e.getCause().equals(DamageCause.FALL) || 
				e.getCause().equals(DamageCause.STARVATION) ||
				e.getCause().equals(DamageCause.DROWNING) ||
				e.getCause().equals(DamageCause.SUFFOCATION) ||
				e.getCause().equals(DamageCause.POISON) ||
				e.getCause().equals(DamageCause.WITHER)){
			e.setCancelled(true);	
			return;
		}
		getPlayerByEntity(e.getEntity()).damage(e.getDamage());
		e.setDamage(1);
		LivingEntity en = (LivingEntity) e.getEntity();
		en.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 5, 100,true));
		
		
	}
	
	private static void hidePlayer(Player hiding) {
	    for(Player p : Bukkit.getOnlinePlayers()){
	    	p.hidePlayer(hiding);
	    EntityPlayer nmsFrom = ((CraftPlayer) p).getHandle();
	    EntityPlayer nmsHiding = ((CraftPlayer) hiding).getHandle();
	    nmsFrom.playerConnection.
	    sendPacket(new PacketPlayOutPlayerInfo(EnumPlayerInfoAction.ADD_PLAYER, nmsHiding));
	    }
	}
	
	private static void showPlayer(Player hiding) {
	    for(Player p : Bukkit.getOnlinePlayers()){
	    	p.showPlayer(hiding);
	    }
	}
	
	public static Player getPlayer(Entity e){
		for(Player p : disguises.keySet()){
			if(disguises.get(p).equals(e))
				return p;
		}
		return null;
	}
	
	
	
}
