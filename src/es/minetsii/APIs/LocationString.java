package es.minetsii.APIs;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.util.Vector;

public class LocationString {
	
	//Locations
	public static String toString(Location l){
	     return l.getX()+":"+l.getY()+":"+l.getZ()+":"+l.getWorld().getName()+
	    		 ":"+l.getDirection().getX()+"="+l.getDirection().getY()+"="+l.getDirection().getZ();
	}
	public static Location fromString(String s){
      String[]array=s.split(":");
      World w = WorldUtils.getSplashoonWorld();
      double x= Double.valueOf(array[0]);
      double y= Double.valueOf(array[1]);
      double z= Double.valueOf(array[2]);
      String[] direction = array[4].split("=");
      Vector d = new Vector(Double.valueOf(direction[0]),
      		Double.valueOf(direction[1]),Double.valueOf(direction[2]));
      Location l = new Location(w,x,y,z);
      l.setDirection(d);
      return l;
  }
}
