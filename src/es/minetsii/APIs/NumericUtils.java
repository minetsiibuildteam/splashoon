package es.minetsii.APIs;

public class NumericUtils {

	
	public static boolean isInteger(String s){
		try{
			new Integer(s);
			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	public static boolean isDouble(String s){
		try{
			new Double(s);
			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	public static boolean isFloat(String s){
		try{
			new Float(s);
			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	public static boolean isLong(String s){
		try{
			new Long(s);
			return true;
		}catch(Exception e){
			return false;
		}
	}
}
