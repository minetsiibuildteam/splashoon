package es.minetsii.APIs.language;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import es.minetsii.ApETSII.Language.PlayerLangUtils;
import es.minetsii.Splashoon.Splashoon;

/**
 * The Class LanguageUtils.
 */
public class LanguageUtils {

	/** The default language specified in the config. */
	public static JavaPlugin 				instance = Splashoon.instance;
	public static String					defaultLang	= instance.getConfig().getString(
																"config.defaultLanguage");

	private static Set<Language	>			langList	= new HashSet<>();
	private static File						folder		= new File(instance.getDataFolder(), "langs/");
	private static Pattern					pattern		= Pattern.compile("\\{(\\d+)\\}");

	/**
	 * Load all the languages of the /lang folder.
	 */
	public static void loadLanguages() {
		langList.clear();
		if (folder.exists() && !folder.isDirectory())
			folder.delete();
		if (!folder.exists())
			folder.mkdir();
		if (folder.exists() && folder.listFiles().length == 0) {
			instance.getLogger().info("Loading default language");
			InputStream is = null;
			OutputStream os = null;
			try {
				is = instance.getResource("english.yml");
				os = new FileOutputStream(new File(folder, "english.yml"));

				int read = 0;
				byte[] bytes = new byte[1024];

				while ((read = is.read(bytes)) != -1) {
					os.write(bytes, 0, read);
				}
			} catch (IOException e) {
				instance.getLogger().warning("Error loading default language!");
				e.printStackTrace();
			} finally {
				if (is != null) {
					try {
						is.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if (os != null) {
					try {
						os.close();
					} catch (IOException e) {
						e.printStackTrace();
					}

				}
			}
		}

		Arrays.asList(folder.listFiles()).stream().filter(file -> file.getName().endsWith(".yml"))
				.forEach(file -> langList.add(new Language(file.getName().replace(".yml", ""), file)));
		langList.stream().forEach(lang -> lang.loadMessages());
	}

	/**
	 * Gets the loaded languages list.
	 *
	 * @return the lang list
	 */
	public static Set<Language> getLangList() {
		return langList;
	}

	/**
	 * Gets the lang from a string.
	 *
	 * @param language
	 *            the language
	 * @return the lang from string
	 */
	public static Language getLangFromString(String language) {
		try {
			Optional<Language> res = langList.stream().filter(lang -> lang.getLang().equals(language)).findAny();
			return res.isPresent() ? res.get() : langList.stream()
					.filter(lang -> lang.getLang().equalsIgnoreCase(defaultLang)).findAny().get();
		} catch (NoSuchElementException nsee) {
			Bukkit.getLogger().log(Level.SEVERE, "The language '" + language + "' is not in the langs folder");
			return null;
		}
	}

	/**
	 * Gets the player lang.
	 *
	 * @param player
	 *            the player
	 * @return the player lang
	 */
	public static Language getPlayerLang(Player player) {
		return getLangFromString(PlayerLangUtils.getPlayerLang(player).getLanguage());
	}

	/**
	 * Adds the message to all the loaded languages.
	 *
	 * @param node
	 *            the node
	 * @param message
	 *            the message
	 */
	public static void addMessage(String node, String message, Boolean replace){
		langList.stream().forEach(l -> l.addMessage(node, message, replace));
	}

	/**
	 * Send the message to the player.
	 *
	 * @param player
	 *            the player
	 * @param node
	 *            the node
	 */
	public static void sendMessage(String node, CommandSender sender) {
		Language lang;
		if(sender instanceof Player){
			lang = getPlayerLang((Player)sender);
		}
		else lang = getLangFromString(defaultLang);
		String[] msg = ChatColor.translateAlternateColorCodes('&', lang.getMessage(node)).split("\\n");
		msg[0] = ChatColor.translateAlternateColorCodes('&', lang.getMessage("prefix")) + msg[0];
		sender.sendMessage(msg);
	}
	
	
	/**
	 * Send the message to the player.
	 *
	 * @param player
	 *            the player
	 * @param node
	 *            the node
	 * @param prefix
	 *            send prefix
	 */
	public static void sendMessage(String node, Boolean prefix, CommandSender sender) {
		Language lang;
		if(sender instanceof Player){
			lang = getPlayerLang((Player)sender);
		}
		else lang = getLangFromString(defaultLang);
		String[] msg = ChatColor.translateAlternateColorCodes('&', lang.getMessage(node)).split("\\n");
		if(prefix)
			msg[0] = ChatColor.translateAlternateColorCodes('&', lang.getMessage("prefix")) + msg[0];
		sender.sendMessage(msg);
	}

	/**
	 * Send the message to the player.
	 *
	 * @param player
	 *            the player
	 * @param node
	 *            the node
	 * @param args
	 *            the arguments to replace in the message
	 */
	public static void sendMessage(String node, CommandSender sender, Object... args) {
		try {
			Language lang;
			if(sender instanceof Player){
				lang = getPlayerLang((Player)sender);
			}
			else lang = getLangFromString(defaultLang);
			String msg = lang.getMessage(node);
			List<Integer> msgParams = new ArrayList<>();

			Matcher matcher = pattern.matcher(msg);
			while (matcher.find())
				msgParams.add(new Integer(matcher.group(1)));
			for (Integer index : msgParams)
				msg = msg.replace("{" + index + "}", args[index].toString());

			String[] msga = ChatColor.translateAlternateColorCodes('&', msg).split("\\n");
			msga[0] = ChatColor.translateAlternateColorCodes('&', lang.getMessage("prefix")) + msga[0];
			sender.sendMessage(msga);
		} catch (NullPointerException e) {
			Bukkit.getLogger().log(Level.SEVERE, "There was an error processing the node: " + node);
			e.printStackTrace();
		}
	}
	
	/**
	 * Send the message to the player.
	 *
	 * @param player
	 *            the player
	 * @param node
	 *            the node
	 * @param prefix
	 *            send prefix
	 * @param args
	 *            the arguments to replace in the message
	 */
	public static void sendMessage(String node, CommandSender sender, Boolean prefix, Object... args) {
		try {
			Language lang;
			if(sender instanceof Player){
				lang = getPlayerLang((Player)sender);
			}
			else lang = getLangFromString(defaultLang);
			String msg = lang.getMessage(node);
			List<Integer> msgParams = new ArrayList<>();

			Matcher matcher = pattern.matcher(msg);
			while (matcher.find())
				msgParams.add(new Integer(matcher.group(1)));
			for (Integer index : msgParams)
				msg = msg.replace("{" + index + "}", args[index].toString());

			String[] msga = ChatColor.translateAlternateColorCodes('&', msg).split("\\n");
			if(prefix)
				msga[0] = ChatColor.translateAlternateColorCodes('&', lang.getMessage("prefix")) + msga[0];
			sender.sendMessage(msga);
		} catch (NullPointerException e) {
			Bukkit.getLogger().log(Level.SEVERE, "There was an error processing the node: " + node);
			e.printStackTrace();
		}
	}

	/**
	 * Get the message using the default language.
	 *
	 * @param node
	 *            the node
	 */
	public static String getMessage(String node) {
		try {
			return ChatColor.translateAlternateColorCodes('&', getLangFromString(defaultLang).getMessage(node));
		} catch (NullPointerException e) {
			instance.getLogger().log(Level.SEVERE,
					"There was a problem processing the node: " + node);
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Get the message of the specified language. If the language doesn't exists
	 * it'll return as if it was the default language.
	 *
	 * @param lang
	 *            the language string, example "english"
	 * @param node
	 *            the node
	 */
	public static String getMessage(String node, String lang) {
		return ChatColor.translateAlternateColorCodes('&', getLangFromString(lang).getMessage(node));
	}

	/**
	 * Get the message of the specified language. If the language doesn't exists
	 * it'll return as if it was the default language.
	 *
	 * @param lang
	 *            the language itself
	 * @param node
	 *            the node
	 */
	public static String getMessage(String node, Language lang) {
		return ChatColor.translateAlternateColorCodes('&', lang.getMessage(node));
	}

	/**
	 * Get the message of the specified language. If the language doesn't exists
	 * it'll return as if it was the default language.
	 *
	 * @param node
	 *            the node
	 * @param player
	 *            the player
	 */
	public static String getMessage(String node, Player p) {
		return ChatColor.translateAlternateColorCodes('&', getMessage(node, LanguageUtils.getPlayerLang(p)));
	}

	/**
	 * Get the message of the specified language. If the language doesn't exists
	 * it'll return as if it was the default language.
	 *
	 * @param node
	 *            the node
	 * @param player
	 *            the player
	 * @param args
	 *            the arguments which will replace the message
	 */
	public static String getMessage(String node, Player player, Object... args) {
		try {
			Language lang = getPlayerLang(player);
			String msg = lang.getMessage(node);
			List<Integer> msgParams = new ArrayList<>();

			Matcher matcher = pattern.matcher(msg);
			while (matcher.find())
				msgParams.add(new Integer(matcher.group(1)));
			for (Integer index : msgParams)
				msg = msg.replace("{" + index + "}", args[index].toString());

			return (ChatColor.translateAlternateColorCodes('&', msg));
		} catch (NullPointerException e) {
			Bukkit.getLogger().log(Level.SEVERE, "There was an error processing the node: " + node);
			return null;
		}
	}
	
	/**
	 * Get the message of the specified language. If the language doesn't exists
	 * it'll return as if it was the default language.
	 *
	 * @param node
	 *            the node
	 * @param lang
	 *            the language
	 * @param args
	 *            the arguments which will replace the message
	 */
	public static String getMessage(String node, Language lang, Object... args) {
		try {
			String msg = lang.getMessage(node);
			List<Integer> msgParams = new ArrayList<>();

			Matcher matcher = pattern.matcher(msg);
			while (matcher.find())
				msgParams.add(new Integer(matcher.group(1)));
			for (Integer index : msgParams)
				msg = msg.replace("{" + index + "}", args[index].toString());

			return (ChatColor.translateAlternateColorCodes('&', msg));
		} catch (NullPointerException e) {
			Bukkit.getLogger().log(Level.SEVERE, "There was an error processing the node: " + node);
			return null;
		}
	}
}
