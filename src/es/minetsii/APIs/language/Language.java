package es.minetsii.APIs.language;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 * The Class Language.
 */
public class Language {

	private String lang;
	private File langFile;
	private Map<String, String> messages;
	
	/**
	 * Instantiates a new language.
	 *
	 * @param lang the lang
	 * @param langFile the lang file
	 */
	public Language(String lang, File langFile){
		this.lang = lang;
		this.langFile = langFile;
		messages = new HashMap<>();
	}
	
	/**
	 * Load/Reload messages.
	 */
	public void loadMessages(){
		messages.clear();
		FileConfiguration langConfig = getConfig();
		
		langConfig.getKeys(true).stream().forEach(node -> messages
				.put(node, langConfig.getString(node)));
	}
	
	/**
	 * Adds the message into the specified language.
	 *
	 * @param node the node
	 * @param message the message
	 */
	public void addMessage(String node, String message, boolean replace){
		messages.put(node, message);
		FileConfiguration c = getConfig();
		if(replace || (!replace && !messages.containsKey(node)))
			c.set(node, message);
		saveConfig(c);
	}
	
	/**
	 * Gets the message which has the specified node.
	 *
	 * @param node the node
	 * @return the message
	 */
	public String getMessage(String node){
		return messages.get(node);
	}
	
	/**
	 * Gets the language name of the Language object.
	 *
	 * @return the lang name
	 */
	public String getLang(){
		return lang;
	}
	
	/**
	 * Gets the language file with the .yml extension.
	 *
	 * @return the lang file
	 */
	public File getLangFile(){
		return langFile;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((lang == null) ? 0 : lang.hashCode());
		result = prime * result + ((langFile == null) ? 0 : langFile.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Language other = (Language) obj;
		if (lang == null) {
			if (other.lang != null)
				return false;
		} else if (!lang.equals(other.lang))
			return false;
		if (langFile == null) {
			if (other.langFile != null)
				return false;
		} else if (!langFile.equals(other.langFile))
			return false;
		return true;
	}
	
	/**
	 * Returns the configuration.
	 */
	public FileConfiguration getConfig(){
		try {
			return YamlConfiguration.loadConfiguration(new InputStreamReader
					(new FileInputStream(langFile), "UTF-8"));
		} catch (UnsupportedEncodingException | FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * Save the configuration.
	 */
	public void saveConfig(FileConfiguration config){
		try {
			config.save(langFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
