package es.minetsii.APIs.language;

import java.sql.Timestamp;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.avaje.ebean.validation.Length;
import com.avaje.ebean.validation.NotEmpty;
import com.avaje.ebean.validation.NotNull;

@Entity()
@Table(name="playerlangs")
public class PlayerLang {
    @Id
    private int id;

    @NotNull
    private String playerUUID;

    @Length(max=30)
    @NotEmpty
    private String name;
    
    @NotNull
	private String language;
    
    @Version  
    Timestamp lastUpdate;  
    
    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
    
    public String getPlayerUUID() {
        return playerUUID;
    }

    public void setPlayerUUID(String playerUUID) {
        this.playerUUID = playerUUID;
    }

    public Player getPlayer() {
        return Bukkit.getServer().getPlayer(UUID.fromString(playerUUID));
    }

    public void setPlayer(Player player) {
        this.playerUUID = player.getUniqueId().toString();
    }
    
    public Timestamp getLastUpdate(){
    	return lastUpdate;
    }
    
    public void setLastUpdate(Timestamp lastUpdate){
    	this.lastUpdate = lastUpdate;
    }
}
