package es.minetsii.APIs.CNB;

import org.bukkit.entity.Player;

import es.minetsii.Splashoon.Splashoon;

public class SetSongPlaying extends SongPlaying {
	public SetSongPlaying(Song song) {
		super(song);
	}

	public void playTick(Player p, int tick) {
		for (Layer l : this.song.getLayerHashMap().values()) {
			Note note = l.getNote(tick);
			if (note != null) {
				p.playSound(p.getLocation(),
						CNBInstrument.getSound(note.getInstrument()),
						1000,
						Pitch.getPitch(note.getKey() - 33));
			}
		}
	}
	
	public static byte getPlayerVolume(Player p) {
		Byte b = (Byte) Splashoon.instance.playerVolume.get(p.getName());
		if (b == null) {
			b = Byte.valueOf((byte) 100);
			Splashoon.instance.playerVolume.put(p.getName(), b);
		}
		return b.byteValue();
	}
}