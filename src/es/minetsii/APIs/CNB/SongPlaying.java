package es.minetsii.APIs.CNB;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import es.minetsii.APIs.CNB.events.SongFinish;
import es.minetsii.APIs.CNB.events.SongRemove;
import es.minetsii.APIs.CNB.events.SongStop;
import es.minetsii.Splashoon.Splashoon;

public abstract class SongPlaying {
	protected Song song;
	protected boolean playing = false;
	protected short tick = -1;
	protected ArrayList<String> playerList = new ArrayList<String>();
	protected boolean autoDestroy = false;
	protected boolean destroyed = false;
	protected Thread playerThread;
	protected byte fadeTarget = 100;
	protected byte volume = 100;
	protected byte fadeStart = this.volume;
	protected int fadeDuration = 60;
	protected int fadeDone = 0;
	protected int customdelay = 0;

	public byte getFadeTarget() {
		return this.fadeTarget;
	}

	public void setFadeTarget(byte fadeTarget) {
		this.fadeTarget = fadeTarget;
	}

	public byte getFadeStart() {
		return this.fadeStart;
	}

	public void setFadeStart(byte fadeStart) {
		this.fadeStart = fadeStart;
	}

	public int getFadeDuration() {
		return this.fadeDuration;
	}

	public void setFadeDuration(int fadeDuration) {
		this.fadeDuration = fadeDuration;
	}

	public int getFadeDone() {
		return this.fadeDone;
	}

	public void setFadeDone(int fadeDone) {
		this.fadeDone = fadeDone;
	}

	public SongPlaying(Song song) {
		this.song = song;
		createThread();
	}

	protected void calculateFade() {
		if (this.fadeDone == this.fadeDuration) {
			return;
		}
		double targetVolume = Interpolator.interpLinear(new double[] { 0.0D,
				this.fadeStart, this.fadeDuration, this.fadeTarget },
				this.fadeDone);
		setVolume((byte) (int) targetVolume);
		this.fadeDone += 1;
	}

	protected void createThread() {
		this.playerThread = new Thread(new Runnable() {
			public void run() {
				while (!SongPlaying.this.destroyed) {
					long startTime = System.currentTimeMillis();
					synchronized (SongPlaying.this) {
						if (SongPlaying.this.playing) {
							SongPlaying.this.calculateFade();
							SongPlaying tmp42_39 = SongPlaying.this;
							tmp42_39.tick = ((short) (tmp42_39.tick + 1));
							if (SongPlaying.this.tick > SongPlaying.this.song
									.getLength()) {
								SongPlaying.this.playing = false;
								SongPlaying.this.tick = -1;
								SongFinish event = new SongFinish(
										SongPlaying.this);
								Bukkit.getPluginManager().callEvent(event);
								if (SongPlaying.this.autoDestroy) {
									SongPlaying.this.remove();
									return;
								}
							}
							for (String s : SongPlaying.this.playerList) {
								Player p = Bukkit.getPlayerExact(s);
								if (p != null) {

									SongPlaying.this.playTick(p,
											SongPlaying.this.tick);
								}
							}
						}
					}
					long duration = System.currentTimeMillis() - startTime;
					int delayMillis = SongPlaying.this.song.getDelay() * 50;
					if (duration < delayMillis) {
						try {
							if(!isCustomDelay())
								Thread.sleep(delayMillis - duration);
							else Thread.sleep(customdelay);

						} catch (InterruptedException e) {
						}
					}
				}
			}
		});
		this.playerThread.setPriority(10);
		this.playerThread.start();
	}

	public List<String> getPlayerList() {
		return Collections.unmodifiableList(this.playerList);
	}

	public void addPlayer(Player p) {
		synchronized (this) {
			if (!this.playerList.contains(p.getName())) {
				this.playerList.add(p.getName());
				ArrayList<SongPlaying> songs = Splashoon.playerslist.get(p
						.getName());

				if (songs == null) {
					songs = new ArrayList<SongPlaying>();
				}
				songs.add(this);
				Splashoon.playerslist.put(p.getName(), songs);
			}
		}
	}

	public void autoRemove(boolean value) {
		synchronized (this) {
			this.autoDestroy = value;
		}
	}

	public abstract void playTick(Player paramPlayer, int paramInt);

	public void remove() {
		synchronized (this) {
			SongRemove event = new SongRemove(this);
			Bukkit.getPluginManager().callEvent(event);

			if (event.isCancelled()) {
				return;
			}
			this.destroyed = true;
			this.playing = false;
			setTick((short) -1);
		}
	}

	public boolean isPlaying() {
		return this.playing;
	}

	public void setPlaying(boolean playing) {
		this.playing = playing;
		if (!playing) {
			SongStop event = new SongStop(this);
			Bukkit.getPluginManager().callEvent(event);
		}
	}

	public short getTick() {
		return this.tick;
	}

	public void setTick(short tick) {
		this.tick = tick;
	}

	public void removePlayer(Player p) {
		synchronized (this) {
			this.playerList.remove(p.getName());
			if (Splashoon.playerslist.get(p.getName()) == null) {
				return;
			}
			ArrayList<SongPlaying> songs = Splashoon.playerslist.get(p
					.getName());

			songs.remove(this);
			Splashoon.playerslist.put(p.getName(), songs);
			if ((this.playerList.isEmpty()) && (this.autoDestroy)) {
				SongFinish event = new SongFinish(this);
				Bukkit.getPluginManager().callEvent(event);
				remove();
			}
		}
	}

	public byte getVolume() {
		return this.volume;
	}

	public void setVolume(byte volume) {
		this.volume = volume;
	}

	public Song getSong() {
		return this.song;
	}
	
	public boolean isCustomDelay(){
		return customdelay != 0;
	}
	public int getCustomDelay(){
		return customdelay;
	}
	public void setCustomDelay(int customdelay){
		this.customdelay = customdelay;
	}
}
