package es.minetsii.APIs.CNB;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class BlockSongPlaying extends SongPlaying {
	
	private Location location;
	
	private SongType type;
	
	private Player player;
	
	private boolean ohter;

	public BlockSongPlaying(Song song,Location location,Player player) {
		super(song);
		this.location = location;
		this.player = player;
		ohter = false;
	}

	public Location getLocation() {
		return this.location;
	}

	public void setLocation(Location targetLocation) {
		this.location = targetLocation;
	}

	public SongType getType() {
		return type;
	}

	public void setType(SongType type) {
		this.type = type;
	}
	
	public Player getPlayer() {
		return player;
	}

	public void setPlayer(Player player) {
		this.player = player;
	}
	
	public boolean isOhter() {
		return ohter;
	}

	public void setOhter(boolean ohter) {
		this.ohter = ohter;
	}

	public void playTick(Player p, int tick) {
		if (!p.getWorld().getName()
				.equals(this.location.getWorld().getName())) {
			return;
		}

		for (Layer l : this.song.getLayerHashMap().values()) {
			Note note = l.getNote(tick);
			if (note != null) {

				p.playSound(
						this.location,
						CNBInstrument.getSound(note.getInstrument()),
						5,
						Pitch.getPitch(note.getKey() - 33));
			}
		}
	}
}
