package es.minetsii.APIs.CNB;

import java.util.ArrayList;
import java.util.List;

public enum SongType {

	Stop(0),
	Repeat(1),
	Random(2);
	
	
	private int i;
	
	
	private SongType(int i){
		this.i = i;
	}
	
	public int getInt(){
		return i;
	}
	
	
	public static List<String> getNames(){
		List<String> list = new ArrayList<String>();
		
		list.add("Stop");
		list.add("Repeat");
		list.add("Random");
		
		return list;
	}
}
