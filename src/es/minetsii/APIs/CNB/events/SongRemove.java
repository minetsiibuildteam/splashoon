package es.minetsii.APIs.CNB.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import es.minetsii.APIs.CNB.SongPlaying;

public class SongRemove extends Event implements
		org.bukkit.event.Cancellable {
	private static final HandlerList handlers = new HandlerList();
	private SongPlaying song;
	private boolean cancelled = false;

	public SongRemove(SongPlaying song) {
		this.song = song;
	}

	public SongPlaying getSongPlayer() {
		return this.song;
	}

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	public boolean isCancelled() {
		return this.cancelled;
	}

	public void setCancelled(boolean arg0) {
		this.cancelled = arg0;
	}
}