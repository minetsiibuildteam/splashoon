package es.minetsii.APIs.CNB.events;

import org.bukkit.event.HandlerList;

import es.minetsii.APIs.CNB.SongPlaying;

public class SongStop extends org.bukkit.event.Event {
	private static final HandlerList handlers = new HandlerList();
	private SongPlaying song;

	public SongStop(SongPlaying song) {
		this.song = song;
	}

	public SongPlaying getSongPlayer() {
		return this.song;
	}

	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}
}
