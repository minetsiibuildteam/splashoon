 package es.minetsii.APIs.CNB;

import org.bukkit.Instrument;
import org.bukkit.Sound;

public class CNBInstrument {
	
	public static Sound getSound(byte instrument) {
		switch (instrument) {
		case 0: 
			return Sound.NOTE_PIANO;
		case 1: 
			return Sound.NOTE_BASS_GUITAR;
		case 2: 
			return Sound.NOTE_BASS_DRUM;
		case 3: 
			return Sound.NOTE_SNARE_DRUM;
		case 4: 
			return Sound.NOTE_STICKS;
		}
		return Sound.NOTE_PIANO;
}
   
	public static org.bukkit.Instrument getInstrument(byte instrument){
	
		switch (instrument) {
		case 0: 
			return Instrument.PIANO;
		case 1: 
			return Instrument.BASS_GUITAR;
		case 2: 
			return Instrument.BASS_DRUM;
		case 3: 
			return Instrument.SNARE_DRUM;
		case 4: 
			return Instrument.STICKS;
		}
		return org.bukkit.Instrument.PIANO;
	}
}